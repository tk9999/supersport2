﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Maintenance
Imports OBLib.Timesheets.OBCity
Imports Singular.DataAnnotations
Imports Singular.Web
Imports Infragistics.Documents.Excel
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.Maintenance.HR
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports OBLib.Maintenance.Productions.ReadOnly.Old
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.SystemManagement

Public Class HumanResourceReports

#Region " HR Timesheet "

  Public Class HRTimesheet
    Inherits Singular.Reporting.ReportBase(Of HRTimesheetCriteria)


    Public Class HRTimesheetCriteria
      Inherits Singular.Reporting.EndDateReportCriteria

      <Display(Name:="Human Resource", Order:=3), Required(ErrorMessage:="Human Resource Required")>
      Public Property HumanResourceID As Integer?

      <Display(Name:="Read Only Human Resource List"), ClientOnly>
      Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    End Class

    Public Class HRTimesheetCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of HRTimesheetCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                    With .Helpers.FieldSet("Human Resource Filters")
                      .Attributes("id") = "HumanResourceFilters"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.FieldSet("Human Resources")
                    .Attributes("id") = "HumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.Bootstrap.Row
                      .Attributes("id") = "HRDiv"
                      With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                        With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddHumanResource($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Timesheet"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRTimesheetCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        If Me.DataSet IsNot Nothing Then
          If Me.DataSet.Tables(0).Rows(0).Item(4) Then '0: HumanResource datatable, 4: UseNewReport column
            Return GetType(rptHRTimesheetNew)
          Else
            Return GetType(rptHRTimesheet)
          End If
        End If
        Return GetType(rptHRTimesheetNew)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        Select Case col.ToString
          Case "HumanresourceID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next
    End Sub

    Public Overrides Function GetDataSource() As DataSet

      Dim ROTimesheetMonth As ROTimesheetMonth = OBLib.CommonData.Lists.ROTimesheetMonthList.GetItemByDate(ReportCriteria.EndDate)
      If ROTimesheetMonth IsNot Nothing Then
        Dim OBCityTimesheetList As OBCityTimesheetList = OBCityTimesheetList.GetOBCityTimesheetList(ReportCriteria.HumanResourceID, ROTimesheetMonth.TimesheetMonthID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Security.Settings.CurrentUser.ProductionAreaID, Nothing, Nothing)
        If OBCityTimesheetList.Count > 0 Then
          Dim OBCityTimesheet As OBCityTimesheet = OBCityTimesheetList(0)
          'CalculationType needed to be calculated & added to "HumanResource" datatable so that we know which crystal report to run in the CrystalReportType function
          Dim CalculationType As OBLib.CommonData.Enums.PublicHolidayCalculationType = OBCityTimesheet.GetPublicHolidayHourCalculationType(ROTimesheetMonth.StartDate)
          Dim UseNewReport As Boolean = False
          If CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.Original OrElse CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.UpToMidnight Then
            UseNewReport = False
          ElseIf CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent Then
            UseNewReport = True
          End If
          Dim d As DSHRTimesheet = HRTimesheetDataset(ReportCriteria.HumanResourceID, OBCityTimesheet, CDate(ReportCriteria.EndDate), UseNewReport)
          Return d
        Else
          Dim n As DSHRTimesheet = HRTimesheetDataset(ReportCriteria.HumanResourceID, Nothing, CDate(ReportCriteria.EndDate), False)
          Return n
        End If
      Else
        Return Nothing
      End If
    End Function

    Private Function HRTimesheetDataset(HumanResourceID As Integer, OBCityTimesheet As OBLib.Timesheets.OBCity.OBCityTimesheet, Month As Csla.SmartDate, UseNewReport As Boolean) As DSHRTimesheet

      Dim ROHumanResource As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID)

      Dim ds As DSHRTimesheet = New DSHRTimesheet

      If OBCityTimesheet IsNot Nothing Then
        '-------------Human Resource---------------------------------------
        Dim hr As DataTable = ds.HumanResource
        Dim d As DataRow = hr.NewRow
        d.Item("HumanResource") = ROHumanResource.Firstname + " " + ROHumanResource.Surname
        d.Item("IDNo") = ROHumanResource.IDNo
        d.Item("StaffNo") = ROHumanResource.EmployeeCode
        d.Item("TimesheetHasChangesFromOtherMonthsInd") = OBCityTimesheet.CrewTimesheetList.TimesheetHasChangesFromOtherMonthsInd
        d.Item("UseNewReport") = UseNewReport
        hr.Rows.Add(d)

        '-------------Timesheet Rows---------------------------------------
        Dim ctl As DataTable = ds.CrewTimesheets
        OBCityTimesheet.CrewTimesheetList.DoCalculations()
        For Each ct As OBLib.Timesheets.OBCity.CrewTimesheet In OBCityTimesheet.CrewTimesheetList
          If ct.LastDayPreviousMonthInd = False Then
            Dim ctr As DataRow = ctl.NewRow
            ctr.Item("DayOfMonth") = ct.CalculatedStartDateTime
            ctr.Item("DayOfWeek") = ct.CalculatedStartDateTime
            ctr.Item("PHInd") = ct.IsPublicHoliday
            ctr.Item("Description") = ct.Description
            ctr.Item("CrewStartDateTime") = ct.CrewStartDateTime
            ctr.Item("CrewEndDateTime") = ct.CrewEndDateTime
            ctr.Item("HoursForDay") = ct.HoursForDay
            ctr.Item("TotalHoursForMonth") = ct.HoursForMonth
            ctr.Item("HoursAboveOTLimit") = ct.HoursAboveOTLimit
            ctr.Item("ShortfallHours") = ct.ShortfallHours
            ctr.Item("TotalShortfall") = ct.ShortfallForMonth
            ctr.Item("PHHours") = ct.PublicHolidayHours
            ctl.Rows.Add(ctr)
          End If
        Next

        '-------------Summaries---------------------------------------
        Dim summ As DataTable = ds.SummariesNew
        Dim summR As DataRow = summ.NewRow

        summR.Item("UnadjustedHours") = OBCityTimesheet.UnadjustedHours
        summR.Item("UnadjustedPlanningHours") = OBCityTimesheet.UnadjustedPlanningHours
        summR.Item("UnadjustedTotal") = OBCityTimesheet.UnadjustedTotal
        summR.Item("UnadjustedRequiredHours") = OBCityTimesheet.UnadjustedRequiredHours
        summR.Item("UnadjustedPublicHolidayHours") = OBCityTimesheet.UnadjustedPublicHolidayHours
        summR.Item("UnadjustedPublicHolidayHoursAdjusted") = OBCityTimesheet.UnadjustedPublicHolidayHoursAdjusted
        summR.Item("UnadjustedOvertime") = OBCityTimesheet.UnadjustedOvertime
        summR.Item("UnadjustedShortfall") = OBCityTimesheet.UnadjustedShortfall
        summR.Item("UnadjustedTotalOvertimeAndShortfall") = OBCityTimesheet.UnadjustedTotalOvertimeAndShortfall
        summR.Item("NormalHoursAdjustment") = OBCityTimesheet.NormalHoursAdjustment
        summR.Item("PlanningHoursAdjustment") = OBCityTimesheet.PlanningHoursAdjustment
        summR.Item("TotalAdjustment") = OBCityTimesheet.TotalAdjustment
        summR.Item("RequiredHoursAdjustment") = OBCityTimesheet.RequiredHoursAdjustment
        summR.Item("PublicHolidayHoursAdjustment") = OBCityTimesheet.PublicHolidayHoursAdjustment
        summR.Item("PublicHolidayHoursAdjustedAdjustment") = OBCityTimesheet.PublicHolidayHoursAdjustedAdjustment
        summR.Item("OvertimeAdjustment") = OBCityTimesheet.OvertimeAdjustment
        summR.Item("ShortfallAdjustment") = OBCityTimesheet.ShortfallAdjustment
        summR.Item("TotalOvertimeAndShortfallAdjustment") = OBCityTimesheet.TotalOvertimeAndShortfallAdjustment
        summR.Item("FinalHours") = OBCityTimesheet.FinalHours
        summR.Item("FinalPlanningHours") = OBCityTimesheet.FinalPlanningHours
        summR.Item("FinalTotal") = OBCityTimesheet.FinalTotal
        summR.Item("FinalRequiredHours") = OBCityTimesheet.FinalRequiredHours
        summR.Item("FinalPublicHolidayHours") = OBCityTimesheet.FinalPublicHolidayHours
        summR.Item("FinalPublicHolidayHoursAdjusted") = OBCityTimesheet.FinalPublicHolidayHoursAdjusted
        summR.Item("FinalOvertime") = OBCityTimesheet.FinalOvertime
        summR.Item("FinalShortfall") = OBCityTimesheet.FinalShortfall
        summR.Item("FinalTotalOvertimeAndShortfall") = OBCityTimesheet.FinalTotalOvertimeAndShortfall

        summR.Item("PublicHolidaysTotal") = OBCityTimesheet.PublicHolidaysTotal
        summR.Item("PublicHolidaysUnderLimit") = OBCityTimesheet.PublicHolidaysUnderLimit
        summR.Item("PublicHolidaysOverLimit") = OBCityTimesheet.PublicHolidaysOverLimit
        summR.Item("OvertimeLessPublicHolidaysOverLimit") = OBCityTimesheet.OvertimeLessPublicHolidaysOverLimit
        summR.Item("FinalPublicHolidaysUnderLimit") = OBCityTimesheet.FinalPublicHolidaysUnderLimit
        summR.Item("FinalPublicHolidaysOverLimit") = OBCityTimesheet.FinalPublicHolidaysOverLimit

        'summR.Item("UnadjustedHours") = CrewTimesheetList.GetTotalHours
        'summR.Item("UnadjustedPlanningHours") = CrewTimesheetList.GetPlanningHours
        'summR.Item("UnadjustedTotalPlanningAndHours") = CrewTimesheetList.GetHoursAndPlanning
        'summR.Item("UnadjustedRequiredWorkingHours") = CommonData.Lists.TimesheetMonthList.GetItem(Month.Date).HoursBeforeOvertime
        'summR.Item("UnadjustedOvertime") = CrewTimesheetList.GetTotalOvertime
        'summR.Item("UnadjustedShortfall") = CrewTimesheetList.GetTotalShortfall
        'summR.Item("UnadjustedTimeForMonth") = CrewTimesheetList.GetFinalTimeForMonth
        'summR.Item("UnadjustedPublicHolidayHours") = (CrewTimesheetList.GetTotalPublicHolidayHours)
        'summR.Item("UnadjustedAdjustedPublicHolidayHours") = (CrewTimesheetList.GetAdjustedPHHours)


        'summR.Item("TotalOvertimeShortfall") = (CrewTimesheetList.GetFinalOverTime)


        'summR.Item("NormalHourAdjustments") = (CrewTimesheetList.GetTotalNormalHourAdjustments)
        'summR.Item("PlanningHourAdjustments") = (CrewTimesheetList.GetTotalPlanningHourAdjustments)
        'summR.Item("OvertimeAdjustments") = (CrewTimesheetList.GetTotalOvertimeHourAdjustments)
        'summR.Item("ShortfallAdjustments") = (CrewTimesheetList.GetTotalShortfallHourAdjustments)
        'summR.Item("PublicHolidayAdjustments") = (CrewTimesheetList.GetTotalPublicHolidayHourAdjustments)

        summ.Rows.Add(summR)


        '-------------Planning Hours---------------------------------------
        Dim emPlanningHourList As OBLib.Timesheets.OBCity.ReadOnly.ROEventManagerPlanningHourList = OBLib.Timesheets.OBCity.ReadOnly.ROEventManagerPlanningHourList.GetROEventManagerPlanningHourList(Month, HumanResourceID)
        Dim planHours As DataTable = ds.ProductionPlanningHours
        For Each pplhr In emPlanningHourList
          Dim planHR As DataRow = planHours.NewRow
          planHR.Item("ProductionID") = pplhr.ProductionID
          planHR.Item("Description") = pplhr.ProdDescription
          planHR.Item("TotalPlanningHours") = pplhr.Hours
          planHours.Rows.Add(planHR)
        Next


        '-------------Timesheet Adjustment----------------------------------
        'Dim timesheetAdjustList As OBLib.Timesheets.OBCity.TimesheetAdjustmentList = OBLib.Timesheets.OBCity.TimesheetAdjustmentList.GetAdjustmentsToReflectInCurrentMonth(HumanResourceID, 0, Month)
        Dim tsAdjust As DataTable = ds.TimesheetAdjustments
        For Each ta In OBCityTimesheet.TimesheetAdjustmentList
          Dim timesheetAdjust As DataRow = tsAdjust.NewRow
          timesheetAdjust.Item("TimesheetHourType") = ta.TimesheetHourType
          timesheetAdjust.Item("Adjustment") = ta.Adjustment
          timesheetAdjust.Item("AdjustmentReason") = ta.AdjustmentReason
          'timesheetAdjust.Item("ProductionRefNo") = ta.ProductionRefNo
          tsAdjust.Rows.Add(timesheetAdjust)
        Next


        '-------------Timesheet Month---------------------------------------
        Dim tMonth As Timesheets.ReadOnly.ROTimesheetMonth = OBLib.CommonData.Lists.ROTimesheetMonthList.GetItemByDate(Month.Date)
        Dim tmTable As DataTable = ds.TimesheetMonth
        Dim tmRow As DataRow = tmTable.NewRow
        tmRow.Item("TimesheetMonth") = tMonth.Description
        tmTable.Rows.Add(tmRow)
      End If

      Return ds

    End Function

  End Class

#End Region

#Region " Annual Leave "

  Public Class AnnualLeaveReport
    Inherits ReportBase(Of AnnualLeaveCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Annual Leave Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptAnnualLeave)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptAnnualLeave"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(AnnualLeaveCriteriaControl)
      End Get
    End Property

  End Class

  Public Class AnnualLeaveCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Off Reason", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROOffReasonList), UnselectedText:="Off Reason")>
    Public Property OffReasonID As Integer?

    <Display(Name:="Unit Supervisor", Order:=2)>
    Public Property UnitSupervisorIDs As List(Of Integer)

    <Display(Name:="Manager", Order:=5)>
    Public Property ManagerIDs As List(Of Integer)

    <Display(Name:="Systems", Order:=2)>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Areas", Order:=5)>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Read Only Unit Supervisor List"), ClientOnly>
    Public Property ROUnitSupervisorList As OBLib.HR.ReadOnly.ROUnitSupervisorList = OBLib.CommonData.Lists.ROUnitSupervisorList

    <Display(Name:="Read Only Manager List"), ClientOnly>
    Public Property ROManagerList As OBLib.HR.ReadOnly.ROManagerList = OBLib.CommonData.Lists.ROManagerList

  End Class

  Public Class AnnualLeaveCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of AnnualLeaveCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.OffReasonID)
                With .Helpers.Bootstrap.FormControlFor(Function(d As AnnualLeaveCriteria) d.OffReasonID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Off Reason"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "AnnualLeaveReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "AnnualLeaveReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.FieldSet("Unit Supervisors")
                With .Helpers.ForEachTemplate(Of ROUnitSupervisor)(Function(d) d.ROUnitSupervisorList)
                  With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                      .Button.AddBinding(KnockoutBindingString.click, "AnnualLeaveReport.AddUnitSupervisor($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.FieldSet("Managers")
                With .Helpers.ForEachTemplate(Of ROManager)(Function(d) d.ROManagerList)
                  With .Helpers.Bootstrap.Column(12, 12, 6, 4, 3)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                      .Button.AddBinding(KnockoutBindingString.click, "AnnualLeaveReport.AddManager($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

  End Class

#End Region

#Region "Human Resource Skills"
  Public Class HumanResourceSkills
    Inherits ReportBase(Of HumanResourceSkillsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Skills"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHRSkillsReport"
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add(New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID"), DataSet.Tables(0).Columns("DisciplineID")},
                            New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID"), DataSet.Tables(1).Columns("DisciplineID")})

      For Each col In DataSet.Tables(0).Columns
        Select Case col.ToString
          Case "HumanResourceID", "DisciplineID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next

      For Each col In DataSet.Tables(1).Columns
        Select Case col.ToString
          Case "HumanResourceID", "DisciplineID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRSkillsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceSkillsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production Type")>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="System")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Area")>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Discipline"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Ignore Date Criteria?")>
    Public Property IgnoreDatesInd As Boolean = True

  End Class

  Public Class HRSkillsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceSkillsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
            With .Helpers.FieldSet("Sub-Depts and Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "HumanResourceSkillsReport.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.Bootstrap.Row
                  .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                    With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                      .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HumanResourceSkillsReport.AddProductionArea($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Div
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillsCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Discipline"
                End With
              End With
            End With
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkillsCriteria) d.IgnoreDatesInd,
                                                  "Ignore dates", "Include dates", "btn-success", "btn-danger", , "btn-sm")
                .Button.Style.Width = "150px"
              End With

            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 11, 10)
            With .Helpers.Bootstrap.Row()
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.FieldSet("Production Types")
                  .Attributes("id") = "Productions"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6, 6)
                          With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                            End With
                          End With
                        End With
                      End With
                    End With
                    .Helpers.HTML.NewLine()
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "HumanResourceSkillsReport.AddProductionType($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource Skill Levels"

  Public Class HumanResourceSkillLevels
    Inherits ReportBase(Of HumanResourceSkillLevelsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Skill Levels"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceSkillLevels"
      Dim pt As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionType").FirstOrDefault
      cmd.Parameters.Remove(pt)
      Dim roptl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionTypeList").FirstOrDefault
      cmd.Parameters.Remove(roptl)

    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Or
          col.ToString = "ContractTypeID" Or
          col.ToString = "DisciplineID" Or
          col.ToString = "HumanResourceSkillID" Or
          col.ToString = "ProductionTypeID" Or
           col.ToString = "PositionID" Or
            col.ToString = "PositionLevelID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRSkillLevelsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceSkillLevelsCriteria
    Inherits DefaultCriteria

    '<Display(Name:="Sub-Dept", Description:="", Order:=1),
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="Sub-Dept")>
    'Public Property SystemID As Integer?

    <Display(Name:="Contract Type", Description:="", Order:=2),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?

    <Display(Name:="Production Type", Description:="", Order:=3)>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Production Type", Description:="", Order:=3)>
    Public Property ProductionType As String = ""

    <Display(Name:="Production Type", Order:=1)>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) =
   OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Discipline", Description:="", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Position", Description:="", Order:=5),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROPositionList), UnselectedText:="Position")>
    Public Property PositionID As Integer?

    <Display(Name:="Position Level", Description:="", Order:=6),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROSkillLevelList), UnselectedText:="Position Level")>
    Public Property PositionLevelID As Integer?

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class HRSkillLevelsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceSkillLevelsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillLevelsCriteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
          '          .Editor.Attributes("placeholder") = "Sub-Dept"
          '        End With
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillLevelsCriteria) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Contract Type"
                    End With
                  End With
                End With
              End With
            End With
          End With
          'With .Helpers.Bootstrap.Row
          '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '        With .Helpers.Div
          '            With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HumanResourceSkillLevelsCriteria) d.ProductionType, "HRSkillLevelsReport.FindProductionType($element)",
          '                                                   "Search For Production Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '            End With
          '        End With
          '    End With
          'End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.Div
              With .Helpers.FieldSet("Production Types")
                With .Helpers.Div
                  .Attributes("id") = "ProductionDiv"
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HRSkillLevelsReport.AddProductionType($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillLevelsCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Discipline"
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillLevelsCriteria) d.PositionID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Position"
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSkillLevelsCriteria) d.PositionLevelID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Position Level"
                    End With
                  End With
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
                With .Helpers.Div
                  With .Helpers.Bootstrap.ButtonGroup
                    If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                      .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkillLevelsCriteria) d.ProductionServicesInd,
                                                     "Production Services", "Production Services", , , , "fa-hand-o-up", )
                    End If
                    If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                      .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkillLevelsCriteria) d.ProductionContentInd,
                                                     "Production Content", "Production Content", , , , "fa-hand-o-up", )
                    End If
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                .Helpers.Bootstrap.LabelDisplay("Areas")
                With .Helpers.Div
                  With .Helpers.Bootstrap.ButtonGroup
                    If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                      .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkillLevelsCriteria) d.OutsideBroadcastInd,
                                                     "OB", "OB", , , , "fa-hand-o-up", )
                    End If
                    If Singular.Security.HasAccess("Production Area", "Studios") Then
                      .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSkillLevelsCriteria) d.StudioInd,
                                                     "Studio", "Studio", , , , "fa-hand-o-up", )
                    End If
                  End With
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource Shifts"

  Public Class HumanResourceShifts
    Inherits Singular.Reporting.ReportBase(Of HumanResourceShiftsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Shifts"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRShiftsReport]"
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      'Dim cuid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@CurrentUserSystemID").FirstOrDefault
      'cmd.Parameters.Remove(cuid)
      'Dim cupid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@CurrentUserProductionAreaID").FirstOrDefault
      'cmd.Parameters.Remove(cupid)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim rohsl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohsl)
      Dim bhr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhr)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceShiftsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceShiftsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria


    <Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=4), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=5), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

    <Display(Name:="Human Resource", Order:=6)>
    Public Property HumanResource As String

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False


    <Display(Name:="Discipline", Description:="", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?


    <Display(Name:="Include/Exclude Freelancers?", Description:="", Order:=3)>
    Public Property FreelancerInd As Boolean
  End Class

  Public Class HumanResourceShiftsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of HumanResourceShiftsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(8, 4, 4, 2)
              .Style.TextAlign = TextAlign.center
              .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
              .Style.Position = Position.relative
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(8, 4, 4, 2)
              .Style.TextAlign = TextAlign.center
              .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
              .Style.Position = Position.relative
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceShiftsCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Discipline"
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.FieldSet("Sub-Depts")
                    With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HRShiftsReport.AddSystem($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Div
                    With .Helpers.FieldSet("Production Areas")
                      With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "HRShiftsReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Div
                              .Helpers.LabelFor(Function(d As HumanResourceShiftsCriteria) d.FreelancerInd)
                            End With
                            With .Helpers.Div
                              With .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceShiftsCriteria) d.FreelancerInd, "Include Freelancers", "Exclude Freelancers", "btn-success", "btn-danger", , , "btn-sm")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                    With .Helpers.FieldSet("Human Resources")
                      .AddClass("FadeHide")
                      .Attributes("id") = "HumanResources"
                      With .Helpers.Div
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 8, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                              .Attributes("placeholder") = "Search Human Resource"
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "HRShiftsReport.RefreshHR()")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                            .Helpers.HTML.NewLine()
                            With .Helpers.Div
                              .Attributes("id") = "HRDiv"
                              With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                  With .Helpers.If(Function(c As HumanResourceShiftsCriteria) Not c.BusyHRList)
                                    With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                          .Button.AddBinding(KnockoutBindingString.click, "HRShiftsReport.AddHumanResource($data)")
                                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                          .Button.AddClass("btn-block buttontext")
                                        End With
                                      End With
                                    End With
                                  End With
                                  With .Helpers.If(Function(c As HumanResourceShiftsCriteria) c.BusyHRList)
                                    With .Helpers.Div
                                      With .Helpers.HTMLTag("i")
                                        .AddClass("fa fa-cog fa-lg fa-spin")
                                      End With
                                      With .Helpers.HTMLTag("span")
                                        .Helpers.HTML("Updating...")
                                      End With
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With




        End With
      End With




      'Protected Overrides Sub Setup()
      '     MyBase.Setup()

      '     With Helpers.With(Of HumanResourceShiftsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
      '         With .Helpers.FieldSet("Date Selection")
      '             With .Helpers.Bootstrap.Row
      '                 With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '                     With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                         With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
      '                             .Editor.Attributes("placeholder") = "Start Date"
      '                         End With
      '                     End With
      '                 End With
      '                 With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '                     With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                         With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
      '                             .Editor.Attributes("placeholder") = "End Date"
      '                         End With
      '                     End With
      '                 End With
      '             End With
      '         End With
      '         With .Helpers.FieldSet("Criteria")
      '             With .Helpers.Bootstrap.Row
      '                 With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '                     With .Helpers.Div
      '                         With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
      '                             With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceShiftsCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
      '                                 .Editor.Attributes("placeholder") = "Discipline"
      '                             End With
      '                         End With
      '                     End With
      '                 End With
      '             End With
      '             With .Helpers.Bootstrap.Row
      '                 With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '                     With .Helpers.Div
      '                         With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HumanResourceShiftsCriteria) d.HumanResource, "HRShiftsReport.FindHR($element)",
      '                                                                "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
      '                         End With
      '                     End With
      '                 End With
      '             End With

      '         End With
      '     End With

    End Sub

  End Class
#End Region

#Region "Human Resource Shift Details"

  Public Class ProductionHRShiftDetailReport
    Inherits ReportBase(Of ProductionHRShiftDetailReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Shift Detail"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptHRShiftDetail)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionHRShiftDetailReport"
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hr)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).ChildRelations.Add("HRID", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")},
                                                   New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionHRShiftDetailCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionHRShiftDetailCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionHRShiftDetailReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionHRShiftDetailReportCriteria) d.HumanResources, "ProductionHRShiftDetailReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ProductionHRShiftDetailReport.ClearHRs()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  Public Class ProductionHRShiftDetailReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required(ErrorMessage:="Start Date Required")>
    Public Property StartDate As Date

    <Display(Name:="End Date", Order:=2),
    Required(ErrorMessage:="End Date Required")>
    Public Property EndDate As Date

    <Display(Name:="Human Resource", Description:="", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Description:="", Order:=4)>
    Public Property HumanResources As String = ""


  End Class

#End Region

#Region "Human Resource S&T"

  Public Class HumanResourceSnT
    Inherits ReportBase(Of HumanResourceSnTCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource S&T"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptHumanResourceSnT)
      End Get

    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHRSnT"

      Dim hri As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResourceID").FirstOrDefault
      cmd.Parameters.Remove(hri)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hrs)
      Dim rorsl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rorsl)
      Dim bhr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhr)
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)


    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)


      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next



    End Sub
    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceSnTCriteriaControl)
      End Get
    End Property

  End Class
  Public Class HumanResourceSnTCriteria
    Inherits Singular.Reporting.EndDateReportCriteria

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResourceIDs As List(Of Integer)

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResourceID As Integer

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResource As String = ""

    <Display(Name:="System")>
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

    <Display(Name:="ProductionArea")>
    Public Property ProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID


    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class HumanResourceSnTCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceSnTCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With

        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                With .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
                  With .Helpers.Div
                    With .Helpers.Bootstrap.ButtonGroup
                      If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                        .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSnTCriteria) d.ProductionServicesInd,
                                                       "Production Services", "Production Services", , , , "fa-hand-o-up", )
                      End If
                      If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                        .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSnTCriteria) d.ProductionContentInd,
                                                       "Production Content", "Production Content", , , , "fa-hand-o-up", )
                      End If
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.LabelDisplay("Areas")
                    With .Helpers.Div
                      With .Helpers.Bootstrap.ButtonGroup
                        If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSnTCriteria) d.OutsideBroadcastInd,
                                                         "OB", "OB", , , , "fa-hand-o-up", )
                        End If
                        If Singular.Security.HasAccess("Production Area", "Studios") Then
                          .Helpers.Bootstrap.StateButtonNew(Function(d As HumanResourceSnTCriteria) d.StudioInd,
                                                         "Studio", "Studio", , , , "fa-hand-o-up", )
                        End If
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                With .Helpers.FieldSet("Human Resources")
                  '.AddClass("FadeHide")
                  .Attributes("id") = "HumanResources"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 8, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                            .Attributes("placeholder") = "Search Human Resource"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "MonthlySubsistenceandTravelAllowanceReport.RefreshHR()")
                          End With
                        End With
                      End With
                    End With
                    .Helpers.HTML.NewLine()
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.If(Function(c As HumanResourceSnTCriteria) Not c.BusyHRList)
                            With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                  .Button.AddBinding(KnockoutBindingString.click, "MonthlySubsistenceandTravelAllowanceReport.AddHumanResource($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.If(Function(c As HumanResourceSnTCriteria) c.BusyHRList)
                            With .Helpers.Div
                              With .Helpers.HTMLTag("i")
                                .AddClass("fa fa-cog fa-lg fa-spin")
                              End With
                              With .Helpers.HTMLTag("span")
                                .Helpers.HTML("Updating...")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.FieldSet("Selected Human Resources")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.Button("", "", , , , , , , , , )
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region " HR Skills Report New"

  Public Class HRSkillsReportNew
    Inherits ReportBase(Of HRSkillsReportNewCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return " HR Skills Report New"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHRSkillsReportNew"
      Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hrs)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      Dim si As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@SystemID").FirstOrDefault
      cmd.Parameters.Remove(si)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim pai As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaID").FirstOrDefault
      cmd.Parameters.Remove(pai)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohrl)
      Dim bhr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhr)

    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceSkillsCriteriaControl)
      End Get
    End Property


  End Class


  Public Class HRSkillsReportNewCriteria
    Inherits DefaultCriteria

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResourceIDs As List(Of Integer)

    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="Human Resource")>
    Public Property HumanResource As String

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)


    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList



    <ClientOnly>
    Public Property BusyHRList As Boolean = False






  End Class

  Public Class HumanResourceSkillsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRSkillsReportNewCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HRSkillsNew.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HRSkillsNew.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.FieldSet("Human Resources")
                    .AddClass("FadeHide")
                    .Attributes("id") = "HumanResources"
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 8, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                            .Attributes("placeholder") = "Search Human Resource"
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HRSkillsNew.DelayedRefreshList}")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "HRSkillsNew.RefreshHR()")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          .Helpers.HTML.NewLine()
                          With .Helpers.Div
                            .Attributes("id") = "HRDiv"
                            With .Helpers.Bootstrap.Row
                              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                With .Helpers.If(Function(c As HRSkillsReportNewCriteria) Not c.BusyHRList)
                                  With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                        .Button.AddBinding(KnockoutBindingString.click, "HRSkillsNew.AddHumanResource($data)")
                                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                        .Button.AddClass("btn-block buttontext")
                                      End With
                                    End With
                                  End With
                                End With
                                With .Helpers.If(Function(c As HRSkillsReportNewCriteria) c.BusyHRList)
                                  With .Helpers.Div
                                    With .Helpers.HTMLTag("i")
                                      .AddClass("fa fa-cog fa-lg fa-spin")
                                    End With
                                    With .Helpers.HTMLTag("span")
                                      .Helpers.HTML("Updating...")
                                    End With
                                  End With
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.FieldSet("Selected Human Resources")
                          With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
                            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                              With .Helpers.Bootstrap.Button("", "", , , , , , , , , )
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With





      'With Helpers.With(Of HRSkillsReportNewCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
      '    With .Helpers.FieldSet("Criteria")
      '        With .Helpers.Bootstrap.Row
      '            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
      '                With .Helpers.Div
      '                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRSkillsReportNewCriteria) d.HumanResources, "HRSkillsNew.FindHR($element)",
      '                                                           "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "HRSkillsNew.ClearHRs()")
      '                    End With
      '                End With
      '            End With
      '        End With
      '    End With
      'End With
    End Sub
  End Class

#End Region

#Region "HR Rates"

  Public Class HRRates
    Inherits ReportBase(Of HRRatesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "HR Rates"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRRates]"
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hrs)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim pai As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaID").FirstOrDefault
      cmd.Parameters.Remove(pai)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim rohsl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohsl)
      Dim ed As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EndDate").FirstOrDefault
      cmd.Parameters.Remove(ed)
      Dim bhr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhr)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRRatesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HRRatesCriteria
    Inherits Singular.Reporting.EndDateReportCriteria

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResource As String

    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="Discipline", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Contract Type", Order:=5),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?

    <Display(Name:="Effective Rate Date")>
    Public Property EffectiveRateDate = EndDate

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False


  End Class

  Public Class HRRatesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRRatesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)

            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Rate Date")
                With .Helpers.Bootstrap.Column(12, 12, 11, 8)
                  With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                    '.Style.TextAlign = TextAlign.center
                    '.Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "HRRates.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.FieldSet("Production Areas")
                  With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HRRates.AddProductionArea($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Discipline"
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Contract Type"
                    End With
                  End With
                End With
              End With
            End With

            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Human Resources")
                .Attributes("id") = "HumanResources"
                .AddClass("FadeHide")
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                          .AddClass("form-control input-sm")
                          .Editor.Attributes("placeholder") = "Search Human Resource"
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                          .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HRRates.DelayedRefreshList}")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "HRRates.RefreshHR()")
                          .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
                        End With
                      End With
                    End With
                  End With

                  .Helpers.HTML.NewLine()
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.If(Function(c As HRRatesCriteria) Not c.BusyHRList)
                          With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                .Button.AddBinding(KnockoutBindingString.click, "HRRates.AddHumanResource($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.If(Function(c As HRRatesCriteria) c.BusyHRList)
                          With .Helpers.Div
                            With .Helpers.HTMLTag("i")
                              .AddClass("fa fa-cog fa-lg fa-spin")
                            End With
                            With .Helpers.HTMLTag("span")
                              .Helpers.HTML("Updating...")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.FieldSet("Selected Human Resources")
                  .Attributes("id") = "SelectedHumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.Button("", "", , , , , , , , , )
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With

          End With
        End With
      End With

    End Sub
  End Class


  'With Helpers.With(Of HRRatesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
  '    With .Helpers.FieldSet("Criteria")
  '        With .Helpers.Bootstrap.Row
  '            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
  '                With .Helpers.Div
  '                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRRatesCriteria) d.HumanResource, "HRRates.FindHR($element)",
  '                                                           "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "HRRates.ClearHRs()")
  '                    End With
  '                End With
  '            End With
  '        End With
  'With .Helpers.Bootstrap.Row
  '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
  '        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
  '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EffectiveRateDate, Singular.Web.BootstrapEnums.InputSize.Small)
  '                .Editor.Attributes("placeholder") = "Rate Date"
  '            End With
  '        End With
  '    End With
  'End With


#End Region

#Region "HR Contract Types"

  Public Class HRContractTypes
    Inherits ReportBase(Of HRContractTypesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "HR Contract Types"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptHRContractTypes)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceContractTypes"
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(e)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRContractTypesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HRContractTypesCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Human Resource", Order:=1)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Discipline", Order:=2),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Contract Type", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?

    <Display(Name:="Human Resources", Order:=3)>
    Public Property HumanResources As String = ""

  End Class

  Public Class HRContractTypesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRContractTypesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRContractTypesCriteria) d.HumanResources, "HRContractTypes.FindHR($element)",
                                                       "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "HRContractTypes.ClearHRs()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Discipline"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Contract Type"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


#End Region

#Region "Human Resource Leave"

  Public Class HumanResourceLeave
    Inherits ReportBase(Of HumanResourceLeaveCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Leave"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceLeave"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hrs)
      Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhl)
      Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohrl)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRLeaveCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceLeaveCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria


    <Display(Name:="Human Resource")>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Human Resource")>
    Public Property HumanResource As String

    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    Public Property BusyHRList As Boolean = False

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

  End Class

  Public Class HRLeaveCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceLeaveCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.FieldSet("Human Resources")
              .Attributes("id") = "HumanResources"
              With .Helpers.Div
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                        .Editor.Attributes("placeholder") = "Search Human Resource"
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshListNoSIDNoPA.DelayedRefreshList}")
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                      With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "AddSingleHumanResource.RefreshHR()")
                        .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
                      End With
                    End With
                  End With
                End With

                .Helpers.HTML.NewLine()
                With .Helpers.Div
                  .Attributes("id") = "HRDiv"
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.If(Function(c As HumanResourceLeaveCriteria) Not c.BusyHRList)
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "AddSingleHumanResource.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.If(Function(c As HumanResourceLeaveCriteria) c.BusyHRList)
                        With .Helpers.Div
                          With .Helpers.HTMLTag("i")
                            .AddClass("fa fa-cog fa-lg fa-spin")
                          End With
                          With .Helpers.HTMLTag("span")
                            .Helpers.HTML("Updating...")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource Timesheet"

  Public Class HumanResourceTimesheet
    Inherits ReportBase(Of HumanResourceTimesheetCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Timesheet"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRTimesheet]"
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(e)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRTimeSheetCriteriaControl)
      End Get
    End Property
  End Class

  Public Class HumanResourceTimesheetCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Discipline", Description:="", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    '<Display(Name:="Human Resource", Description:="", Order:=2), Required(ErrorMessage:="Human Resource Required")>
    'Public Property HumanResourceIDs As Object
    '<Display(Name:="Human Resources", Order:=3)>
    'Public Property HumanResources As String = ""

    '<System.ComponentModel.DisplayName("Human Resource")>
    'Public Property HumanResourceIDs As List(Of Integer)

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResources As String = ""

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False




    '<Display(Name:="Human Resource", Order:=3),
    'Required(ErrorMessage:="Human Resource Required")>
    'Public Property HumanResourceID As Integer?

    Public Property CurrentUserSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
    Public Property CurrentUserProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList



    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Human Resource", Order:=4)>
    Public Property HumanResource As String


    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False
  End Class

  Public Class HRTimeSheetCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceTimesheetCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With


        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      'With .Helpers.If(Function(c) c.IsSelected)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        'With .Helpers.If(Function(c) c.IsSelected)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                          'With .Button.Helpers.HTMLTag("i")
                          '    .AddClass("fa fa-check-square-o")
                          'End With
                        End With
                        'End With
                      End With
                    End With
                  End With
                End With

                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.FieldSet("Human Resources")
                    '.Attributes("id") = "HumanResources"
                    '.AddClass("FadeHide")
                    With .Helpers.Div
                      With .Helpers.Div
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                            With .Helpers.EditorFor(Function(d) d.HumanResource)
                              .AddClass("form-control input-sm")
                              .Attributes("placeholder") = "Search Human Resource"
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "HRTimeSheetReport.RefreshHR()")
                              .Button.AddBinding(KnockoutBindingString.visible, Function(c) c.ROHumanResourceList.Count >= 0)
                            End With
                          End With
                        End With
                      End With

                      .Helpers.HTML.NewLine()
                      With .Helpers.Div
                        '.Attributes("id") = "HRDiv"
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                            With .Helpers.If(Function(c As HumanResourceTimesheetCriteria) Not c.BusyHRList)
                              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                    .Button.AddBinding(KnockoutBindingString.click, "HRTimeSheetReport.AddHumanResource($data)")
                                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                    .Button.AddClass("btn-block buttontext")
                                  End With
                                End With
                              End With
                            End With
                            With .Helpers.If(Function(c As HumanResourceTimesheetCriteria) c.BusyHRList)
                              With .Helpers.Div
                                With .Helpers.HTMLTag("i")
                                  .AddClass("fa fa-cog fa-lg fa-spin")
                                End With
                                With .Helpers.HTMLTag("span")
                                  .Helpers.HTML("Updating...")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region " Production HR Shift Report "

  Public Class ProductionHRShiftReport
    Inherits ReportBase(Of ProductionHRShiftReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Content Human Resource Shifts"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptHumanResourceShifts)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionHRShiftsReport"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionHRShiftCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionHRShiftReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria



  End Class

  Public Class ProductionHRShiftCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionHRShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)

        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource Details"

  Public Class HumanResourceDetailsReport
    Inherits ReportBase(Of HumanResourceDetailsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRDetails]"
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceDetailsControl)
      End Get
    End Property
  End Class

  Public Class HumanResourceDetailsCriteria
    Inherits DefaultCriteria

    <Display(Name:="Sub-Dept.", Order:=1), Required(ErrorMessage:="Sub-Dept. is Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="Sub-Dept.")>
    Public Property SystemID As Integer?

    <Display(Name:="Area", Order:=2), Required(ErrorMessage:="Area is Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(Productions.Areas.ReadOnly.ROProductionAreaList), UnselectedText:="Area")>
    Public Property ProductionAreaID As Integer?

    <Display(Name:="Contract Type", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?


    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList


    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

  End Class

  Public Class HumanResourceDetailsControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of HumanResourceDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Div
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HumanResourceDetailsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HumanResourceDetailsReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                        .Editor.Attributes("placeholder") = "Contract Type"
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Hours Booked"

  Public Class HoursBookedNew

    Inherits ReportBase(Of HoursBookedNewCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Hours Booked"
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      DataSet.Relations.Add(DataSet.Tables(0).Columns("HumanResourceID"), DataSet.Tables(1).Columns("HumanResourceID"))
    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHoursBookedNew]"
      cmd.CommandTimeout = 0
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HoursBookedNewCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HoursBookedNewCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Discipline", Description:="", Order:=4),
   Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Select Discipline...")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Contract Type", Description:="", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Select Contract Type...")>
    Public Property ContractTypeID As Integer?

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

    <Display(Name:="Timesheet Month", Description:="", Order:=5),
    Singular.DataAnnotations.DropDownWeb(GetType(ROTimesheetMonthList), UnselectedText:="Timesheet Month")>
    Public Property TimeSheetMonthID As Integer?

  End Class

  Public Class HoursBookedNewCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HoursBookedNewCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As HoursBookedNewCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As HoursBookedNewCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As HoursBookedNewCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As HoursBookedNewCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.ContractTypeID)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Contract Type"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.DisciplineID)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Discipline"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.TimeSheetMonthID)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.TimeSheetMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Timesheet Month"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

#Region "Skills Database"
  Public Class SkillsDatabase
    Inherits ReportBase(Of SkillsDatabaseCriteria)

    Private Const WorksheetName As String = "Skills Database Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Skills Database"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSkillsDatabasePositions]"
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      Dim pt As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionType").FirstOrDefault
      cmd.Parameters.Remove(pt)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim bhr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhr)
      Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohrl)
      Dim roptl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionTypeList").FirstOrDefault
      cmd.Parameters.Remove(roptl)
      While cmd.Parameters.Count > 0
        cmd.Parameters.Remove(cmd.Parameters.Last)
      End While
      cmd.Parameters.AddWithValue("@HumanResourceID", ZeroNothingDBNull(Me.ReportCriteria.HumanResourceID))
      cmd.Parameters.AddWithValue("@ProductionTypeID", ZeroNothingDBNull(Me.ReportCriteria.ProductionTypeID))
      cmd.Parameters.AddWithValue("@DisciplineID", ZeroNothingDBNull(Me.ReportCriteria.DisciplineID))
      cmd.Parameters.AddWithValue("@PositionTypeID", ZeroNothingDBNull(Me.ReportCriteria.PositionTypeID))
      'cmd.Parameters.AddWithValue("@RoomTypeID", ZeroNothingDBNull(Me.ReportCriteria.RoomTypeID))
      'cmd.Parameters.AddWithValue("@RoomID", ZeroNothingDBNull(Me.ReportCriteria.RoomID))
      cmd.Parameters.AddWithValue("@SystemID", ZeroNothingDBNull(Me.ReportCriteria.SystemID))
      cmd.Parameters.AddWithValue("@ProductionAreaID", ZeroNothingDBNull(Me.ReportCriteria.ProductionAreaID))
    End Sub

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Camera 1", "Camera 2", "Camera 3", "Gunmount", "H/H", "Jib", "Offside",
                                                               "SteadyCam", "Ultramotion"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)
      Dim p = 0
      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, Data.Tables.Item(1).Rows.Count - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      For Col As Integer = 0 To (Data.Tables.Item(1).Rows.Count)
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        If (Col = 0) Then
          ws.Columns(Col).Width = DefaultColumnWidth * 400
        ElseIf (Col > 0 AndAlso Col < Data.Tables.Item(1).Rows.Count) Then
          ws.Columns(Col).Width = DefaultColumnWidth * 200
        Else
          ws.Columns(Col).Width = DefaultColumnWidth * 225
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          ' Each Human resource Row
          If Col = 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, "Human Resource", ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > 0 Then 'First Column
            SetText(ws, Row, Col, Data.Tables.Item(0).Rows.Item(Row - 1)(1), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = 0 Then 'Header Row
            SetText(ws, 0, Col, Data.Tables.Item(1).Rows.Item(Col - 1)(0), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, Data.Tables.Item(1).Rows.Count, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      ' Cell Population
      Dim slider = 0
      Dim SkillHRID = 0
      Dim nCol = 0
      For Row As Integer = 1 To (Data.Tables.Item(0).Rows.Count) ' Human Resources
        Dim HumanResourceID = Data.Tables.Item(0).Rows.Item(Row - 1)(0)
        For Col As Integer = 1 To (Data.Tables.Item(1).Rows.Count) ' Skill Positions
          slider += 1
          Try
            SkillHRID = Data.Tables.Item(2).Rows.Item(slider - 1)(3)
          Catch e As IndexOutOfRangeException
            Exit For
          End Try
          If SkillHRID = HumanResourceID Then
            Try
              SkillHRID = Data.Tables.Item(2).Rows.Item(slider - 1)(3)
              Dim PositionTypeID = Data.Tables.Item(2).Rows.Item(slider - 1)(2) 'Link to headings
              Dim PositionLevelID = Data.Tables.Item(2).Rows.Item(slider - 1)(1)
              For n As Integer = 1 To Data.Tables.Item(1).Rows.Count
                Dim ThisPosType = Data.Tables.Item(1).Rows.Item(n - 1)(1)
                If PositionTypeID = ThisPosType Then
                  nCol = n
                  Exit For
                End If
              Next
              SetText(ws, Row, nCol, PositionLevelID, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            Catch e As IndexOutOfRangeException
            End Try
          Else
            slider -= 1
          End If
        Next
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String,
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default,
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8,
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center,
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None,
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean,
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color,
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color,
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin,
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin,
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer,
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SkillsDatabaseCriteriaControl)
      End Get
    End Property

  End Class

  Public Class SkillsDatabaseCriteria
    Inherits DefaultCriteria

    <Display(Name:="Human Resource", Order:=1)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResource As String

    <Display(Name:="Production Type", Order:=3), Required(ErrorMessage:="Production Type is Required")>
    Public Property ProductionTypeID As Integer?
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) =
    OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Production Type", Order:=4)>
    Public Property ProductionType As String

    <Display(Name:="Disciplines", Order:=5), Required(ErrorMessage:="Discipline is Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Position Type", Order:=6),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROPositionTypeList), UnselectedText:="Position Type")>
    Public Property PositionTypeID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) =
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) =
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False


    '<Display(Name:="Room Type", Order:=9),
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeList), UnselectedText:="Room Type")>
    'Public Property RoomTypeID As Integer?

    '<Display(Name:="Room", Order:=10)>
    'Public Property RoomID As Integer?

    '<Display(Name:="Room", Order:=11)>
    'Public Property Room As String

  End Class

  Public Class SkillsDatabaseCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SkillsDatabaseCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SkillsDatabaseReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "SkillsDatabaseReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Production Types")
                      With .Helpers.Div
                        .Attributes("id") = "ProductionDiv"
                        With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "SkillsDatabaseReport.AddProductionType($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                'With .Helpers.Bootstrap.Row
                '    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                '        With .Helpers.Div
                '            With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SkillsDatabaseCriteria) d.ProductionType, "SkillsDatabaseReport.FindProductionType($element)",
                '                                                   "Search For Production Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                '            End With
                '        End With
                '    End With
                'End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                    With .Helpers.Div
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As SkillsDatabaseCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                          .Editor.Attributes("placeholder") = "Discipline"
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                    With .Helpers.Div
                      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d As SkillsDatabaseCriteria) d.PositionTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                          .Editor.Attributes("placeholder") = "Position Type"
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Human Resources")
                  .Attributes("id") = "HumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.EditorFor(Function(d) d.HumanResource)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Search Human Resource"

                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "HRRates.RefreshHR()")
                            .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
                          End With
                        End With
                      End With
                    End With

                    .Helpers.HTML.NewLine()
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.If(Function(c As SkillsDatabaseCriteria) Not c.BusyHRList)
                            With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                  .Button.AddBinding(KnockoutBindingString.click, "SkillsDatabaseReport.AddHumanResource($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.If(Function(c As SkillsDatabaseCriteria) c.BusyHRList)
                            With .Helpers.Div
                              With .Helpers.HTMLTag("i")
                                .AddClass("fa fa-cog fa-lg fa-spin")
                              End With
                              With .Helpers.HTMLTag("span")
                                .Helpers.HTML("Updating...")

                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Skills Availability Report"
  Public Class SkillsAvailability
    Inherits ReportBase(Of SkillsAvailabilityCriteria)

    Private Const WorksheetName As String = "Skills Availability Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Skills Availability"
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Available", "City", "Disciplines", "Previous Booking Date", "Clash Start Date", "Clash End Date", "Next Booking Date", "Telephone Number", "Alternative Contact Number"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSkillsDatabaseAvailability]"
      Dim param As Object = cmd.Parameters("@Positions")
      cmd.Parameters.Remove(param)

      Dim DPTable As New DataTable
      DPTable.Columns.Add("DisciplineID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      DPTable.Columns.Add("PositionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      For Each dp As DisciplinePosition In Me.ReportCriteria.Positions
        Dim row As DataRow = DPTable.NewRow
        row("DisciplineID") = NothingDBNull(dp.DisciplineID)
        row("PositionID") = NothingDBNull(dp.PositionID)
        DPTable.Rows.Add(row)
      Next
      Dim param2 As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      param2.Name = "@DisciplinePositions"
      param2.SqlType = SqlDbType.Structured
      param2.Value = DPTable
      cmd.Parameters.Add(param2)

      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)
      Dim p = 0
      Dim HeaderName As New HeaderNames
      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      For ColIndex As Integer = 0 To HeaderName.GetCount - 1
        ws.Columns(ColIndex).CellFormat.WrapText = ExcelDefaultableBoolean.True
        If (ColIndex = 0) Then
          'HR Name
          ws.Columns(ColIndex).Width = DefaultColumnWidth * 400
        ElseIf (ColIndex > 0 AndAlso ColIndex < 2) Then
          ws.Columns(ColIndex).Width = DefaultColumnWidth * 200
        Else
          ws.Columns(ColIndex).Width = DefaultColumnWidth * 410
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          'Each Human resource Row
          If ColIndex = 0 AndAlso Row = 0 Then
            'If first row and first column
            SetText(ws, Row, ColIndex, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)

          ElseIf ColIndex = 0 AndAlso Row > 0 Then
            'First Column, any other row
            SetText(ws, Row, ColIndex, Data.Tables(0).Rows.Item(Row - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)

          ElseIf ColIndex > 0 AndAlso Row = 0 Then
            'Any column in header row
            SetText(ws, Row, ColIndex, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim StartDate? As DateTime = Nothing
      Dim EndDate? As DateTime = Nothing
      Dim LastBookingDate? As DateTime = Nothing
      Dim NextBookingDate? As DateTime = Nothing
      Dim TelephoneNumber As String = Nothing
      Dim AlternativeContactNumber As String = Nothing
      Dim City As String = Nothing
      Dim Disciplines As String = Nothing
      For Row As Integer = 1 To (Data.Tables(0).Rows.Count) ' Human Resources Clashes and Non-Clashes
        Dim HumanResourceID = Data.Tables(0).Rows.Item(Row - 1)(1)
        Dim Available = Data.Tables(0).Rows.Item(Row - 1)(2)
        City = Data.Tables(0).Rows.Item(Row - 1)(3)
        TelephoneNumber = Data.Tables(0).Rows.Item(Row - 1)(4)
        AlternativeContactNumber = Data.Tables(0).Rows.Item(Row - 1)(5)
        Disciplines = Data.Tables(0).Rows.Item(Row - 1)(6)
        For Col As Integer = 1 To (HeaderName.GetCount - 1)
          If Available = 0 Then
            Select Case Col
              Case 2
                SetText(ws, Row, Col, City, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              Case 3
                SetText(ws, Row, Col, Disciplines, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              Case 4
                Dim lbd As DateTime? = Nothing
                Dim nbd As DateTime? = Nothing
                For p = 1 To Data.Tables(0).Rows.Count
                  If HumanResourceID = Data.Tables(0).Rows.Item(p - 1)(1) Then
                    StartDate = Data.Tables(0).Rows.Item(p - 1)(8)
                    EndDate = Data.Tables(0).Rows.Item(p - 1)(9)
                    lbd = IIf(IsDBNull(Data.Tables(0).Rows.Item(p - 1)(7)), Nothing, Data.Tables(0).Rows.Item(p - 1)(7))
                    If lbd IsNot Nothing Then
                      LastBookingDate = Convert.ToDateTime(lbd).AddMilliseconds(1)
                    End If
                    nbd = IIf(IsDBNull(Data.Tables(0).Rows.Item(p - 1)(10)), Nothing, Data.Tables(0).Rows.Item(p - 1)(10))
                    If nbd IsNot Nothing Then
                      NextBookingDate = Convert.ToDateTime(nbd).AddMilliseconds(1)
                    End If
                    If LastBookingDate IsNot Nothing Then
                      SetText(ws, Row, Col, LastBookingDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                    End If
                    Exit For
                  End If
                Next
              Case 5
                If StartDate IsNot Nothing Then
                  SetText(ws, Row, Col, StartDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                End If
              Case 6
                If EndDate IsNot Nothing Then
                  SetText(ws, Row, Col, EndDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                End If
              Case 7
                If NextBookingDate IsNot Nothing Then
                  SetText(ws, Row, Col, NextBookingDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                End If
              Case 8
                SetText(ws, Row, Col, TelephoneNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center, False)
              Case 9
                SetText(ws, Row, Col, AlternativeContactNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center, False)
              Case Else
                FormatCells(ws, Col, Col, Row, Row, System.Drawing.Color.LightSalmon, FillPatternStyle.Solid)
                SetText(ws, Row, 1, "No", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            End Select
          Else
            Select Case Col
              Case 2
                SetText(ws, Row, Col, City, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              Case 3
                SetText(ws, Row, Col, Disciplines, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              Case 8
                SetText(ws, Row, Col, TelephoneNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center, False)
              Case 9
                SetText(ws, Row, Col, AlternativeContactNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center, False)
              Case Else
                FormatCells(ws, Col, 1, Row, Row, System.Drawing.Color.LightGreen, FillPatternStyle.Solid)
                SetText(ws, Row, 1, "Yes", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            End Select
          End If
        Next
        StartDate = Nothing
        EndDate = Nothing
        NextBookingDate = Nothing
        LastBookingDate = Nothing
      Next

      'Return MyBase.CreateExcelWorkbook(Data)


      Dim ws2 As Worksheet = wb.Worksheets.Add("New")
      Dim crew As DataTable = Data.Tables.Item(0)
      Dim crewDays As DataTable = Data.Tables.Item(1)
      Dim dayCount As Integer = Data.Tables.Item(2).Rows(0)(0)

      Dim allHeaders As New List(Of String)(New String() {"Human Resource", "City", "Disciplines", "Tel Number", "Alt Number", "Day", "Times"})
      ', "Available", "Day Start", "Day End"

      'Freeze columns and rows
      ws2.DisplayOptions.PanesAreFrozen = True
      ws2.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws2.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add Headers
      For ColIndex As Integer = 0 To allHeaders.Count - 1
        'ws2.Columns(ColIndex).CellFormat.WrapText = ExcelDefaultableBoolean.True
        Dim columnName As String = allHeaders(ColIndex)
        If columnName = "Human Resource" Then
          ws2.Columns(ColIndex).Width = DefaultColumnWidth * 400
          SetText(ws2, 0, ColIndex, columnName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        ElseIf {"City", "Tel Number", "Alt Number"}.Contains(columnName) Then
          ws2.Columns(ColIndex).Width = DefaultColumnWidth * 200
          SetText(ws2, 0, ColIndex, columnName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        ElseIf columnName = "Disciplines" Then
          ws2.Columns(ColIndex).Width = DefaultColumnWidth * 410
          SetText(ws2, 0, ColIndex, columnName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          'ws2.Columns(ColIndex).CellFormat.WrapText = True
        ElseIf {"Day", "Times"}.Contains(columnName) Then
          ws2.Columns(ColIndex).Width = DefaultColumnWidth * 150
          SetText(ws2, 0, ColIndex, columnName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        ', "Available", "Day Start", "Day End"
        'For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
        '  'Each Human resource Row
        '  If ColIndex = 0 AndAlso Row = 0 Then
        '    'If first row and first column
        '    SetText(ws, Row, ColIndex, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)

        '  ElseIf ColIndex = 0 AndAlso Row > 0 Then
        '    'First Column, any other row
        '    SetText(ws, Row, ColIndex, Data.Tables(0).Rows.Item(Row - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)

        '  ElseIf ColIndex > 0 AndAlso Row = 0 Then
        '    'Any column in header row
        '    SetText(ws, Row, ColIndex, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        '  Else 'Column > 0 And Row > 0
        '  End If
        'Next
      Next

      Dim DayStart? As DateTime = Nothing
      Dim DayEnd? As DateTime = Nothing
      Dim DayName As String = ""
      Dim PrevBookingDate? As DateTime = Nothing
      Dim NxtBookingDate? As DateTime = Nothing
      Dim PrimaryNumber As String = ""
      Dim AltNumber As String = ""
      Dim HomeCity As String = ""
      Dim CrewMemberDisciplines As String = ""
      Dim HRID As Integer? = Nothing
      Dim IsAvailable As Boolean = False
      Dim HRName As String = ""

      Dim currentRowIndex As Integer = 1
      'For Each Crew Member
      For crewMemberRowIndex As Integer = 0 To (crew.Rows.Count - 1)
        If crewMemberRowIndex = 0 Then
          currentRowIndex = 1
        End If
        HRID = crew.Rows.Item(crewMemberRowIndex)(1)
        HRName = crew.Rows.Item(crewMemberRowIndex)(0)
        HomeCity = crew.Rows.Item(crewMemberRowIndex)(3)
        PrimaryNumber = crew.Rows.Item(crewMemberRowIndex)(4)
        AltNumber = crew.Rows.Item(crewMemberRowIndex)(5)
        CrewMemberDisciplines = crew.Rows.Item(crewMemberRowIndex)(6)

        'Set the column values for each crew member
        For ColumnIndex As Integer = 0 To (allHeaders.Count - 1)
          Dim columnName As String = allHeaders(ColumnIndex)
          Select Case columnName
            Case "Human Resource"
              SetText(ws2, currentRowIndex, ColumnIndex, HRName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
              FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
              SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
            Case "City"
              SetText(ws2, currentRowIndex, ColumnIndex, HomeCity, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
              FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
              SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
            Case "Disciplines"
              SetText(ws2, currentRowIndex, ColumnIndex, CrewMemberDisciplines, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
              FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
              SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
            Case "Tel Number"
              SetText(ws2, currentRowIndex, ColumnIndex, PrimaryNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center, False)
              FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
              SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
            Case "Alt Number"
              SetText(ws2, currentRowIndex, ColumnIndex, AltNumber, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center, False)
              FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
              SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
          End Select
        Next

        'For Each Crew Member Day
        For Each dayRow As DataRow In crewDays.Rows
          If CompareSafe(dayRow(0), HRID) Then
            'Set the column values for each crew member day
            For ColumnIndex As Integer = 0 To (allHeaders.Count - 1)
              Dim columnName As String = allHeaders(ColumnIndex)
              Select Case columnName
                Case "Day"
                  DayName = dayRow(2)
                  SetText(ws2, currentRowIndex, ColumnIndex, DayName, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
                  IsAvailable = dayRow(3)
                  If IsAvailable Then
                    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, System.Drawing.Color.LightGreen, FillPatternStyle.Solid, "@")
                  Else
                    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, System.Drawing.Color.LightSalmon, FillPatternStyle.Solid, "@")
                  End If
                  SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
                Case "Times"
                  DayStart = OBLib.OBMisc.DBNullNothing(dayRow(4))
                  DayEnd = OBLib.OBMisc.DBNullNothing(dayRow(5))
                  If DayStart IsNot Nothing AndAlso DayEnd IsNot Nothing Then
                    SetText(ws2, currentRowIndex, ColumnIndex, DayStart.Value.ToString("HH:mm") & " - " & DayEnd.Value.ToString("HH:mm"), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
                    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
                  End If
                  SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
                  'Case "Available"
                  '  IsAvailable = dayRow(3)
                  '  If IsAvailable Then
                  '    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, System.Drawing.Color.LightGreen, FillPatternStyle.Solid, "@")
                  '    SetText(ws2, currentRowIndex, ColumnIndex, "Yes", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                  '  Else
                  '    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, System.Drawing.Color.LightSalmon, FillPatternStyle.Solid, "@")
                  '    SetText(ws2, currentRowIndex, ColumnIndex, "No", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                  '  End If
                  '  SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
                  'Case "Day Start"
                  '  DayStart = OBLib.OBMisc.DBNullNothing(dayRow(4))
                  '  If DayStart IsNot Nothing Then
                  '    SetText(ws2, currentRowIndex, ColumnIndex, DayStart.Value.ToString("HH:mm"), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
                  '    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
                  '  End If
                  '  SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
                  'Case "Day End"
                  '  DayEnd = OBLib.OBMisc.DBNullNothing(dayRow(5))
                  '  If DayEnd IsNot Nothing Then
                  '    SetText(ws2, currentRowIndex, ColumnIndex, DayEnd.Value.ToString("HH:mm"), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Left, , VerticalCellAlignment.Center)
                  '    FormatCells(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, Color.White, FillPatternStyle.None, "@")
                  '  End If
                  '  SetCellBorders(ws2, ColumnIndex, ColumnIndex, currentRowIndex, currentRowIndex, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
              End Select
            Next
            currentRowIndex += 1
          End If
        Next

      Next

      Dim totalRows As Integer = crew.Rows.Count * dayCount 'crewDays.Rows.Count

      'Style cell headings
      FormatCells(ws2, 0, allHeaders.Count - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws2, 0, 0, 0, totalRows, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      'Add borders to cells
      'Dim totalRows As Integer = crew.Rows.Count * dayCount 'crewDays.Rows.Count
      'SetCellBorders(ws2, 0, allHeaders.Count - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      'SetCellBorders(ws2, 0, 0, 0, totalRows, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      Return wb

    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String,
                        Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default,
                        Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8,
                        Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center,
                        Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None,
                        Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default,
                        Optional ByVal ConvertToInt As Boolean = True)

      If ConvertToInt Then
        If Integer.TryParse(Text, 0) Then
          ws.Rows(Row).Cells(Column).Value = CInt(Text)
        Else
          ws.Rows(Row).Cells(Column).Value = Text
        End If
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      'dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean,
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color,
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color,
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin,
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin,
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer,
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle, Optional FormatString As String = "")

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FormatOptions = WorksheetCellFormatOptions.All
              If FormatString <> "" Then
                ws.Rows(i).Cells(j).CellFormat.FormatString = FormatString
              End If
          End Select
        Next
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SkillsAvailabilityCriteriaControl)
      End Get
    End Property

  End Class

  Public Class SkillsAvailabilityCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'Public Property CurrentUserSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID
    'Public Property CurrentUserProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    <ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.Where(Function(c) c.SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer)).ToList

    <Display(Name:="Area", Order:=2), Required(ErrorMessage:="Area Required")>
    Public Property ProductionAreaID As Integer?
    <ClientOnly>
    Public Property ROProductionAreaList As List(Of ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.Where(Function(c) c.ProductionAreaID = CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)).ToList

    <Display(Name:="Human Resource", Order:=5)>
    Public Property HumanResourceIDs As List(Of Integer)
    <ClientOnly>
    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)
    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="Production Type", Order:=8)>
    Public Property ProductionTypeIDs As List(Of Integer)
    <ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.FilterList("ProductionTypeID", {OBLib.CommonData.Enums.ProductionType.Cricket,
                                                                                                                                                                                               OBLib.CommonData.Enums.ProductionType.Rugby,
                                                                                                                                                                                               OBLib.CommonData.Enums.ProductionType.Soccer}).ToList

    <Display(Name:="Contract Types", Order:=10)>
    Public Property ContractTypeIDs As List(Of Integer)
    <ClientOnly>
    Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

    <Display(Name:="Discipline", Order:=12), Required(ErrorMessage:="Discipline is Required")>
    Public Property DisciplineIDs As List(Of Integer) = Nothing
    <ClientOnly>
    Public Property RODisciplineList As List(Of ROSystemProductionAreaDisciplineSelect) = New List(Of ROSystemProductionAreaDisciplineSelect)

    <Display(Name:="Position", Order:=12)>
    Public Property Positions As List(Of DisciplinePosition) = New List(Of DisciplinePosition)
    <ClientOnly>
    Public Property ROPositionList As List(Of ROSystemProductionAreaDisciplinePositionType) = New List(Of ROSystemProductionAreaDisciplinePositionType)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class SkillsAvailabilityCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SkillsAvailabilityCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                'With .Helpers.FieldSet("Sub-Depts")
                '  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                '      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                '        .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddSystem($data)")
                '        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                '        .Button.AddClass("btn-block buttontext")
                '      End With
                '    End With
                '  End With
                'End With
                'With .Helpers.Div
                '  With .Helpers.FieldSet("Production Areas")
                '    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                '      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                '        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                '          .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddProductionArea($data)")
                '          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                '          .Button.AddClass("btn-block buttontext")
                '        End With
                '      End With
                '    End With
                '  End With
                'End With
                With .Helpers.FieldSet("Contract Types")
                  With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) d.ROContractTypeList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddContractType($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.FieldSet("Production Types")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddProductionType($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.FieldSet("Disciplines")
                  .Attributes("id") = "Disciplines"
                  With .Helpers.ForEachTemplate(Of ROSystemProductionAreaDiscipline)(Function(d) d.RODisciplineList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddDiscipline($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.FieldSet("Positions")
                  .Attributes("id") = "Position"
                  With .Helpers.ForEachTemplate(Of ROSystemProductionAreaDisciplinePositionTypeSelect)(Function(d) d.ROPositionList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddPosition($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As ROSystemProductionAreaDisciplinePositionTypeSelect) c.Position)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              '  With .Helpers.FieldSet("Human Resources")
              '    .Attributes("id") = "HumanResources"
              '    .AddClass("FadeHide")
              '    With .Helpers.Div
              '      With .Helpers.Div
              '        With .Helpers.Bootstrap.Row
              '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
              '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
              '              .Editor.Attributes("placeholder") = "Search Human Resource"
              '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
              '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : SkillsAvailabilityReport.DelayedRefreshList}")
              '            End With
              '          End With
              '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
              '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "SkillsAvailabilityReport.RefreshHR()")
              '              .Button.AddBinding(KnockoutBindingString.visible, Function(c) c.ROHumanResourceList.Count >= 0)
              '            End With
              '          End With
              '        End With
              '      End With

              '      .Helpers.HTML.NewLine()
              '      With .Helpers.Div
              '        .Attributes("id") = "HRDiv"
              '        With .Helpers.Bootstrap.Row
              '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              '            With .Helpers.If(Function(c As SkillsAvailabilityCriteria) Not c.BusyHRList)
              '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
              '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
              '                    .Button.AddBinding(KnockoutBindingString.click, "SkillsAvailabilityReport.AddHumanResource($data)")
              '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
              '                    .Button.AddClass("btn-block buttontext")
              '                  End With
              '                End With
              '              End With
              '            End With
              '            With .Helpers.If(Function(c As SkillsAvailabilityCriteria) c.BusyHRList)
              '              With .Helpers.Div
              '                With .Helpers.HTMLTag("i")
              '                  .AddClass("fa fa-cog fa-lg fa-spin")
              '                End With
              '                With .Helpers.HTMLTag("span")
              '                  .Helpers.HTML("Updating...")
              '                End With
              '              End With
              '            End With
              '          End With
              '        End With
              '      End With
              '    End With
              '  End With
              '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              '    With .Helpers.FieldSet("Selected Human Resources")
              '      .Attributes("id") = "SelectedHumanResources"
              '      .AddClass("FadeHide")
              '      With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
              '        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '          With .Helpers.Bootstrap.Button("", "", , , , , , , , , )
              '            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
              '            .Button.AddClass("btn-block buttontext")
              '          End With
              '        End With
              '      End With
              '    End With
              '  End With
              'End With
            End With
          End With
        End With
      End With


    End Sub
  End Class

#End Region

#Region "Discipline Bookings"
  Public Class DisciplinesBookedForStudios
    Inherits ReportBase(Of DisciplinesBookedForStudiosCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Discipline Bookings"
      End Get
    End Property

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptDisciplinesBookedForStudios]"
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(e)
      Dim f As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Disciplines").FirstOrDefault
      cmd.Parameters.Remove(f)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        Select Case col.ToString
          Case "HumanResourceID"
            col.ExtendedProperties.Add("AutoGenerate", False)
          Case "RoomScheduleID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(DisciplinesBookedForStudiosCriteriaControl)
      End Get
    End Property

  End Class

  Public Class DisciplinesBookedForStudiosCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=4)>
    Public Property HumanResources As String

    <Display(Name:="Discipline", Order:=5)>
    Public Property DisciplineIDs As List(Of Integer)

    <Display(Name:="Discipline", Order:=6), Required(ErrorMessage:="Discipline is Required")>
    Public Property Disciplines As String

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class DisciplinesBookedForStudiosCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of DisciplinesBookedForStudiosCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As DisciplinesBookedForStudiosCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As DisciplinesBookedForStudiosCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As DisciplinesBookedForStudiosCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButtonNew(Function(d As DisciplinesBookedForStudiosCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As DisciplinesBookedForStudiosCriteria) d.HumanResources, "DisciplinesBookedForStudiosReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "DisciplinesBookedForStudiosReport.ClearHRs()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As DisciplinesBookedForStudiosCriteria) d.Disciplines, "DisciplinesBookedForStudiosReport.GetDiscipline($element)",
                                                       "Search For Disciplines", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "DisciplinesBookedForStudiosReport.ClearDisciplines()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Monthly Schedule By Individual Report"

  Public Class MonthlyScheduleByIndividualCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'Actual Criteria***********************************************************************************************************
    '<Display(Name:="Preferred Name", Order:=3), Required(AllowEmptyStrings:=False, ErrorMessage:="Select Human Resources")>
    'Public Property HumanResourceIDsXML As String = ""    <Display(Name:="Human Resource")>

    Public Property HumanResourceIDs As New List(Of Integer)

    'Lists and other stuff*****************************************************************************************************
    <Display(Name:="Firstname", Order:=3), ClientOnly>
    Public Property FirstName As String = ""

    <Display(Name:="Surname", Order:=3), ClientOnly>
    Public Property Surname As String = ""

    <Display(Name:="Preferred Name", Order:=3), ClientOnly>
    Public Property PreferredName As String = ""

    <Display(Name:="System", Order:=1), ClientOnly>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Area", Order:=2), ClientOnly>
    Public Property ProductionAreaIDs As List(Of Integer)

    '<ClientOnly>
    'Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

    '<ClientOnly>
    'Public Property ROProductionAreaList As List(Of ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.ToList

    <ClientOnly>
    Public Property ROHumanResourceFindList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

    <Display(Name:="Contract Types", Order:=10), ClientOnly>
    Public Property ContractTypeIDList As List(Of Integer)

    Public Property HRSystemID As Integer?

    Public Property HRProductionAreaID As Integer?

    Public Property HRDisciplineID As Integer?

    Public Property HRContractTypeID As Integer?

    <Display(Name:="Disciplines", Order:=5), ClientOnly>
    Public Property RODisciplineList As List(Of RODisciplineReport) = New List(Of RODisciplineReport)

    <ClientOnly>
    Public Property DisciplineIDList As New List(Of Integer)

  End Class

  Public Class MonthlyScheduleByIndividualCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MonthlyScheduleByIndividualCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '.Style.TextAlign = TextAlign.center
              'With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
              '  .Style.Width = "100%"
              '  .Style.TextAlign = TextAlign.left
              'End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
            '  '.Style.TextAlign = TextAlign.center
            '  'With .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
            '  '  .Style.Width = "100%"
            '  '  .Style.TextAlign = TextAlign.left
            '  'End With

            'End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "MonthlyScheduleByIndividualReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "MonthlyScheduleByIndividualReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
              With .Helpers.FieldSet("Disciplines")
                With .Helpers.Bootstrap.Row
                  .Attributes("id") = "disciplines-row"
                  With .Helpers.ForEachTemplate(Of RODisciplineReport)(Function(d) d.RODisciplineList)
                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "MonthlyScheduleByIndividualReport.AddDiscipline($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
              With .Helpers.FieldSet("Contract Types")
                With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) d.ROContractTypeList)
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "MonthlyScheduleByIndividualReport.AddContractType($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.FieldSet("Human Resource Filters")
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MonthlyScheduleByIndividualCriteria) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MonthlyScheduleByIndividualCriteria) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MonthlyScheduleByIndividualCriteria) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "MonthlyScheduleByIndividualReport.RefreshHR()")
                    .Button.AddClass("btn-block")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 9, 10)
              With .Helpers.FieldSet("Human Resources")
                .Attributes("id") = "HumanResources"
                With .Helpers.Bootstrap.Row
                  .Attributes("id") = "HRDiv"
                  With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceFindList)
                    With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                        .Button.AddBinding(KnockoutBindingString.click, "MonthlyScheduleByIndividualReport.AddHumanResource($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class

  Public Class MonthlyScheduleByIndividualReport
    Inherits Singular.Reporting.ReportBase(Of MonthlyScheduleByIndividualCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Monthly Schedule By Individual"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptMonthlyScheduleByIndividual)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptMonthlyScheduleByIndividual"
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MonthlyScheduleByIndividualCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Human Resource Schedule (Excel) Report"

  Public Class HumanResourceScheduleExcelCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'Actual Criteria***********************************************************************************************************
    '<Display(Name:="Preferred Name", Order:=3), Required(AllowEmptyStrings:=False, ErrorMessage:="Select Human Resources")>
    'Public Property HumanResourceIDsXML As String = ""    <Display(Name:="Human Resource")>

    Public Property HumanResourceIDs As New List(Of Integer)

    'Lists and other stuff*****************************************************************************************************
    <Display(Name:="Firstname", Order:=3), ClientOnly>
    Public Property FirstName As String = ""

    <Display(Name:="Surname", Order:=3), ClientOnly>
    Public Property Surname As String = ""

    <Display(Name:="Preferred Name", Order:=3), ClientOnly>
    Public Property PreferredName As String = ""

    <Display(Name:="System", Order:=1), ClientOnly>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Area", Order:=2), ClientOnly>
    Public Property ProductionAreaIDs As List(Of Integer)

    '<ClientOnly>
    'Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

    '<ClientOnly>
    'Public Property ROProductionAreaList As List(Of ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.ToList

    <ClientOnly>
    Public Property ROHumanResourceFindList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="RO Contract Types List"), ClientOnly>
    Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

    <Display(Name:="Contract Types", Order:=10), ClientOnly>
    Public Property ContractTypeIDList As List(Of Integer)

    Public Property HRSystemID As Integer?

    Public Property HRProductionAreaID As Integer?

    Public Property HRDisciplineID As Integer?

    Public Property HRContractTypeID As Integer?

    <Display(Name:="Disciplines", Order:=5), ClientOnly>
    Public Property RODisciplineList As List(Of RODisciplineReport) = New List(Of RODisciplineReport)

    <ClientOnly>
    Public Property DisciplineIDList As New List(Of Integer)

  End Class

  Public Class HumanResourceScheduleExcelCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceScheduleExcelCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '.Style.TextAlign = TextAlign.center
              'With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
              '  .Style.Width = "100%"
              '  .Style.TextAlign = TextAlign.left
              'End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
            '  '.Style.TextAlign = TextAlign.center
            '  'With .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
            '  '  .Style.Width = "100%"
            '  '  .Style.TextAlign = TextAlign.left
            '  'End With

            'End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "HumanResourceScheduleExcelReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HumanResourceScheduleExcelReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 8, 8)
              With .Helpers.FieldSet("Disciplines")
                With .Helpers.Bootstrap.Row
                  .Attributes("id") = "disciplines-row"
                  With .Helpers.ForEachTemplate(Of RODisciplineReport)(Function(d) d.RODisciplineList)
                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HumanResourceScheduleExcelReport.AddDiscipline($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 2, 2)
              With .Helpers.FieldSet("Contract Types")
                With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) d.ROContractTypeList)
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "HumanResourceScheduleExcelReport.AddContractType($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.FieldSet("Human Resource Filters")
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceScheduleExcelCriteria) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceScheduleExcelCriteria) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceScheduleExcelCriteria) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "MonthlyScheduleByIndividualReport.RefreshHR()")
                    .Button.AddClass("btn-block")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 9, 10)
              With .Helpers.FieldSet("Human Resources")
                .Attributes("id") = "HumanResources"
                With .Helpers.Bootstrap.Row
                  .Attributes("id") = "HRDiv"
                  With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceFindList)
                    With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                        .Button.AddBinding(KnockoutBindingString.click, "HumanResourceScheduleExcelReport.AddHumanResource($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class

  Public Class HumanResourceScheduleExcelReport
    Inherits Singular.Reporting.ReportBase(Of HumanResourceScheduleExcelCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Schedule (Excel)"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As System.Type
    '  Get
    '    Return GetType(rptMonthlyScheduleByIndividual)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceScheduleExcel"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceScheduleExcelCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Human Resource (For Accreditation) "

  Public Class HumanResourceAccreditationReport
    Inherits ReportBase(Of HumanResourceAccreditationCriteria)

    Private Enum ColumnType
      Name = 0
      IDNo = 1
      HumanResourceID = 2
      Photo = 3
      Discipline = 4
    End Enum

    Private Const mDefaultWidth As Integer = 413
    Private Const mDefaultHeight As Integer = 531

    Private mHeight As Integer?
    Private mWidth As Integer?
    Private Const DefaultColumnWidth As Decimal = 17.5

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Accreditation Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceAccreditationCriteriaControl)
      End Get
    End Property


    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHumanResource]"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook
      Dim wb As New Infragistics.Documents.Excel.Workbook(Infragistics.Documents.Excel.WorkbookFormat.Excel2007)
      Dim ws = wb.Worksheets.Add("Human Resources")
      Try
        ' add headers
        ws.Rows(0).Cells(ColumnType.Name).Value = "Names"
        ws.Rows(0).Cells(ColumnType.IDNo).Value = "ID Numbers"
        ws.Rows(0).Cells(ColumnType.Discipline).Value = "Discipline"
        ws.Rows(0).Cells(ColumnType.Photo).Value = "Photos"
        ws.Rows(0).CellFormat.Font.Bold = Infragistics.Documents.Excel.ExcelDefaultableBoolean.True
        ws.Columns(0).Width = DefaultColumnWidth * 600
        ws.Columns(1).Width = DefaultColumnWidth * 200

        Dim RowIndex As Integer = 1
        For Each drw As DataRow In Data.Tables("Table").Rows
          ws.Rows(RowIndex).Cells(ColumnType.Name).Value = drw("HumanResourceName")
          ws.Rows(RowIndex).Cells(ColumnType.IDNo).Value = drw("IDNo")
          ws.Rows(RowIndex).Cells(ColumnType.IDNo).Value = drw("Discipline")
          ws.Rows(RowIndex).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
          AddImageCell(ws, RowIndex, drw("HumanResourceID").ToString)
          RowIndex += 1
          GC.Collect()
        Next
      Catch ex As Exception
      End Try
      Return wb
    End Function

    Private Sub AddImageCell(Worksheet As Worksheet, RowIndex As Integer, HumanResourceID As String)

      Dim Image As Image = Nothing

      mHeight = If(IsNullNothing(ZeroDBNull(mHeight)), mDefaultHeight, mHeight)
      mWidth = If(IsNullNothing(ZeroDBNull(mWidth)), mDefaultWidth, mWidth)

      Dim CurrentPath As String = AppDomain.CurrentDomain.BaseDirectory 'Get the directory where the application is located.
      Dim SourcePath As String = CurrentPath + "\Images\Accreditation\" + HumanResourceID + "_3.jpg"
      If System.IO.File.Exists(SourcePath) Then
        Using Original As New Bitmap(SourcePath)
          Image = ResizeImage(Original, New Size(mWidth, mHeight), False)
        End Using

        Dim img = New WorksheetImage(Image)
        img.TopLeftCornerCell = Worksheet.Rows(RowIndex).Cells(ColumnType.Photo)
        img.BottomRightCornerCell = Worksheet.Rows(RowIndex).Cells(ColumnType.Photo)
        img.TopLeftCornerPosition = New PointF(0.0F, 0.0F)
        img.BottomRightCornerPosition = New PointF(100.0F, 100.0F)

        Worksheet.Rows(RowIndex).Height = Image.Height * 10

        If Worksheet.Columns(ColumnType.Photo).Width < Image.Width * 20 Then
          Worksheet.Columns(ColumnType.Photo).Width = Image.Width * 20
        End If

        img.SetBoundsInTwips(Worksheet, img.GetBoundsInTwips(), True)
        Worksheet.Shapes.Add(img)
      End If

    End Sub

    Public Shared Function ResizeImage(ByVal image As Image, ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image

      Dim newWidth As Integer
      Dim newHeight As Integer
      If preserveAspectRatio Then
        Dim originalWidth As Integer = image.Width
        Dim originalHeight As Integer = image.Height
        Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
        Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
        Dim percent As Single = If(percentHeight < percentWidth, percentHeight, percentWidth)
        newWidth = CInt(originalWidth * percent)
        newHeight = CInt(originalHeight * percent)
      Else
        newWidth = size.Width
        newHeight = size.Height
      End If

      Dim newImage As Image = New Bitmap(newWidth, newHeight)

      Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
        graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
        graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
      End Using
      Return newImage

    End Function

    Public Class HumanResourceAccreditationCriteria
      Inherits DefaultCriteria

      <Display(Name:="Human Resource", Order:=1)>
      Public Property HumanResourceIDs As List(Of Integer)

      <Display(Name:="Sub-Dept", Order:=3)>
      Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

      <Display(Name:="Production Area ID")>
      Public Property ProductionAreaID As Integer?

      <Display(Name:="Discipline ID")>
      Public Property DisciplineID As Integer?

      <Display(Name:="Firstname")>
      Public Overridable Property FirstName As String

      <Display(Name:="Surname")>
      Public Overridable Property Surname As String

      <Display(Name:="Pref. Name")>
      Public Overridable Property PreferredName As String

      <Display(Name:="Read Only Human Resource List", Order:=5), ClientOnly>
      Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

      <Display(Name:="Human Resources List", Order:=5), ClientOnly>
      Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

      <Display(Name:="Select All", Order:=5), ClientOnly>
      Public Property SelectAllInd As Boolean = False

    End Class

    Public Class HumanResourceAccreditationCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.With(Of HumanResourceAccreditationCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                    With .Helpers.FieldSet("Sub-Depts and Areas")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "HumanResourceAccreditationReport.AddSystem($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                          .Button.AddClass("btn-block buttontext")
                        End With
                        With .Helpers.Bootstrap.Row
                          .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                            With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                              .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                .Button.AddBinding(KnockoutBindingString.click, "HumanResourceAccreditationReport.AddProductionArea($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                    With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, BootstrapEnums.InputSize.Small, , "Discipline")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.SelectAllInd, "Select All", "Select All", , , , , )
                            .Button.AddBinding(KnockoutBindingString.click, "HumanResourceAccreditationReport.AddAllHR($data)")
                            .Button.AddClass("btn-block")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "HumanResourceAccreditationReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.FieldSet("Selected Human Resources")
                      .Attributes("id") = "SelectedHumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
                        With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , , )
                            .Button.AddBinding(KnockoutBindingString.click, "HumanResourceAccreditationReport.AddHumanResource($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

  End Class

#End Region

#Region " Print HR Timesheet "

  Public Class PrintHRTimesheet
    Inherits Singular.Reporting.ReportBase(Of PrintHRTimesheetCriteria)


    Public Class PrintHRTimesheetCriteria
      Inherits Singular.Reporting.EndDateReportCriteria

      <Display(Name:="Manager"), DropDownWeb(GetType(ROManagerList), DisplayMember:="HRName", ValueMember:="HumanResourceID"),
      Required(ErrorMessage:="Manager is required")>
      Public Property ManagerHumanResourceID As Integer?

      <Display(Name:="Contract Type"), DropDownWeb(GetType(ROContractTypeList))>
      Public Property ContractTypeID As Integer?

    End Class

    Public Class HRTimesheetCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of PrintHRTimesheetCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 6, 5, 4, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 6, 7, 8, 9)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.ManagerHumanResourceID).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ManagerHumanResourceID, BootstrapEnums.InputSize.Small, , "Select Manager...")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                      .Helpers.Bootstrap.LabelFor(Function(d) d.ContractTypeID).Style.Width = "100%"
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type...")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Print HR Timesheets"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRTimesheetCriteriaControl)
      End Get
    End Property

    Public Overrides Function GetDocumentFile(DocumentType As ReportDocumentType) As ReportFileInfo
      Dim Files As New List(Of ReportFileInfo)

      Dim ROTimesheetMonth As ROTimesheetMonth = OBLib.CommonData.Lists.ROTimesheetMonthList.GetItemByDate(ReportCriteria.EndDate)
      Dim OBCityTimesheets As OBCityTimesheetList = OBCityTimesheetList.GetOBCityTimesheetList(Nothing, ROTimesheetMonth.TimesheetMonthID,
                                                                                               OBLib.Security.Settings.CurrentUser.SystemID,
                                                                                               OBLib.Security.Settings.CurrentUser.ProductionAreaID,
                                                                                               ReportCriteria.ManagerHumanResourceID,
                                                                                               ReportCriteria.ContractTypeID)

      For Each obct As OBCityTimesheet In OBCityTimesheets
        Dim ds As DataSet = GetDataSet(ROTimesheetMonth, obct)
        Dim newRpt As New OBWebReports.HumanResourceReports.HRTimesheet
        Dim newFileInfo As Singular.Reporting.ReportFileInfo = Nothing
        newRpt.SetDataSet(ds)
        newFileInfo = newRpt.GetDocumentFile(Singular.Reporting.ReportDocumentType.PDF)
        newFileInfo.FileName = obct.HumanResource.Trim.Replace(" ", "") & ".pdf"
        Files.Add(newFileInfo)
      Next

      Dim RetInfo As New ReportFileInfo
      Dim FileBytes As Byte()() = Files.Select(Function(d) d.FileBytes).ToArray()
      Dim FileNames As String() = Files.Select(Function(d) d.FileName).ToArray()
      Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes, FileNames)
      Dim ms As New IO.MemoryStream(CompressedFiles)
      RetInfo.FileStream = ms
      RetInfo.FileName = "Timesheets.zip"
      Return RetInfo

    End Function

    Public Function GetDataSet(ROTimesheetMonth As ROTimesheetMonth, OBCityTimesheet As OBCityTimesheet) As DataSet

      If ROTimesheetMonth IsNot Nothing Then
        Dim CalculationType As OBLib.CommonData.Enums.PublicHolidayCalculationType = OBCityTimesheet.GetPublicHolidayHourCalculationType(ROTimesheetMonth.StartDate)
        Dim UseNewReport As Boolean = False
        If CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.Original OrElse CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.UpToMidnight Then
          UseNewReport = False
        ElseIf CalculationType = OBLib.CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent Then
          UseNewReport = True
        End If
        Dim d As DSHRTimesheet = HRTimesheetDataset(OBCityTimesheet, CDate(ReportCriteria.EndDate), UseNewReport)
        Return d
      Else
        Dim n As DSHRTimesheet = HRTimesheetDataset(Nothing, CDate(ReportCriteria.EndDate), False)
        Return n
      End If

    End Function

    Private Function HRTimesheetDataset(OBCityTimesheet As OBLib.Timesheets.OBCity.OBCityTimesheet, Month As Csla.SmartDate, UseNewReport As Boolean) As DSHRTimesheet

      Dim ROHumanResource As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(OBCityTimesheet.HumanResourceID)

      Dim ds As DSHRTimesheet = New DSHRTimesheet

      If OBCityTimesheet IsNot Nothing Then
        '-------------Human Resource---------------------------------------
        Dim hr As DataTable = ds.HumanResource
        Dim d As DataRow = hr.NewRow
        d.Item("HumanResource") = ROHumanResource.Firstname + " " + ROHumanResource.Surname
        d.Item("IDNo") = ROHumanResource.IDNo
        d.Item("StaffNo") = ROHumanResource.EmployeeCode
        d.Item("TimesheetHasChangesFromOtherMonthsInd") = OBCityTimesheet.CrewTimesheetList.TimesheetHasChangesFromOtherMonthsInd
        d.Item("UseNewReport") = UseNewReport
        hr.Rows.Add(d)

        '-------------Timesheet Rows---------------------------------------
        Dim ctl As DataTable = ds.CrewTimesheets
        OBCityTimesheet.CrewTimesheetList.DoCalculations()
        For Each ct As OBLib.Timesheets.OBCity.CrewTimesheet In OBCityTimesheet.CrewTimesheetList
          If ct.LastDayPreviousMonthInd = False Then
            Dim ctr As DataRow = ctl.NewRow
            ctr.Item("DayOfMonth") = ct.CalculatedStartDateTime
            ctr.Item("DayOfWeek") = ct.CalculatedStartDateTime
            ctr.Item("PHInd") = ct.IsPublicHoliday
            ctr.Item("Description") = ct.Description
            ctr.Item("CrewStartDateTime") = ct.CrewStartDateTime
            ctr.Item("CrewEndDateTime") = ct.CrewEndDateTime
            ctr.Item("HoursForDay") = ct.HoursForDay
            ctr.Item("TotalHoursForMonth") = ct.HoursForMonth
            ctr.Item("HoursAboveOTLimit") = ct.HoursAboveOTLimit
            ctr.Item("ShortfallHours") = ct.ShortfallHours
            ctr.Item("TotalShortfall") = ct.ShortfallForMonth
            ctr.Item("PHHours") = ct.PublicHolidayHours
            ctl.Rows.Add(ctr)
          End If
        Next

        '-------------Summaries---------------------------------------
        Dim summ As DataTable = ds.SummariesNew
        Dim summR As DataRow = summ.NewRow

        summR.Item("UnadjustedHours") = OBCityTimesheet.UnadjustedHours
        summR.Item("UnadjustedPlanningHours") = OBCityTimesheet.UnadjustedPlanningHours
        summR.Item("UnadjustedTotal") = OBCityTimesheet.UnadjustedTotal
        summR.Item("UnadjustedRequiredHours") = OBCityTimesheet.UnadjustedRequiredHours
        summR.Item("UnadjustedPublicHolidayHours") = OBCityTimesheet.UnadjustedPublicHolidayHours
        summR.Item("UnadjustedPublicHolidayHoursAdjusted") = OBCityTimesheet.UnadjustedPublicHolidayHoursAdjusted
        summR.Item("UnadjustedOvertime") = OBCityTimesheet.UnadjustedOvertime
        summR.Item("UnadjustedShortfall") = OBCityTimesheet.UnadjustedShortfall
        summR.Item("UnadjustedTotalOvertimeAndShortfall") = OBCityTimesheet.UnadjustedTotalOvertimeAndShortfall
        summR.Item("NormalHoursAdjustment") = OBCityTimesheet.NormalHoursAdjustment
        summR.Item("PlanningHoursAdjustment") = OBCityTimesheet.PlanningHoursAdjustment
        summR.Item("TotalAdjustment") = OBCityTimesheet.TotalAdjustment
        summR.Item("RequiredHoursAdjustment") = OBCityTimesheet.RequiredHoursAdjustment
        summR.Item("PublicHolidayHoursAdjustment") = OBCityTimesheet.PublicHolidayHoursAdjustment
        summR.Item("PublicHolidayHoursAdjustedAdjustment") = OBCityTimesheet.PublicHolidayHoursAdjustedAdjustment
        summR.Item("OvertimeAdjustment") = OBCityTimesheet.OvertimeAdjustment
        summR.Item("ShortfallAdjustment") = OBCityTimesheet.ShortfallAdjustment
        summR.Item("TotalOvertimeAndShortfallAdjustment") = OBCityTimesheet.TotalOvertimeAndShortfallAdjustment
        summR.Item("FinalHours") = OBCityTimesheet.FinalHours
        summR.Item("FinalPlanningHours") = OBCityTimesheet.FinalPlanningHours
        summR.Item("FinalTotal") = OBCityTimesheet.FinalTotal
        summR.Item("FinalRequiredHours") = OBCityTimesheet.FinalRequiredHours
        summR.Item("FinalPublicHolidayHours") = OBCityTimesheet.FinalPublicHolidayHours
        summR.Item("FinalPublicHolidayHoursAdjusted") = OBCityTimesheet.FinalPublicHolidayHoursAdjusted
        summR.Item("FinalOvertime") = OBCityTimesheet.FinalOvertime
        summR.Item("FinalShortfall") = OBCityTimesheet.FinalShortfall
        summR.Item("FinalTotalOvertimeAndShortfall") = OBCityTimesheet.FinalTotalOvertimeAndShortfall

        summR.Item("PublicHolidaysTotal") = OBCityTimesheet.PublicHolidaysTotal
        summR.Item("PublicHolidaysUnderLimit") = OBCityTimesheet.PublicHolidaysUnderLimit
        summR.Item("PublicHolidaysOverLimit") = OBCityTimesheet.PublicHolidaysOverLimit
        summR.Item("OvertimeLessPublicHolidaysOverLimit") = OBCityTimesheet.OvertimeLessPublicHolidaysOverLimit
        summR.Item("FinalPublicHolidaysUnderLimit") = OBCityTimesheet.FinalPublicHolidaysUnderLimit
        summR.Item("FinalPublicHolidaysOverLimit") = OBCityTimesheet.FinalPublicHolidaysOverLimit

        'summR.Item("UnadjustedHours") = CrewTimesheetList.GetTotalHours
        'summR.Item("UnadjustedPlanningHours") = CrewTimesheetList.GetPlanningHours
        'summR.Item("UnadjustedTotalPlanningAndHours") = CrewTimesheetList.GetHoursAndPlanning
        'summR.Item("UnadjustedRequiredWorkingHours") = CommonData.Lists.TimesheetMonthList.GetItem(Month.Date).HoursBeforeOvertime
        'summR.Item("UnadjustedOvertime") = CrewTimesheetList.GetTotalOvertime
        'summR.Item("UnadjustedShortfall") = CrewTimesheetList.GetTotalShortfall
        'summR.Item("UnadjustedTimeForMonth") = CrewTimesheetList.GetFinalTimeForMonth
        'summR.Item("UnadjustedPublicHolidayHours") = (CrewTimesheetList.GetTotalPublicHolidayHours)
        'summR.Item("UnadjustedAdjustedPublicHolidayHours") = (CrewTimesheetList.GetAdjustedPHHours)


        'summR.Item("TotalOvertimeShortfall") = (CrewTimesheetList.GetFinalOverTime)


        'summR.Item("NormalHourAdjustments") = (CrewTimesheetList.GetTotalNormalHourAdjustments)
        'summR.Item("PlanningHourAdjustments") = (CrewTimesheetList.GetTotalPlanningHourAdjustments)
        'summR.Item("OvertimeAdjustments") = (CrewTimesheetList.GetTotalOvertimeHourAdjustments)
        'summR.Item("ShortfallAdjustments") = (CrewTimesheetList.GetTotalShortfallHourAdjustments)
        'summR.Item("PublicHolidayAdjustments") = (CrewTimesheetList.GetTotalPublicHolidayHourAdjustments)

        summ.Rows.Add(summR)


        ''-------------Planning Hours---------------------------------------
        'Dim emPlanningHourList As OBLib.Timesheets.OBCity.ReadOnly.ROEventManagerPlanningHourList = OBLib.Timesheets.OBCity.ReadOnly.ROEventManagerPlanningHourList.GetROEventManagerPlanningHourList(Month, HumanResourceID)
        'Dim planHours As DataTable = ds.ProductionPlanningHours
        'For Each pplhr In emPlanningHourList
        '  Dim planHR As DataRow = planHours.NewRow
        '  planHR.Item("ProductionID") = pplhr.ProductionID
        '  planHR.Item("Description") = pplhr.ProdDescription
        '  planHR.Item("TotalPlanningHours") = pplhr.Hours
        '  planHours.Rows.Add(planHR)
        'Next


        '-------------Timesheet Adjustment----------------------------------
        'Dim timesheetAdjustList As OBLib.Timesheets.OBCity.TimesheetAdjustmentList = OBLib.Timesheets.OBCity.TimesheetAdjustmentList.GetAdjustmentsToReflectInCurrentMonth(HumanResourceID, 0, Month)
        Dim tsAdjust As DataTable = ds.TimesheetAdjustments
        For Each ta In OBCityTimesheet.TimesheetAdjustmentList
          Dim timesheetAdjust As DataRow = tsAdjust.NewRow
          timesheetAdjust.Item("TimesheetHourType") = ta.TimesheetHourType
          timesheetAdjust.Item("Adjustment") = ta.Adjustment
          timesheetAdjust.Item("AdjustmentReason") = ta.AdjustmentReason
          'timesheetAdjust.Item("ProductionRefNo") = ta.ProductionRefNo
          tsAdjust.Rows.Add(timesheetAdjust)
        Next


        '-------------Timesheet Month---------------------------------------
        Dim tMonth As Timesheets.ReadOnly.ROTimesheetMonth = OBLib.CommonData.Lists.ROTimesheetMonthList.GetItemByDate(Month.Date)
        Dim tmTable As DataTable = ds.TimesheetMonth
        Dim tmRow As DataRow = tmTable.NewRow
        tmRow.Item("TimesheetMonth") = tMonth.Description
        tmTable.Rows.Add(tmRow)
      End If

      Return ds

    End Function

  End Class

#End Region

#Region " Stage Hand Schedule "

  Public Class StageHandSchedule
    Inherits ReportBase(Of StageHandScheduleCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Stage Hand Schedule"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StageHandScheduleCriteriaControl)
      End Get
    End Property


    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)


    End Sub


    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptStageHandsBookingReport]"
      cmd.CommandTimeout = 0
    End Sub


    Public Class StageHandScheduleCriteria
      Inherits Singular.Reporting.StartAndEndDateReportCriteria

      <Display(Name:="Human Resource")>
      Public Property HumanResourceIDs As List(Of Integer)

      <Display(Name:="Sub-Dept"), ClientOnly>
      Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

      <Display(Name:="Production Area ID"), ClientOnly>
      Public Property ProductionAreaID As Integer?

      <Display(Name:="Read Only Human Resource List", Order:=5), ClientOnly>
      Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

      <Display(Name:="Human Resources List", Order:=5), ClientOnly>
      Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    End Class

    Public Class StageHandScheduleCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.With(Of StageHandScheduleCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          'With .Helpers.FieldSet("Criteria")
          '  With .Helpers.Bootstrap.Row
          '    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
          '      With .Helpers.Bootstrap.Row
          '        With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
          '          With .Helpers.FieldSet("Sub-Depts and Areas")
          '            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
          '              With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
          '                .Button.AddBinding(KnockoutBindingString.click, "StageHandScheduleReport.AddSystem($data)")
          '                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
          '                .Button.AddClass("btn-block buttontext")
          '              End With
          '              With .Helpers.Bootstrap.Row
          '                .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
          '                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
          '                  With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
          '                    .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
          '                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
          '                      .Button.AddBinding(KnockoutBindingString.click, "StageHandScheduleReport.AddProductionArea($data)")
          '                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
          '                      .Button.AddClass("btn-block buttontext")
          '                    End With
          '                  End With
          '                End With
          '              End With
          '            End With
          '          End With
          '        End With
          '        With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
          '          With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
          '            With .Helpers.FieldSet("Human Resource Filters")
          '              .Attributes("id") = "HumanResourceFilters"
          '              .AddClass("FadeHide")
          '              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '                With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
          '                End With
          '              End With
          '              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
          '                End With
          '              End With
          '              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
          '                With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
          '                End With
          '              End With
          '            End With
          '          End With
          '        End With
          '        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
          '          With .Helpers.FieldSet("Human Resources")
          '            .Attributes("id") = "HumanResources"
          '            .AddClass("FadeHide")
          '            With .Helpers.Bootstrap.Row
          '              .Attributes("id") = "HRDiv"
          '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
          '                With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
          '                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
          '                    .Button.AddBinding(KnockoutBindingString.click, "StageHandScheduleReport.AddHumanResource($data)")
          '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
          '                    .Button.AddClass("btn-block buttontext")
          '                  End With
          '                End With
          '              End With
          '            End With
          '          End With
          '          With .Helpers.FieldSet("Selected Human Resources")
          '            .Attributes("id") = "SelectedHumanResources"
          '            .AddClass("FadeHide")
          '            With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.HumanResources)
          '              With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
          '                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , , )
          '                  .Button.AddBinding(KnockoutBindingString.click, "StageHandScheduleReport.AddHumanResource($data)")
          '                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
          '                  .Button.AddClass("btn-block buttontext")
          '                End With
          '              End With
          '            End With
          '          End With
          '        End With
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

  End Class

#End Region

#Region " Timesheet (New Policy) "

  Public Class TimesheetNewPolicy
    Inherits Singular.Reporting.ReportBase(Of TimesheetNewPolicyCriteria)

    Public Class TimesheetNewPolicyCriteria
      Inherits Singular.Reporting.ReportCriteria

      <ClientOnly>
      Public Property MinDate As Date = New Date(2017, 7, 1)
      <ClientOnly>
      Public Property MaxDate As Date = New Date(2018, 3, 31)

      <Display(Name:="Month"),
      DateField(AlwaysShow:=True, MinDateProperty:="MinDate", MaxDateProperty:="MaxDate"),
      Required(ErrorMessage:="Month is required")>
      Public Property Month As Date?

      <Display(Name:="Sub-Dept"), RequiredIf(ConditionLogicJS:="!self.IgnoreSystemFilter()")>
      Public Property SystemID As Integer?

      <Display(Name:="All Sub-Depts")>
      Public Property IgnoreSystemFilter As Boolean = False

    End Class

    Public Class TimesheetNewPolicyCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of TimesheetNewPolicyCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.Month)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.Month, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "TimesheetNewPolicyReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
              If OBLib.Security.Settings.CurrentUserID = 1 Then
                With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                  With .Helpers.Bootstrap.StateButtonNew("IgnoreSystemFilter", "Ignore Sub-Dept", "Dont Ignore Sub-Dept", "btn-default", "btn-success",,, "btn-md")
                  End With
                End With
              End If
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Timesheet (New Policy)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptTimesheetsNewPolicy]"
      cmd.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TimesheetNewPolicyCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Dim ws2 As Worksheet = Workbook.Worksheets(0)

    End Sub

  End Class

#End Region

#Region " Timesheet (New Policy) - Playout "

  Public Class TimesheetNewPolicyPlayout
    Inherits Singular.Reporting.ReportBase(Of TimesheetNewPolicyPlayoutCriteria)


    Public Class TimesheetNewPolicyPlayoutCriteria
      Inherits Singular.Reporting.ReportCriteria

      <ClientOnly>
      Public Property MinDate As Date = New Date(2017, 1, 1)
      <ClientOnly>
      Public Property MaxDate As Date = New Date(2018, 3, 31)

      <Display(Name:="Month"),
      DateField(AlwaysShow:=True, MinDateProperty:="MinDate", MaxDateProperty:="MaxDate"),
      Required(ErrorMessage:="Month is required")>
      Public Property Month As Date?

      <Display(Name:="Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer?

    End Class

    Public Class TimesheetNewPolicyPlayoutCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of TimesheetNewPolicyPlayoutCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.Month)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.Month, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "TimesheetNewPolicyReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Timesheet (New Policy) - Playout"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptTimesheetsNewPolicyPlayout]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TimesheetNewPolicyPlayoutCriteriaControl)
      End Get
    End Property

    'Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
    '  RowCount = Me.DataSet.Tables.Item(0).Rows.Count
    'End Sub

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Dim ws As Worksheet = Workbook.Worksheets(0)

      'Formatting the BREAKS between sections (HEADING LINE ONLY)
      ws.Rows.Item(1).Cells.Item(15).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
      ws.Columns(15).Width = 1829
      ws.Rows.Item(1).Cells.Item(16).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))
      ws.Rows.Item(1).Cells.Item(35).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
      ws.Columns(35).Width = 1829
      ws.Rows.Item(1).Cells.Item(36).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))
      ws.Rows.Item(1).Cells.Item(47).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
      ws.Columns(47).Width = 1829
      ws.Rows.Item(1).Cells.Item(48).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))

      'Formatting the AMOUNT (Rand value) columns
      ws.Columns(14).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(20).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(22).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(23).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(27).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(32).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(33).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(34).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(37).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(39).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(40).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(41).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(42).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(43).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(44).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(45).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(46).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(49).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(50).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws.Columns(51).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"

      For Each Row As WorksheetRow In ws.Rows.Skip(1)

        If (String.IsNullOrWhiteSpace(Row.GetCellValue(1))) And (String.IsNullOrWhiteSpace(Row.GetCellValue(2))) Then
          Exit For
        Else
          'apply formulas
          ws.Rows(Row.Index).Cells(27).ApplyFormula("=Sum((AA" + (Row.Index + 1).ToString + "+ Y" + (Row.Index + 1).ToString + ") * O" + (Row.Index + 1).ToString + ")") 'Public Holiday Amount
          ws.Rows(Row.Index).Cells(32).ApplyFormula("=Sum(AD" + (Row.Index + 1).ToString + " * O" + (Row.Index + 1).ToString + ")") 'Overtime Amount (x1.5)
          ws.Rows(Row.Index).Cells(33).ApplyFormula("=Sum(AF" + (Row.Index + 1).ToString + " * O" + (Row.Index + 1).ToString + ")") 'Overtime Amount (x2)
          '---REMOVED COLUMN---ws.Rows(Row.Index).Cells(36).ApplyFormula("=Sum(AG" + (Row.Index + 1).ToString + " + AH" + (Row.Index + 1).ToString + "+ AB" + (Row.Index + 1).ToString + "- X" + (Row.Index + 1).ToString + ")") 'Total Overtime Less Allowance
          ws.Rows(Row.Index).Cells(34).ApplyFormula("=Sum(X" + (Row.Index + 1).ToString + "+ AB" + (Row.Index + 1).ToString + "+ AG" + (Row.Index + 1).ToString + "+ AH" + (Row.Index + 1).ToString + ")") 'Total New Policy Amount
          ws.Rows(Row.Index).Cells(39).ApplyFormula("=Sum(AM" + (Row.Index + 1).ToString + "* AL" + (Row.Index + 1).ToString + ")") 'Total P/H Amount
          ws.Rows(Row.Index).Cells(46).ApplyFormula("=Sum(AT" + (Row.Index + 1).ToString + "+ AN" + (Row.Index + 1).ToString + ")") 'Total Overtime Payable
          ws.Rows(Row.Index).Cells(49).ApplyFormula("=Sum(X" + (Row.Index + 1).ToString + "- AS" + (Row.Index + 1).ToString + ")") 'New Policy Allowance vs Old Policy Allowance
          ws.Rows(Row.Index).Cells(50).ApplyFormula("=Sum(AI" + (Row.Index + 1).ToString + "- AU" + (Row.Index + 1).ToString + ")") 'Total difference, New Policy vs Old Policy
          ws.Rows(Row.Index).Cells(51).ApplyFormula("=Sum(AY" + (Row.Index + 1).ToString + "- AX" + (Row.Index + 1).ToString + ")") 'Total Overtime less Allowance difference  ((=SUM(AG + AH + AB - X)

          'Formatting the BREAKS between sections
          ws.Rows.Item(Row.Index + 1).Cells.Item(15).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
          ws.Columns(15).Width = 1829
          ws.Rows.Item(Row.Index + 1).Cells.Item(16).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))
          ws.Rows.Item(Row.Index + 1).Cells.Item(35).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
          ws.Columns(35).Width = 1829
          ws.Rows.Item(Row.Index + 1).Cells.Item(36).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))
          ws.Rows.Item(Row.Index + 1).Cells.Item(47).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 255, 255))
          ws.Columns(47).Width = 1829
          ws.Rows.Item(Row.Index + 1).Cells.Item(48).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(204, 12, 12))
        End If

      Next

      'HEADING COLOURS
      'Pink
      ws.Rows.Item(0).Cells.Item(19).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      ws.Rows.Item(0).Cells.Item(20).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      ws.Rows.Item(0).Cells.Item(21).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      ws.Rows.Item(0).Cells.Item(22).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      ws.Rows.Item(0).Cells.Item(23).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      'Purple
      ws.Rows.Item(0).Cells.Item(24).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      ws.Rows.Item(0).Cells.Item(25).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      ws.Rows.Item(0).Cells.Item(26).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      ws.Rows.Item(0).Cells.Item(27).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      'Blue
      ws.Rows.Item(0).Cells.Item(28).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(29).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(30).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(31).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(32).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(33).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(34).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      'Pink
      ws.Rows.Item(0).Cells.Item(38).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      ws.Rows.Item(0).Cells.Item(39).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(255, 132, 206))
      'Blue
      ws.Rows.Item(0).Cells.Item(40).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      ws.Rows.Item(0).Cells.Item(41).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(65, 196, 219))
      'Purple
      ws.Rows.Item(0).Cells.Item(42).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      ws.Rows.Item(0).Cells.Item(43).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      ws.Rows.Item(0).Cells.Item(44).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(190, 97, 206))
      'Green
      ws.Rows.Item(0).Cells.Item(45).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(111, 221, 77))
      ws.Rows.Item(0).Cells.Item(46).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(111, 221, 77))

      'USER ENTRY COLUMNS (RAND)
      'Orange
      ws.Rows.Item(0).Cells.Item(14).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(214, 92, 0))
      ws.Rows.Item(0).Cells.Item(37).CellFormat.Fill = CellFill.CreateSolidFill(System.Drawing.Color.FromArgb(214, 92, 0))

    End Sub

  End Class

#End Region

#Region " Staff Usage (Production Content) "

  Public Class ProductionContentStaffUsage
    Inherits Singular.Reporting.ReportBase(Of ProductionContentStaffUsageCriteria)


    Public Class ProductionContentStaffUsageCriteria
      Inherits Singular.Reporting.ReportCriteria

      <Display(Name:="Start Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="Start Date is required")>
      Public Property StartDate As Date?

      <Display(Name:="End Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="End Date is required")>
      Public Property EndDate As Date?

      <Display(Name:="Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer? = 2

    End Class

    Public Class TimesheetNewPolicyCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of ProductionContentStaffUsageCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
              '    With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
              '      .Button.AddBinding(KnockoutBindingString.click, "TimesheetNewPolicyReport.AddSystem($data)")
              '      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
              '      .Button.AddClass("btn-block buttontext")
              '    End With
              '  End With
              'End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Usage (Production Content)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionContentStaffUsage]"
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TimesheetNewPolicyCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Staff Usage (Production Services) "

  Public Class ProductionServicesStaffUsage
    Inherits Singular.Reporting.ReportBase(Of ProductionServicesStaffUsageCriteria)


    Public Class ProductionServicesStaffUsageCriteria
      Inherits Singular.Reporting.ReportCriteria

      <Display(Name:="Start Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="Start Date isrequired")>
      Public Property StartDate As Date?

      <Display(Name:="End Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="End Date is required")>
      Public Property EndDate As Date?

      <Display(Name:="Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer? = 1

    End Class

    Public Class ProductionServicesStaffUsageCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of ProductionServicesStaffUsageCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
              '    With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
              '      .Button.AddBinding(KnockoutBindingString.click, "TimesheetNewPolicyReport.AddSystem($data)")
              '      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
              '      .Button.AddClass("btn-block buttontext")
              '    End With
              '  End With
              'End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Usage (Production Services)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptStaffUsageProductionServicesReport]"
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionServicesStaffUsageCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Staff Usage (Production Content) "

  Public Class ProductionContentStaffUsage
    Inherits Singular.Reporting.ReportBase(Of ProductionContentStaffUsageCriteria)


    Public Class ProductionContentStaffUsageCriteria
      Inherits Singular.Reporting.ReportCriteria

      <Display(Name:="Start Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="Start Date is required")>
      Public Property StartDate As Date?

      <Display(Name:="End Date"),
      DateField(AlwaysShow:=True),
      Required(ErrorMessage:="End Date is required")>
      Public Property EndDate As Date?

      <Display(Name:="Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer? = 2

      <Display(Name:="Discipline"),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
      Public Property DisciplineID As Integer?

      <Display(Name:="Contract Type", Description:="", Order:=2),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
      Public Property ContractTypeID As Integer?

    End Class

    Public Class TimesheetNewPolicyCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of ProductionContentStaffUsageCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              '  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
              '    With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
              '      .Button.AddBinding(KnockoutBindingString.click, "TimesheetNewPolicyReport.AddSystem($data)")
              '      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
              '      .Button.AddClass("btn-block buttontext")
              '    End With
              '  End With
              'End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(3, 3, 2, 2, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionContentStaffUsageCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Discipline"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(3, 3, 2, 2, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionContentStaffUsageCriteria) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Contract Type"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Usage (Production Content)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionContentStaffUsage]"
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TimesheetNewPolicyCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Hours per Quarter "

  Public Class PersonHoursPerQuarter
    Inherits Singular.Reporting.ReportBase(Of HoursPerQuarterCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Hours per Quarter"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptQuarterlyHoursPerPerson"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HoursPerQuarterCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

    Public Class HoursPerQuarterCriteria
      Inherits Singular.Reporting.DefaultCriteria

      <Required(ErrorMessage:="Year is required"),
      DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemYearList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="TeamHoursPerQuarterReportBO.setSystemYearCriteriaBeforeRefresh",
                  PreFindJSFunction:="TeamHoursPerQuarterReportBO.triggerSystemYearAutoPopulate",
                  AfterFetchJS:="TeamHoursPerQuarterReportBO.afterSystemYearRefreshAjax",
                  LookupMember:="SystemYear", ValueMember:="SystemYearID", DropDownColumns:={"SubDept", "YearName", "YearStartDate", "YearEndDate"},
                  OnItemSelectJSFunction:="TeamHoursPerQuarterReportBO.onYearSelected"),
      Display(Name:="Year")>
      Public Property SystemYearID As Integer?
      <ClientOnly>
      Public Property SystemYear As String = ""

    End Class

    Public Class HoursPerQuarterCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.With(Of HoursPerQuarterCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  .Helpers.Bootstrap.LabelFor(Function(d) d.SystemYearID).Style.Width = "100%"
                  .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemYearID, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
        End With
      End Sub

    End Class

  End Class

#End Region

End Class
