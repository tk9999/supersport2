﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations

Public Class QuoteReports

  Public Class QuotationReport
    Inherits Singular.Reporting.ReportBase(Of QuotationReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Quotation"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptQuotation)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      cmd.CommandText = "[RptProcs].[rptQuotationReport]"
    End Sub
  End Class

  'Detailed Quotation Report
  Public Class QuotationReportDetailed
    Inherits Singular.Reporting.ReportBase(Of QuotationReportDetailedCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Detailed Quotation Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptQuotationDetailed)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptQuotationReportDetailed]"
    End Sub
  End Class

  'Quotation Temp Report

  Public Class QuotationTempReport
    Inherits Singular.Reporting.ReportBase(Of QuotationTempReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Quotation Temp Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptQuotationTempReport)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptQuotationReportTemp]"
    End Sub
  End Class

  Public Class QuotationReportCriteria
    Inherits DefaultCriteria
    Public Property QuoteID As Integer
  End Class

  Public Class QuotationReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

  End Class

  Public Class QuotationReportDetailedCriteria
    Inherits DefaultCriteria
    Public Property QuoteID As Integer
  End Class

  Public Class QuotationReportCriteriaDetailedControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

  End Class

  Public Class QuotationTempReportCriteria
    Inherits DefaultCriteria
    Public Property QuoteID As Integer
    Public Property CreatedBy As Integer
  End Class

  Public Class QuotationTempReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase
  End Class
End Class
