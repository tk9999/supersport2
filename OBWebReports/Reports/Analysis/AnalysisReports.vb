﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations



Public Class AnalysisReports

#Region "Production Schedule"
  Public Class ProductionSchedule
    Inherits ReportBase(Of ProductionScheduleCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Schedule"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionSchedulingAnalysis]"
    End Sub
  End Class
  Public Class ProductionScheduleCriteria
    Inherits DefaultCriteria

  End Class

#End Region

#Region "Productions with multiple catering"
  Public Class ProductionsWithMultiplecatering
    Inherits ReportBase(Of ProductionsWithMultiplecateringCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Productions with multiple catering"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionsWithMultipleCatering]"
    End Sub
  End Class

  Public Class ProductionsWithMultiplecateringCriteria
    Inherits DefaultCriteria
  End Class
#End Region

#Region "Production Scheduling Breakdown"

  Public Class ProductionSchedulingBreakdown
    Inherits ReportBase(Of ProductionSchedulingBreakdownCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Scheduling Breakdown"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionSchedulingBreakdown]"
    End Sub
  End Class

  Public Class ProductionSchedulingBreakdownCriteria
    Inherits DefaultCriteria
  End Class

#End Region

#Region "Production Scheduling Summary"
  Public Class ProductionSchedulingSummary
    Inherits ReportBase(Of ProductionSchedulingSummaryCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Scheduling Summary"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionSchedulingSummary]"
    End Sub
  End Class
  Public Class ProductionSchedulingSummaryCriteria
    Inherits DefaultCriteria
  End Class
#End Region
#Region "Max Overtime per Discipline"
  Public Class MaxOvertimePerDiscipline
    Inherits ReportBase(Of MaxOvertimePerDisciplineCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Max Overtime per Discipline"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptMaxOvertimePerDiscipline]"
    End Sub
  End Class
  Public Class MaxOvertimePerDisciplineCriteria
    Inherits DefaultCriteria

  End Class
#End Region

#Region "Overtime Hours New"

  Public Class OvertimeHoursNew
    Inherits ReportBase(Of OvertimeHoursNewCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Overtime Hours New"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptOvertimeHoursNew]"

    End Sub
  End Class

  Public Class OvertimeHoursNewCriteria
    Inherits DefaultCriteria

  End Class
#End Region

#Region "Production Vehicles Hours Worked"
  Public Class ProductionVehiclesHoursWorked
    Inherits ReportBase(Of ProductionVehiclesHoursWorkedCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Vehicles Hours Worked"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionVehiclesHoursWorked]"
    End Sub
  End Class
  Public Class ProductionVehiclesHoursWorkedCriteria
    Inherits DefaultCriteria
  End Class
#End Region
End Class
