﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Singular.DataAnnotations
Imports Csla
Imports OBLib.Security
Imports OBLib.HR.ReadOnly
Imports OBLib.Notifications.Sms.ReadOnly

Public Class NotificationReports

#Region "Sms Delivery"

  Class SmsDelivery
    Inherits Singular.Reporting.ReportBase(Of SmsDeliveryCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Sms Delivery"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SmsDeliveryCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSmsDelivery]"
    End Sub

  End Class

  Class SmsDeliveryCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Search By"), Required(ErrorMessage:="Search By is required"),
    SetExpression("SmsDeliveryReport.ScenarioSet"),
    DropDownWeb(GetType(OBLib.Notifications.Sms.ReadOnly.ROSmsSearchScenarioList), DisplayMember:="ScenarioName", ValueMember:="ScenarioID", Source:=DropDownWeb.SourceType.CommonData)>
    Public Property Scenario() As Integer?

    <Display(Name:="Created Start Date")>
    Public Property CreatedStartDate As DateTime? = Now
    ', SetExpression("ROSmsSearchListCriteriaBO.CreatedStartDateSet(self)"),
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.CreatedStartDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    <Display(Name:="Created End Date")>
    Public Property CreatedEndDate As DateTime? = Now
    ', SetExpression("ROSmsSearchListCriteriaBO.CreatedEndDateSet(self)"),
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.CreatedEndDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    <Display(Name:="Created By"),
        DropDownWeb(GetType(ROUserFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                    BeforeFetchJS:="SmsDeliveryReport.setCreatedByUserIDCriteriaBeforeRefresh",
                    PreFindJSFunction:="SmsDeliveryReport.triggerCreatedByUserIDAutoPopulate",
                    AfterFetchJS:="SmsDeliveryReport.afterCreatedByUserIDRefreshAjax",
                    OnItemSelectJSFunction:="SmsDeliveryReport.onUserIDSelected",
                    LookupMember:="CreatedBy", DisplayMember:="FullName", ValueMember:="UserID",
                    DropDownColumns:={"FirstName", "Surname", "System", "ProductionArea"})>
    Public Property CreatedByUserID() As Integer?

    <Display(Name:="Created By", Description:=""), ClientOnly>
    Public Property CreatedBy() As String

    <Display(Name:="Recipient"),
     DropDownWeb(GetType(ROHumanResourceContactList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SmsDeliveryReport.setRecipientHumanResourceIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="SmsDeliveryReport.triggerRecipientHumanResourceIDAutoPopulate",
                 AfterFetchJS:="SmsDeliveryReport.afterRecipientHumanResourceIDRefreshAjax",
                 OnItemSelectJSFunction:="SmsDeliveryReport.onRecipientHumanResourceIDSelected",
                 LookupMember:="RecipientName", DisplayMember:="PreferredFirstSurname", ValueMember:="HumanResourceID",
                 DropDownColumns:={"Firstname", "Surname", "CellPhoneNumber", "AlternativeContactNumber"})>
    Public Property RecipientHumanResourceID() As Integer?


    <Display(Name:="Recipient", Description:=""), ClientOnly>
    Public Property RecipientName() As String

    <Display(Name:="Batch Name", Description:=""),
    DropDownWeb(GetType(ROSmsBatchFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SmsDeliveryReport.setSmsBatchIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SmsDeliveryReport.triggerSmsBatchIDAutoPopulate",
                AfterFetchJS:="SmsDeliveryReport.afterSmsBatchIDRefreshAjax",
                OnItemSelectJSFunction:="SmsDeliveryReport.onSmsBatchIDSelected",
                LookupMember:="BatchName", DisplayMember:="BatchName", ValueMember:="SmsBatchID",
                DropDownColumns:={"BatchName", "BatchDescription", "TotalSmses", "DeliveredCount", "RecipientCount"})>
    Public Property SmsBatchID() As Integer?

    <Display(Name:="Batch Name"), ClientOnly>
    Public Property BatchName() As String

    <Display(Name:="Batch Name", Description:=""),
     DropDownWeb(GetType(ROSmsBatchCrewTypeList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SmsDeliveryReport.setSmsBatchCrewTypeIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="SmsDeliveryReport.triggerSmsBatchCrewTypeIDAutoPopulate",
                 AfterFetchJS:="SmsDeliveryReport.afterSmsBatchCrewTypeIDRefreshAjax",
                 OnItemSelectJSFunction:="SmsDeliveryReport.onSmsBatchCrewTypeIDSelected",
                 LookupMember:="CrewType", DisplayMember:="CrewType", ValueMember:="SmsBatchCrewTypeID",
                 DropDownColumns:={"CrewType", "CrewCount"})>
    Public Property SmsBatchCrewTypeID() As Integer?

    <Display(Name:="Crew Type"), ClientOnly>
    Public Property CrewType() As String

    <Display(Name:="Event Start Date", Description:="")>
    Public Property ProductionSystemAreaStartDate() As DateTime?
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.ProductionSystemAreaStartDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    <Display(Name:="Event End Date", Description:="")>
    Public Property ProductionSystemAreaEndDate() As DateTime?
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.ProductionSystemAreaEndDateValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    <Display(Name:="Event"),
     DropDownWeb(GetType(ROPSAListSms), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SmsDeliveryReport.setProductionSystemAreaCriteriaBeforeRefresh",
                 PreFindJSFunction:="SmsDeliveryReport.triggerProductionSystemAreaAutoPopulate",
                 AfterFetchJS:="SmsDeliveryReport.afterProductionSystemAreaRefreshAjax",
                 OnItemSelectJSFunction:="SmsDeliveryReport.onProductionSystemAreaIDSelected",
                 LookupMember:="ProductionSystemAreaName", DisplayMember:="ProductionSystemAreaName", ValueMember:="ProductionSystemAreaID",
                 DropDownColumns:={"SubDept", "Area", "ProductionSystemAreaName", "MinSD", "MaxED", "Room"})>
    Public Property ProductionSystemAreaID() As Integer?

    <Display(Name:="Event"), ClientOnly>
    Public Property ProductionSystemAreaName() As String

    <Display(Name:="Start Day", Description:="")>
    Public Property StartDay() As Date?
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.StartDayValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    <Display(Name:="End Day", Description:="")>
    Public Property EndDay() As Date?
    'Singular.DataAnnotations.JSRule(FunctionName:="ROSmsSearchListCriteriaBO.EndDayValid", OtherTriggerProperties:={"Scenario"}, RuleName:="Required")

    '<Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required")>
    'Public Property StartDate As DateTime?

    '<Display(Name:="End Date"), Required(ErrorMessage:="End Date is required")>
    'Public Property EndDate As DateTime?

    <Display(Name:="Sub-Dept")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Area")>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Sub-Dept"), ClientOnly, Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID As Integer?

    <Display(Name:="Area"), ClientOnly, Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID As Integer?

  End Class

  Public Class SmsDeliveryCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SmsDeliveryCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 6, 6, 4)
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.Scenario).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.Scenario, BootstrapEnums.InputSize.Small)
              End With
            End With
          End With
        End With
        'Scenario - by created
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 0")
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
            'Systems and Areas
            With .Helpers.FieldSet("Sub-Depts")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                  .Style.MarginLeft("10px")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddArea($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                    .Button.AddClass("btn-block buttontext")
                    .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
            With .Helpers.FieldSet("Other")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedStartDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedEndDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedByUserID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , "Created By")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.RecipientHumanResourceID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , "Recipient Name")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        'Scenario - by day
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 1")
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
            'Systems and Areas
            With .Helpers.FieldSet("Sub-Depts")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                  .Style.MarginLeft("10px")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddArea($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                    .Button.AddClass("btn-block buttontext")
                    .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
            With .Helpers.FieldSet("Other")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.StartDay).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.StartDay, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.EndDay).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.EndDay, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.RecipientHumanResourceID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.RecipientHumanResourceID, Singular.Web.BootstrapEnums.InputSize.Small, , "Recipient Name")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        'Scenario - by day
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 2")
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
            'Systems and Areas
            With .Helpers.FieldSet("Sub-Depts")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                  .Style.MarginLeft("10px")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddArea($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                    .Button.AddClass("btn-block buttontext")
                    .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
            With .Helpers.FieldSet("Other")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaStartDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Start Date")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaEndDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Event End Date")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.ProductionSystemAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , "Event")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        'Scenario - by batch
        With .Helpers.Bootstrap.Row
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 3")
          With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
            'Systems and Areas
            With .Helpers.FieldSet("Sub-Depts")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                  .Style.MarginLeft("10px")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryReport.AddArea($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                    .Button.AddClass("btn-block buttontext")
                    .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
            With .Helpers.FieldSet("Other")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedStartDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Day")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedEndDate).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Day")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                .AddBinding(Singular.Web.KnockoutBindingString.visible, "$data.Scenario() == 3")
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.CreatedByUserID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.CreatedByUserID, Singular.Web.BootstrapEnums.InputSize.Small, , "Created By")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryCriteria) d.SmsBatchID).Style.Width = "100%"
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryCriteria) d.SmsBatchID, Singular.Web.BootstrapEnums.InputSize.Small, , "Created By")
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Sms Delivery (OB Event)"

  Class SmsDeliveryOBEvent
    Inherits Singular.Reporting.ReportBase(Of SmsDeliveryOBEventCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Sms Delivery (OB Event)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SmsDeliveryOBEventCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSmsDeliveryOBEvent]"
    End Sub

  End Class

  Class SmsDeliveryOBEventCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Event Start Date", Description:="")>
    Public Property ProductionSystemAreaStartDate() As DateTime?

    <Display(Name:="Event End Date", Description:="")>
    Public Property ProductionSystemAreaEndDate() As DateTime?

    <Display(Name:="Event"),
     DropDownWeb(GetType(ROPSAListSms), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SmsDeliveryOBEventReport.setProductionSystemAreaCriteriaBeforeRefresh",
                 PreFindJSFunction:="SmsDeliveryOBEventReport.triggerProductionSystemAreaAutoPopulate",
                 AfterFetchJS:="SmsDeliveryOBEventReport.afterProductionSystemAreaRefreshAjax",
                 OnItemSelectJSFunction:="SmsDeliveryOBEventReport.onProductionSystemAreaIDSelected",
                 LookupMember:="ProductionSystemAreaName", DisplayMember:="ProductionSystemAreaName", ValueMember:="ProductionSystemAreaID",
                 DropDownColumns:={"RefNum", "ProductionSystemAreaName", "MinSD", "MaxED", "Room"})>
    Public Property ProductionSystemAreaID() As Integer?

    <Display(Name:="Event"), ClientOnly>
    Public Property ProductionSystemAreaName() As String

    <Display(Name:="Sub-Dept")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Area")>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Sub-Dept"), ClientOnly, Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID As Integer?

    <Display(Name:="Area"), ClientOnly, Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID As Integer?

  End Class

  Public Class SmsDeliveryOBEventCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SmsDeliveryOBEventCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        'Scenario - by event
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.FieldSet("Criteria")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                  'Systems and Areas
                  With .Helpers.FieldSet("Sub-Depts")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryOBEventReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        .Style.MarginLeft("10px")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "SmsDeliveryOBEventReport.AddArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                          .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaStartDate).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaStartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Event Start Date")
                          .Editor.AddBinding(KnockoutBindingString.enable, "SmsDeliveryOBEventReport.canEdit('ProductionSystemAreaStartDate', $data)")
                        End With
                      End With
                    End With
                    With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaEndDate).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaEndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Event End Date")
                          .Editor.AddBinding(KnockoutBindingString.enable, "SmsDeliveryOBEventReport.canEdit('ProductionSystemAreaEndDate', $data)")
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        .Helpers.Bootstrap.LabelFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaID).Style.Width = "100%"
                        With .Helpers.Bootstrap.FormControlFor(Function(d As SmsDeliveryOBEventCriteria) d.ProductionSystemAreaID, Singular.Web.BootstrapEnums.InputSize.Small, , "Event")
                          .Editor.AddBinding(KnockoutBindingString.enable, "SmsDeliveryOBEventReport.canEdit('ProductionSystemAreaID', $data)")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

End Class
