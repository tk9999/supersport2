﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports Singular.Web


Public Class VehicleAndEquipmentReports



#Region "Vehicle Equipment Summary Report"

  Public Class VehicleEquipmentSummary
    Inherits ReportBase(Of VehicleEquipmentSummaryCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Vehicle Equipment Summary Report"
      End Get
    End Property


    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptVehicleEquipmentSummary"
      'cmd.Parameters.AddWithValue()
    End Sub

  End Class

  Public Class VehicleEquipmentSummaryCriteria
    Inherits DefaultCriteria

  End Class

#End Region

#Region "Ad Hoc Timesheets Report"

  Public Class AdHocTimesheets
    Inherits ReportBase(Of AdHocTimesheetsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Ad Hoc Timesheets Report"
      End Get
    End Property


    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptAdHocTimesheetsReport"
      'cmd.Parameters.AddWithValue()
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(AdHocTimesheetsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class AdHocTimesheetsCriteria
        Inherits Singular.Reporting.StartAndEndDateReportCriteria



  End Class

  Public Class AdHocTimesheetsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()

            With Helpers.With(Of AdHocTimesheetsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
                With .Helpers.FieldSet("Date Selection")
                    With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 11, 8)
                            With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                                .Style.TextAlign = TextAlign.center
                                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                                End With
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                        .Editor.Attributes("placeholder") = "Start Date"
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Column(8, 12, 4, 3)

                                .Style.TextAlign = TextAlign.center
                                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                        .Editor.Attributes("placeholder") = "End Date"
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
            End With
        End Sub
  End Class

#End Region

End Class
