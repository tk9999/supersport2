﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Imports Singular.Web
Imports Infragistics.Documents.Excel
Imports Singular.DataAnnotations
Imports System.IO
Imports OBLib
Imports Csla
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly

Public Class PlayoutOperationsReports

#Region "Bookings by Controller Report"

  Public Class PlayoutOperationsActivityByResource
    Inherits ReportBase(Of PlayoutOperationsActivityByResourceCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Bookings by Controller Report (Shift Roster)"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptPlayoutOperationsActivityByResource)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsActivityByResource]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsActivityByResourceControl)
      End Get
    End Property
  End Class

  Public Class PlayoutOperationsActivityByResourceCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Only Rooms")>
    Public Property OnlyRoomsInd As Boolean = False

    <Display(Name:="Only Human Resources")>
    Public Property OnlyHRInd As Boolean = False

  End Class

  Public Class PlayoutOperationsActivityByResourceControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsActivityByResourceCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "BookingsByControllerReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsByControllerReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.Bootstrap.StateButton(Function(c) c.OnlyRoomsInd, "Only Rooms", "Only Rooms", "btn-success", , , , )
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.Bootstrap.StateButton(Function(c) c.OnlyHRInd, "Only HR", "Only HR", "btn-success", , , , )
                        .Button.AddClass("btn-block")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsByControllerReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "BookingsByControllerReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " MCR Bookings Report"

  Public Class PlayoutOperationsMCRShiftReport
    Inherits ReportBase(Of PlayoutOperationsMCRShiftReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "MCR ASRA Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptPlayoutOperationsMCRShiftReport)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      MyBase.TablesToIgnore.Add("Table")
    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsMCRShiftReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsMCRShiftReportControl)
      End Get
    End Property
  End Class

  Public Class PlayoutOperationsMCRShiftReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class PlayoutOperationsMCRShiftReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region "SCCR Bookings Report"

  Public Class PlayoutOperationsSCCRShiftReport
    Inherits ReportBase(Of PlayoutOperationsSCCRShiftReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SCCR ASRA Report"
      End Get
    End Property


    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptPlayoutOperationsSCCRShiftReport)

      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsSCCRShiftReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsSCCRShiftReportControl)
      End Get
    End Property
  End Class

  Public Class PlayoutOperationsSCCRShiftReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class PlayoutOperationsSCCRShiftReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region "SCCR Calculator"

  Public Class SCCRCalculator
    Inherits ReportBase(Of SCCRCalculatorCriteria)

    Private Const WorksheetName As String = "SCCR Calculator Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SCCR Calculator"
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderNameMonths As New List(Of String)(New String() {"Month", "Morning SCCR Shortfall (06:00 - 11:59)", "Afternoon SCCR Shortfall (12:00 - 17:59)", "Evening SCCR Shortfall (18:00 - 23:59)", "Night SCCR Shortfall (00:00 - 05:59)"})
      Private mHeaderNameDays As New List(Of String)(New String() {"Day", "Morning SCCR Shortfall (06:00 - 11:59)", "Afternoon SCCR Shortfall (12:00 - 17:59)", "Evening SCCR Shortfall (18:00 - 23:59)", "Night SCCR Shortfall (00:00 - 05:59)"})
      Private mHeaderNameInfo As New List(Of String)(New String() {"Extra Channels", "Start Date", "End Date", "Event", "Channel", "Available SCCRs"})
      Private mCurrentHeaderMonths As Integer = -1
      Private mCurrentHeaderDays As Integer = -1
      Private mCurrentHeaderInfo As Integer = -1

      Public Sub AddHeaderMonths(HeaderName As String)
        mHeaderNameMonths.Add(HeaderName)
      End Sub

      Public Sub AddHeaderDays(HeaderName As String)
        mHeaderNameDays.Add(HeaderName)
      End Sub

      Public Sub AddHeaderInfo(HeaderName As String)
        mHeaderNameInfo.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderNameMonths() As String
        mCurrentHeaderMonths += 1
        Return (mHeaderNameMonths.Item(mCurrentHeaderMonths))
      End Function

      Public Function GetNextHeaderNameDays() As String
        mCurrentHeaderDays += 1
        Return (mHeaderNameDays.Item(mCurrentHeaderDays))
      End Function

      Public Function GetNextHeaderNameInfo() As String
        mCurrentHeaderInfo += 1
        Return (mHeaderNameInfo.Item(mCurrentHeaderInfo))
      End Function

      Public Function GetCountMonths() As Integer
        Return mHeaderNameMonths.Count()
      End Function

      Public Function GetCountDays() As Integer
        Return mHeaderNameDays.Count()
      End Function

      Public Function GetCountInfo() As Integer
        Return mHeaderNameInfo.Count()
      End Function

    End Class
#End Region

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSCCRCalculator]"
      cmd.CommandTimeout = 0
    End Sub

#Region "Excel"
    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)
      Dim p = 0
      Dim HeaderName As New HeaderNames
      Dim LineNo As Integer = 0
      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1
      '----
      'Report Info
      '----

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCountInfo - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      'SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(7).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      For Col As Integer = 0 To HeaderName.GetCountInfo - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400
        For Row As Integer = 0 To (Data.Tables.Item(7).Rows.Count)
          If Col = 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameInfo(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > 0 Then 'First Column
            SetText(ws, Row, Col, Data.Tables.Item(7).Rows.Item(Row - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = 0 Then 'Header Row
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameInfo(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCountInfo - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim ExtraEvents As Integer? = Nothing
      Dim StartDate As String = ""
      Dim EndDate As String = ""
      Dim EventType As String = ""
      Dim Channel As String = ""
      Dim AvailableSCCRs As Integer? = Nothing
      For Row As Integer = 1 To (Data.Tables.Item(7).Rows.Count)
        ExtraEvents = Data.Tables.Item(7).Rows.Item(Row - 1)(0)
        StartDate = Data.Tables.Item(7).Rows.Item(Row - 1)(1)
        EndDate = Data.Tables.Item(7).Rows.Item(Row - 1)(2)
        If Not IsDBNull(Data.Tables.Item(7).Rows.Item(Row - 1)(3)) Then
          EventType = Data.Tables.Item(7).Rows.Item(Row - 1)(3)
        End If
        If Not IsDBNull(Data.Tables.Item(7).Rows.Item(Row - 1)(4)) Then
          Channel = Data.Tables.Item(7).Rows.Item(Row - 1)(4)
        End If
        AvailableSCCRs = Data.Tables.Item(7).Rows.Item(Row - 1)(5)
        For Col As Integer = 1 To (HeaderName.GetCountInfo - 1)
          Select Case Col
            Case 0
              SetText(ws, Row, Col, ExtraEvents, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            Case 1
              If StartDate IsNot Nothing Then
                SetText(ws, Row, Col, StartDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                StartDate = Nothing
              End If
            Case 2
              If EndDate IsNot Nothing Then
                SetText(ws, Row, Col, EndDate, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                EndDate = Nothing
              End If
            Case 3
              If EventType IsNot Nothing Then
                SetText(ws, Row, Col, EventType, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                EventType = ""
              End If
            Case 4
              If Channel IsNot Nothing Then
                SetText(ws, Row, Col, Channel, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                Channel = ""
              End If
            Case 5
              If AvailableSCCRs IsNot Nothing Then
                SetText(ws, Row, Col, AvailableSCCRs, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                AvailableSCCRs = Nothing
              End If
            Case Else
          End Select
        Next
      Next

      '----
      'Month Shortfalls
      '----
      LineNo = Data.Tables.Item(7).Rows.Count + 2

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCountMonths - 1, LineNo, LineNo, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, LineNo, LineNo + Data.Tables.Item(5).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      For Col As Integer = 0 To HeaderName.GetCountMonths - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400
        For Row As Integer = LineNo To LineNo + (Data.Tables.Item(5).Rows.Count)
          If Col = 0 AndAlso Row = LineNo Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameMonths(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > LineNo Then 'First Column
            SetText(ws, Row, Col, Data.Tables.Item(5).Rows.Item((Row - LineNo) - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = LineNo Then 'Header Row
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameMonths(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCountMonths - 1, LineNo, LineNo, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, LineNo, LineNo + Data.Tables.Item(5).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim Month As String = Nothing
      Dim MorningShort As Integer? = Nothing
      Dim AfternoonShort As Integer? = Nothing
      Dim EveningShort As Integer? = Nothing
      Dim NightShort As Integer? = Nothing
      For Row As Integer = LineNo To (LineNo + (Data.Tables.Item(5).Rows.Count)) - 1
        Month = Data.Tables.Item(5).Rows.Item(Row - LineNo)(0)
        If Not IsDBNull(Data.Tables.Item(5).Rows.Item(Row - LineNo)(1)) Then
          MorningShort = Math.Abs(CInt(Data.Tables.Item(5).Rows.Item(Row - LineNo)(1)))
        End If
        If Not IsDBNull(Data.Tables.Item(5).Rows.Item(Row - LineNo)(2)) Then
          AfternoonShort = Math.Abs(CInt(Data.Tables.Item(5).Rows.Item(Row - LineNo)(2)))
        End If
        If Not IsDBNull(Data.Tables.Item(5).Rows.Item(Row - LineNo)(3)) Then
          EveningShort = Math.Abs(CInt(Data.Tables.Item(5).Rows.Item(Row - LineNo)(3)))
        End If
        If Not IsDBNull(Data.Tables.Item(5).Rows.Item(Row - LineNo)(4)) Then
          NightShort = Math.Abs(CInt(Data.Tables.Item(5).Rows.Item(Row - LineNo)(4)))
        End If
        For Col As Integer = 1 To (HeaderName.GetCountMonths - 1)
          Select Case Col
            Case 0
              SetText(ws, Row + 1, Col, Month, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            Case 1
              If MorningShort IsNot Nothing Then
                SetText(ws, Row + 1, Col, MorningShort, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                MorningShort = Nothing
              End If
            Case 2
              If AfternoonShort IsNot Nothing Then
                SetText(ws, Row + 1, Col, AfternoonShort, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                AfternoonShort = Nothing
              End If
            Case 3
              If EveningShort IsNot Nothing Then
                SetText(ws, Row + 1, Col, EveningShort, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                EveningShort = Nothing
              End If
            Case 4
              If NightShort IsNot Nothing Then
                SetText(ws, Row + 1, Col, NightShort, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                NightShort = Nothing
              End If
            Case Else
          End Select
        Next
      Next

      '----
      'Day Shortfalls
      '----
      LineNo += Data.Tables.Item(5).Rows.Count + 2

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCountDays - 1, LineNo, LineNo, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, LineNo, LineNo + Data.Tables.Item(6).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      For Col As Integer = 0 To HeaderName.GetCountDays - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400
        For Row As Integer = LineNo To LineNo + (Data.Tables.Item(6).Rows.Count)
          If Col = 0 AndAlso Row = LineNo Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameDays(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > LineNo Then 'First Column
            SetText(ws, Row, Col, Data.Tables.Item(6).Rows.Item((Row - LineNo) - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = LineNo Then 'Header Row
            SetText(ws, Row, Col, HeaderName.GetNextHeaderNameDays(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCountDays - 1, LineNo, LineNo, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, LineNo, LineNo + Data.Tables.Item(6).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim Day As String = Nothing
      Dim MorningShortDay As Integer? = Nothing
      Dim AfternoonShortDay As Integer? = Nothing
      Dim EveningShortDay As Integer? = Nothing
      Dim NightShortDay As Integer? = Nothing
      For Row As Integer = LineNo To (LineNo + (Data.Tables.Item(6).Rows.Count)) - 1
        Day = Data.Tables.Item(6).Rows.Item(Row - (LineNo))(0)
        If Not IsDBNull(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(1)) Then
          MorningShortDay = Math.Abs(CInt(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(1)))
        End If
        If Not IsDBNull(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(2)) Then
          AfternoonShortDay = Math.Abs(CInt(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(2)))
        End If
        If Not IsDBNull(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(3)) Then
          EveningShortDay = Math.Abs(CInt(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(3)))
        End If
        If Not IsDBNull(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(4)) Then
          NightShortDay = Math.Abs(CInt(Data.Tables.Item(6).Rows.Item(Row - (LineNo))(4)))
        End If
        For Col As Integer = 1 To (HeaderName.GetCountDays - 1)
          Select Case Col
            Case 1
              If MorningShortDay IsNot Nothing Then
                SetText(ws, Row + 1, Col, MorningShortDay, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                MorningShortDay = Nothing
              End If
            Case 2
              If AfternoonShortDay IsNot Nothing Then
                SetText(ws, Row + 1, Col, AfternoonShortDay, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                AfternoonShortDay = Nothing
              End If
            Case 3
              If EveningShortDay IsNot Nothing Then
                SetText(ws, Row + 1, Col, EveningShortDay, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                EveningShortDay = Nothing
              End If
            Case 4
              If NightShortDay IsNot Nothing Then
                SetText(ws, Row + 1, Col, NightShortDay, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                NightShortDay = Nothing
              End If
            Case Else
          End Select
        Next
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub
#End Region

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SCCRCalculatorCriteriaControl)
      End Get
    End Property

  End Class


  Public Class SCCRCalculatorCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Event Type")>
    Public Property EventTypeID As Integer? = Nothing

    <Display(Name:="Event Type to include")>
    Public Property IncludeEventTypeIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Event Types to exclude")>
    Public Property ExcludeEventTypeIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Production Type"), ClientOnly>
    Public Property ProductionType As String = ""

    <Display(Name:="Event Type"), ClientOnly>
    Public Property EventType As String

    <Display(Name:="Channel")>
    Public Property ChannelID As Integer? = Nothing

    '<Display(Name:="Site"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ProductionAreaList))>
    'Public Property ProductionAreaID As Integer? = Nothing
    <Display(Name:="Site"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROSiteList), UnselectedText:="Choose Area")>
    Public Property SiteID As Integer? = Nothing

    '<Display(Name:="Channel to include")>
    'Public Property IncludeChannelIDs As List(Of Integer) = New List(Of Integer)

    '<Display(Name:="Channel to exclude")>
    'Public Property ExcludeChannelIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Channel"), ClientOnly>
    Public Property Channel As String

    <Display(Name:="Gen Ref Number")>
    Public Property GenRefNumber As Int64? = Nothing

    <Display(Name:="Number of Extra Events")>
    Public Property NoOfExtraEvents As Integer? = Nothing

    <Display(Name:="Morning")>
    Public Property MorningInd As Boolean = True

    <Display(Name:="Afternoon")>
    Public Property AfternoonInd As Boolean = True

    <Display(Name:="Evening")>
    Public Property EveningInd As Boolean = True

    <Display(Name:="Night")>
    Public Property NightInd As Boolean = True

    <Display(Name:="Add Events")>
    Public Property AddEventsInd As Boolean = True

    <Display(Name:="Sponsored Events Only")>
    Public Property SponsorshipInd As Boolean = False


    <Display(Name:="Time Categories")>
    Public Property TimeCategories As List(Of TimeCategory) = New List(Of TimeCategory) From {New TimeCategory With {.Category = "Morning", .StartTime = New DateTime(Now.Year, Now.Month, Now.Day, 6, 0, 0), .EndTime = New DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0)}, _
                                                               New TimeCategory With {.Category = "Afternoon", .StartTime = New DateTime(Now.Year, Now.Month, Now.Day, 12, 0, 0), .EndTime = New DateTime(Now.Year, Now.Month, Now.Day, 18, 0, 0)}, _
                                                               New TimeCategory With {.Category = "Evening", .StartTime = New DateTime(Now.Year, Now.Month, Now.Day, 18, 0, 0), .EndTime = New DateTime(Now.Year, Now.Month, Now.Day, 23, 59, 59)}, _
                                                               New TimeCategory With {.Category = "Night", .StartTime = New DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0), .EndTime = New DateTime(Now.Year, Now.Month, Now.Day, 6, 0, 0)} _
                                                              }

  End Class

  Public Class TimeCategory

    Public Property Category As String
    Public Property StartTime As DateTime
    Public Property EndTime As DateTime

  End Class

  Public Class SCCRCalculatorCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SCCRCalculatorCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)


            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                .Helpers.Bootstrap.Label("Select Site")
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.SiteID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Select Site"
                End With
              End With
            End With

            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(6, 6, 3, 3)
                '.Helpers.Bootstrap.Label("Sponsored Events Only")
                With .Helpers.Bootstrap.StateButton(Function(d As SCCRCalculatorCriteria) d.SponsorshipInd, "Sponsored Events Only", "Sponsored Events Only", "btn-success", , , , "btn-md")
                  .Button.AddClass("btn-block buttontext")
                End With
              End With
            End With
            'With .Helpers.Bootstrap.FormGroup
            '  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SCCRCalculatorCriteria) d.Channel, "SCCRCalculatorReport.FindSite($element)",
            '                                           "Search for Site", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
            '  End With
            'End With

            With .Helpers.FieldSet("Add Event Types")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SCCRCalculatorCriteria) d.EventType, "SCCRCalculatorReport.IncludeEventType($element)",
                                                             "Search for Event Type to include", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SCCRCalculatorCriteria) d.Channel, "SCCRCalculatorReport.FindChannel($element)",
                                                             "Search for Channel to include", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup
                    With .Helpers.Bootstrap.FormControlFor(Function(d As SCCRCalculatorCriteria) d.NoOfExtraEvents, BootstrapEnums.InputSize.ExtraSmall, , "Number of Extra Events")
                    End With
                  End With
                End With
              End With
            End With

            With .Helpers.FieldSet("Delete Event Types")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SCCRCalculatorCriteria) d.EventType, "SCCRCalculatorReport.ExcludeEventType($element)",
                                                             "Search for Event Type to exclude", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SCCRCalculatorCriteria) d.Channel, "SCCRCalculatorReport.FindChannel($element)",
                                                             "Search for Channel to exclude", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
            End With


            With .Helpers.FieldSet("Time of Day")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                  With .Helpers.TableFor(Of TimeCategory)(Function(d As SCCRCalculatorCriteria) d.TimeCategories, True, True)
                    '.AddClass("col-xs-12 col-sm-12 col-md-8 col-lg-8 text-left")
                    With .FirstRow
                      '.AddClass("col-xs-12 col-sm-12 col-md-8 col-lg-8 text-left")
                      With .AddColumn("Start Time")
                        .AddClass("col-xs-6 col-sm-6 col-md-3 col-lg-3 text-left")
                        .Helpers.TimeEditorFor(Function(d As TimeCategory) d.StartTime)
                      End With
                      With .AddColumn("End Time")
                        .AddClass("col-xs-6 col-sm-6 col-md-3 col-lg-3 text-left")
                        .Helpers.TimeEditorFor(Function(d As TimeCategory) d.EndTime)
                      End With
                      With .AddColumn("Time Name")
                        .AddClass("col-xs-6 col-sm-6 col-md-4 col-lg-4 text-left")
                        .Helpers.EditorFor(Function(d As TimeCategory) d.Category)
                      End With
                    End With
                  End With
                End With
              End With
              'With .Helpers.Bootstrap.Row
              '  With .Helpers.Bootstrap.Column(12, 12, 8, 8)
              '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '      With .Helpers.Bootstrap.StateButton(Function(d As SCCRCalculatorCriteria) d.MorningInd, "Morning (06:00 - 11:59)", "Morning (06:00 - 11:59)", "btn-success", , , , )
              '        .Button.AddClass("btn-block buttontext")
              '      End With
              '    End With
              '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '      With .Helpers.Bootstrap.StateButton(Function(d As SCCRCalculatorCriteria) d.AfternoonInd, "Afternoon (12:00 - 17:59)", "Afternoon (12:00 - 17:59)", "btn-success", , , , )
              '        .Button.AddClass("btn-block buttontext")
              '      End With
              '    End With
              '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '      With .Helpers.Bootstrap.StateButton(Function(d As SCCRCalculatorCriteria) d.EveningInd, "Evening (18:00 - 23:59)", "Evening (18:00 - 23:59)", "btn-success", , , , )
              '        .Button.AddClass("btn-block buttontext")
              '      End With
              '    End With
              '    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              '      With .Helpers.Bootstrap.StateButton(Function(d As SCCRCalculatorCriteria) d.NightInd, "Night (00:00 - 05:59)", "Night (00:00 - 05:59)", "btn-success", , , , )
              '        .Button.AddClass("btn-block buttontext")
              '      End With
              '    End With
              '  End With
              'End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3, 3)
                    With .Helpers.Bootstrap.Button("", "Update Stats", , , , , "fa-refresh", , PostBackType.None, "SCCRCalculatorReport.UpdateGraphs($data)", )
                      .Button.AddClass("btn-block")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.FieldSet("SCCR Statistics")
                    .Attributes("id") = "Stats"
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.Div
                          .Attributes("id") = "SCCRShortFallContainer"
                          .Style.Width = "600px"
                          .Style.Height = "400px"
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.Div
                          .Attributes("id") = "SCCRShortFallWeekdayContainer"
                          .Style.Width = "600px"
                          .Style.Height = "400px"
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Independent Freelance Agreement Report"

  Public Class IndependentFreelanceAgreementReport
    Inherits ReportBase(Of IndependentFreelanceAgreementCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Independent Freelance Agreement Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptIndependentFreelanceAgreementReport)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptIndependentFreelancerAgreementReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceDetailsControl)
      End Get
    End Property
  End Class

  Public Class IndependentFreelanceAgreementCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class HumanResourceDetailsControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "FreelanceAgreementReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "FreelanceAgreementReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "FreelanceAgreementReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "FreelanceAgreementReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class
#End Region

#Region "Transmission MCR Report"

  Public Class TransmissionMCRReport
    Inherits ReportBase(Of TransmissionMCRReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Transmission MCR Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptTransmissionMCRReport)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptTransmissionMCRReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceDetailsControl)
      End Get
    End Property
  End Class

  Public Class TransmissionMCRReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class TransmissionMCRControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of TransmissionMCRReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.Bootstrap.Row
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "PO ASRA Validation Report"

  Public Class PlayoutOperationsASRAReport
    Inherits ReportBase(Of PlayoutOperationsASRAReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "PO ASRA Validation Report"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As Type
    '  Get
    '    Return GetType(rptSSMSStaffRemunerationAllotmentReport)
    '  End Get
    'End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "HumanResourceShiftID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsASRA]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsASRAReportControl)
      End Get
    End Property

  End Class

  Public Class PlayoutOperationsASRAReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID As Integer?

    <Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaIDs As New List(Of Integer)

  End Class

  Public Class PlayoutOperationsASRAReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsASRAReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Other Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 3)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsASRAReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsASRAReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Bookings Report"

  Public Class SCCRResourcesInWorkOrder
    Inherits ReportBase(Of SCCRResourcesInWorkOrderCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SS Playout Operations - Live Bookings Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptSCCRResourcesInWorkOrder)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsSCCRResourcesInWorkOrder]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SCCRResourcesInWorkOrderControl)
      End Get
    End Property
  End Class

  Public Class SCCRResourcesInWorkOrderCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

    '<Display(Name:="Only Rooms")>
    'Public Property OnlyRoomsInd As Boolean = False

    '<Display(Name:="Only Human Resources")>
    'Public Property OnlyHRInd As Boolean = False

  End Class

  Public Class SCCRResourcesInWorkOrderControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of SCCRResourcesInWorkOrderCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "GeneralBookingsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName & " Booking Area")
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "GeneralBookingsReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.StateButton(Function(c) c.OnlyRoomsInd, "Only Rooms", "Only Rooms", "btn-success", , , , )
                  '      .Button.AddClass("btn-block")
                  '    End With
                  '  End With
                  'End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.StateButton(Function(c) c.OnlyHRInd, "Only HR", "Only HR", "btn-success", , , , )
                  '      .Button.AddClass("btn-block")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "GeneralBookingsReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "GeneralBookingsReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region "Playout Checks"

    Public Class PlayoutChecksReport
        Inherits ReportBase(Of PlayoutChecksReportCriteria)

        Public Overrides ReadOnly Property ReportName As String
            Get
                Return "SS Playout Operations - Data Checks"
            End Get
        End Property

        Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
            cmd.CommandText = "[RptProcs].[rptPlayoutOperationsDataChecks]"
        End Sub

        Public Overrides ReadOnly Property CustomCriteriaControlType As Type
            Get
                Return GetType(PlayoutChecksReportCriteriaControl)
            End Get
        End Property

        Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
            MyBase.ModifyDataSet(DataSet)

            DataSet.Tables(0).TableName = "Shift Times Incorrect"
            DataSet.Tables(1).TableName = "Shifts With No Room Bookings"
            DataSet.Tables(2).TableName = "Doubled Up Shifts"

        End Sub

    End Class

    Public Class PlayoutChecksReportCriteria
        Inherits DefaultCriteria

        <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required")>
        Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

        <Display(Name:="Run Data Fixes", Order:=2)>
        Public Property RunDataFixes As Boolean = False

    End Class

    Public Class PlayoutChecksReportCriteriaControl
        Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.With(Of PlayoutChecksReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
                With .Helpers.FieldSet("Date Selection")
                    With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                        .AddClass("date-picker-reports")
                        .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.Attributes("placeholder") = "Start Date"
                        End With
                    End With
                End With
            End With

        End Sub
    End Class

#End Region

#Region "Live Events Report"
    Public Class SCCRLiveEvents
        Inherits ReportBase(Of SCCRLiveEventsCriteria)

        Public Overrides ReadOnly Property ReportName As String
            Get
                Return "SS Playout Operations - Live Events Report"
            End Get
        End Property

        Public Overrides ReadOnly Property CrystalReportType As Type
            Get
                Return GetType(rptSCCRLiveEvents)
            End Get
        End Property

        Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
            cmd.CommandText = "[RptProcs].[rptPlayoutOperationsSCCRLiveEvents]"
        End Sub

        Public Overrides ReadOnly Property CustomCriteriaControlType As Type
            Get
                Return GetType(SCCRLiveEventsControl)
            End Get
        End Property
    End Class

    Public Class SCCRLiveEventsCriteria
        Inherits DefaultCriteria

        <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
        Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

        <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
        Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

        <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
        Public Property SystemIDs As List(Of Integer)

        <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
        Public Property ProductionAreaIDs As List(Of Integer)

        '<Display(Name:="Only Rooms")>
        'Public Property OnlyRoomsInd As Boolean = False

        '<Display(Name:="Only Human Resources")>
        'Public Property OnlyHRInd As Boolean = False

    End Class

    Public Class SCCRLiveEventsControl
        Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()
            With Helpers.With(Of SCCRLiveEventsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
                With .Helpers.FieldSet("Date Selection")
                    With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .AddClass("date-picker-reports")
                                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                    .Editor.Attributes("placeholder") = "Start Date"
                                End With
                            End With
                            With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                .AddClass("date-picker-reports")
                                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                    .Editor.Attributes("placeholder") = "End Date"
                                End With
                            End With
                        End With
                    End With
                End With
                With .Helpers.FieldSet("Criteria")
                    With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                            With .Helpers.Bootstrap.Row
                                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                                    With .Helpers.FieldSet("Sub-Depts and Areas")
                                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                                            With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                                                .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsLiveEventsReport.AddSystem($data)")
                                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName & "Event Area")
                                                .Button.AddClass("btn-block buttontext")
                                            End With
                                            With .Helpers.Bootstrap.Row
                                                .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                                                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                                                    With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                                                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                                                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                                            .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsLiveEventsReport.AddProductionArea($data)")
                                                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                                                            .Button.AddClass("btn-block buttontext")
                                                        End With
                                                    End With
                                                End With
                                            End With
                                        End With
                                    End With

                                End With

                            End With
                        End With
                    End With
                    With .Helpers.DivC("loading-custom")
                        .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
                        .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
                    End With
                End With
            End With
        End Sub
    End Class

#End Region

#Region "Playout Booking Stats Report"

  Public Class PlayoutBookingStats
    Inherits ReportBase(Of PlayoutBookingStatsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SS Playout Operations - Booking Stats Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutBookingStats]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutBookingStatsControl)
      End Get
    End Property
  End Class

  Public Class PlayoutBookingStatsCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemID As Integer?

  End Class

  Public Class PlayoutBookingStatsControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutBookingStatsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "PlayoutBookingStatsReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Playout ISP Hours Summary Report"
  Public Class PlayoutISPHoursSummary
    Inherits ReportBase(Of PlayoutISPHoursSummaryCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SS Playout Operations - ISP Hours Summary Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptIndependentFreelancerAgreementSummaryReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutISPHoursSummaryControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).TableName = "ISP Hours"
      DataSet.Tables(1).TableName = "ISP Projected Costs"

    End Sub

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Dim ws2 As Worksheet = Workbook.Worksheets(1)

      ws2.Columns(1).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(2).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(3).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(4).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(5).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(6).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(7).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(8).CellFormat.FormatString = """R""#,##0.00;[red](""R""#,##0.00)"
      ws2.Columns(9).CellFormat.FormatString = "0.00%"

    End Sub


  End Class

  Public Class PlayoutISPHoursSummaryCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts")>
    Public Property SystemID As Integer?

  End Class

  Public Class PlayoutISPHoursSummaryControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutISPHoursSummaryCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Playout Daily Event Summary Detailed Report"
  Public Class PlayoutDailyEventSummaryDetailed
    Inherits ReportBase(Of PlayoutDailyEventSummaryDetailedCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SS Playout Operations - Daily Event Summary Detailed Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutsEventHoursReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutDailyEventSummaryDetailedControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).TableName = "Detailed"
      DataSet.Tables(1).TableName = "Summary"

    End Sub

    Private Class HeaderCell
      Public Property HeaderName As String
      Public Property Region As Infragistics.Documents.Excel.WorksheetMergedCellsRegion

      Public Sub New(Heading As String, ByRef ws As Worksheet, StartRow As Integer, StartColumn As Integer, EndRow As Integer, EndColumn As Integer)
        Me.HeaderName = Heading
        Me.Region = ws.MergedCellsRegions.Add(StartRow, StartColumn, EndRow, EndColumn)
      End Sub

      Public Sub FormatRegion()
        Me.Region.Value = Me.HeaderName
        Me.Region.CellFormat.Font.Bold = ExcelDefaultableBoolean.True
      End Sub

    End Class

    Private Sub PopulateCell(text As Object, ByRef wsc As WorksheetCell, Optional Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.False)
      If Not IsDBNull(text) Then
        wsc.Value = text
        wsc.CellFormat.Alignment = HorizontalCellAlignment.Center
        wsc.CellFormat.Alignment = VerticalCellAlignment.Center
        wsc.CellFormat.Font.Bold = Bold
      End If
    End Sub

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)

      '====================First Table (detailed version)======================
      Dim ws As Worksheet = wb.Worksheets.Add("Detailed")
      Dim SummaryTable As DataTable = Data.Tables(0)

      Dim HeaderCells As List(Of HeaderCell) = New List(Of HeaderCell) 'Create a List that holds the header values and their cell-range (used later for merging headings correctly)

      HeaderCells.Add(New HeaderCell("Date", ws, 0, 0, 1, 0))

      HeaderCells.Add(New HeaderCell("Controller", ws, 0, 1, 1, 1))

      HeaderCells.Add(New HeaderCell("SCCR Permanent", ws, 0, 2, 0, 3))
      HeaderCells.Add(New HeaderCell("Events", ws, 1, 2, 1, 2))
      HeaderCells.Add(New HeaderCell("Hours", ws, 1, 3, 1, 3))

      HeaderCells.Add(New HeaderCell("SCCR Freelance", ws, 0, 4, 0, 5))
      HeaderCells.Add(New HeaderCell("Events", ws, 1, 4, 1, 4))
      HeaderCells.Add(New HeaderCell("Hours", ws, 1, 5, 1, 5))

      HeaderCells.Add(New HeaderCell("MCR Permanent Controllers", ws, 0, 6, 0, 7))
      HeaderCells.Add(New HeaderCell("Events", ws, 1, 6, 1, 6))
      HeaderCells.Add(New HeaderCell("Hours", ws, 1, 7, 1, 7))

      HeaderCells.Add(New HeaderCell("FD Controllers", ws, 0, 8, 0, 10))
      HeaderCells.Add(New HeaderCell("MCRs", ws, 1, 8, 1, 8))
      HeaderCells.Add(New HeaderCell("Events", ws, 1, 9, 1, 9))
      HeaderCells.Add(New HeaderCell("Hours", ws, 1, 10, 1, 10))

      HeaderCells.ForEach(Sub(c) c.FormatRegion())

      Dim CurrentRow As Integer = 2
      Dim SameDateCount As Integer = 0
      Dim DateGroup As Date = SummaryTable.Rows(0)("StartDate")

      Dim colChar As Char = "A"
      Dim rowChar As Char

      For Each SummaryTableRow As DataRow In SummaryTable.Rows

        If (DateGroup <> SummaryTableRow("StartDate")) Then 'Or Singular.Misc.IsNullNothingOrEmpty(SummaryTable("StartDate"))

          Dim DateRegion As WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(CurrentRow - SameDateCount, 0, CurrentRow - 1, 0)
          DateRegion.Value = DateGroup
          DateRegion.CellFormat.Alignment = HorizontalCellAlignment.Center
          DateRegion.CellFormat.Alignment = VerticalCellAlignment.Center
          DateRegion.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

          PopulateCell(DateGroup, ws.Rows(CurrentRow).Cells(0), ExcelDefaultableBoolean.True)
          For i As Integer = 2 To 10
            'Inserts the Summary/Totals row under each date grouping
            rowChar = Chr(Asc(colChar) + i)
            ws.Rows(CurrentRow).Cells(i).ApplyFormula("=Sum(" & rowChar.ToString & "" & (CurrentRow - SameDateCount + 1) & ":" & rowChar.ToString & "" & (CurrentRow) & ")")

            ws.Rows(CurrentRow).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
            ws.Rows(CurrentRow).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
            ws.Rows(CurrentRow).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
            ws.Rows(CurrentRow).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
            ws.Rows(CurrentRow).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
          Next
          ws.Rows(CurrentRow).Cells(1).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
          ws.Rows(CurrentRow).Cells(1).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
          ws.Rows(CurrentRow).Cells(1).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
          ws.Rows(CurrentRow).Cells(1).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
          ws.Rows(CurrentRow).Cells(1).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
          ws.Rows(CurrentRow).Cells(0).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
          ws.Rows(CurrentRow).Cells(0).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
          CurrentRow += 1
          SameDateCount = 0
        End If

        For i As Integer = 1 To 10
          'Fills in each cell
          PopulateCell(SummaryTableRow(i), ws.Rows(CurrentRow).Cells(i))
        Next

        '-Column 1 - Formats heading for this column as well (thats why it's not included in the loop)
        ws.Rows(CurrentRow).Cells(0).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws.Rows(CurrentRow).Cells(0).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(CurrentRow).Cells(0).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(CurrentRow).Cells(0).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
        '-Column 2
        ws.Rows(CurrentRow).Cells(1).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
        '-Column 4
        ws.Rows(CurrentRow).Cells(3).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
        '-Column 6
        ws.Rows(CurrentRow).Cells(5).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
        '-Column 8
        ws.Rows(CurrentRow).Cells(7).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
        '-Column 11
        ws.Rows(CurrentRow).Cells(10).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin


        SameDateCount += 1
        CurrentRow += 1
        DateGroup = SummaryTableRow("StartDate")
      Next

      Dim DateRegionEnd As WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(CurrentRow - SameDateCount, 0, CurrentRow - 1, 0)
      DateRegionEnd.Value = DateGroup
      DateRegionEnd.CellFormat.Alignment = HorizontalCellAlignment.Center
      DateRegionEnd.CellFormat.Alignment = VerticalCellAlignment.Center
      DateRegionEnd.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

      PopulateCell(DateGroup, ws.Rows(CurrentRow).Cells(0), ExcelDefaultableBoolean.True)
      For i As Integer = 2 To 10
        'Inserts the Summary/Totals row under each date grouping
        rowChar = Chr(Asc(colChar) + i)
        ws.Rows(CurrentRow).Cells(i).ApplyFormula("=Sum(" & rowChar.ToString & "" & (CurrentRow - SameDateCount + 1) & ":" & rowChar.ToString & "" & (CurrentRow) & ")")

        ws.Rows(CurrentRow).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws.Rows(CurrentRow).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(CurrentRow).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(CurrentRow).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(CurrentRow).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      Next
      ws.Rows(CurrentRow).Cells(1).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
      ws.Rows(CurrentRow).Cells(1).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
      ws.Rows(CurrentRow).Cells(1).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
      ws.Rows(CurrentRow).Cells(1).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
      ws.Rows(CurrentRow).Cells(1).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      ws.Rows(CurrentRow).Cells(0).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
      ws.Rows(CurrentRow).Cells(0).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin

      '<<<<<Basic Formatting Specfic to this Report>>>>>:

      'Column sizing (pixels * 36.58)-Always rounded up
      ws.Columns(0).Width = 3293
      ws.Columns(1).Width = 2744
      ws.Columns(2).Width = 2342
      ws.Columns(3).Width = 2342
      ws.Columns(4).Width = 2342
      ws.Columns(5).Width = 2342
      ws.Columns(6).Width = 3293
      ws.Columns(7).Width = 3293
      ws.Columns(8).Width = 2342
      ws.Columns(9).Width = 2342
      ws.Columns(10).Width = 2342
      'Column Alignment
      For i As Integer = 2 To 10
        ws.Columns(i).CellFormat.Alignment = HorizontalCellAlignment.Right
      Next
      '-Column 1
      ws.Columns(0).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
      ws.Columns(0).CellFormat.Alignment = HorizontalCellAlignment.Center
      '-Column 2
      ws.Columns(1).CellFormat.Alignment = HorizontalCellAlignment.Center

      'Loop the MAIN Headers
      For i As Integer = 0 To 10
        ws.Rows(0).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws.Rows(0).Cells(i).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
        ws.Rows(0).Cells(i).CellFormat.Alignment = HorizontalCellAlignment.Center
        ws.Rows(0).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(0).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(0).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(0).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      Next
      'Format the SUB headers
      For i As Integer = 1 To 10
        ws.Rows(1).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws.Rows(1).Cells(i).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
        ws.Rows(1).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(1).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(1).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws.Rows(1).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      Next
      '====================END TABLE 1=========================

      '============TABLE 2 (Summary Version)===================
      Dim ws2 As Worksheet = wb.Worksheets.Add("Summary")
      Dim SummaryTable2 As DataTable = Data.Tables(1)

      Dim HeaderCells2 As List(Of HeaderCell) = New List(Of HeaderCell) 'Create a List that holds the header values and their cell-range (used later for merging headings correctly)

      HeaderCells2.Add(New HeaderCell("Date", ws2, 0, 0, 1, 0))

      HeaderCells2.Add(New HeaderCell("SCCR Permanent", ws2, 0, 1, 0, 3))
      HeaderCells2.Add(New HeaderCell("HRs", ws2, 1, 1, 1, 1))
      HeaderCells2.Add(New HeaderCell("Events", ws2, 1, 2, 1, 2))
      HeaderCells2.Add(New HeaderCell("Hours", ws2, 1, 3, 1, 3))

      HeaderCells2.Add(New HeaderCell("SCCR Freelance", ws2, 0, 4, 0, 6))
      HeaderCells2.Add(New HeaderCell("HRs", ws2, 1, 4, 1, 4))
      HeaderCells2.Add(New HeaderCell("Events", ws2, 1, 5, 1, 5))
      HeaderCells2.Add(New HeaderCell("Hours", ws2, 1, 6, 1, 6))

      HeaderCells2.Add(New HeaderCell("MCR Permanent", ws2, 0, 7, 0, 9))
      HeaderCells2.Add(New HeaderCell("HRs", ws2, 1, 7, 1, 7))
      HeaderCells2.Add(New HeaderCell("Events", ws2, 1, 8, 1, 8))
      HeaderCells2.Add(New HeaderCell("Hours", ws2, 1, 9, 1, 9))

      HeaderCells2.Add(New HeaderCell("FD Controllers", ws2, 0, 10, 0, 13))
      HeaderCells2.Add(New HeaderCell("HRs", ws2, 1, 10, 1, 10))
      HeaderCells2.Add(New HeaderCell("MCRs", ws2, 1, 11, 1, 11))
      HeaderCells2.Add(New HeaderCell("Events", ws2, 1, 12, 1, 12))
      HeaderCells2.Add(New HeaderCell("Hours", ws2, 1, 13, 1, 13))

      HeaderCells2.ForEach(Sub(c) c.FormatRegion())

      Dim CurrentRow2 As Integer = 2

      For Each SummaryTableRow2 As DataRow In SummaryTable2.Rows 'Goes through each row in the Summary table *declared at start of each table
        For i As Integer = 0 To 13
          'Fills in each cell
          PopulateCell(SummaryTableRow2(i), ws2.Rows(CurrentRow2).Cells(i))
          ws2.Rows(CurrentRow2).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
          ws2.Rows(CurrentRow2).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
          ws2.Rows(CurrentRow2).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
          ws2.Rows(CurrentRow2).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
          If CurrentRow2 Mod 2 > 0 Then
            ws2.Rows(CurrentRow2).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
          End If
        Next
        CurrentRow2 += 1
      Next

      'Column sizing (pixels * 36.58)-Always rounded up
      ws2.Columns(0).Width = 3293
      ws2.Columns(1).Width = 1829
      ws2.Columns(2).Width = 2342
      ws2.Columns(3).Width = 2342
      ws2.Columns(4).Width = 1829
      ws2.Columns(5).Width = 2342
      ws2.Columns(6).Width = 2342
      ws2.Columns(7).Width = 1829
      ws2.Columns(8).Width = 2342
      ws2.Columns(9).Width = 2342
      ws2.Columns(10).Width = 1829
      ws2.Columns(11).Width = 2195
      ws2.Columns(12).Width = 2342
      ws2.Columns(13).Width = 2342

      'Loop through and format the MAIN Headers
      For i As Integer = 0 To 13
        ws2.Rows(0).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws2.Rows(0).Cells(i).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
        ws2.Rows(0).Cells(i).CellFormat.Alignment = HorizontalCellAlignment.Center
        ws2.Rows(0).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(0).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(0).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(0).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      Next
      'Format the SUB headers
      For i As Integer = 1 To 13
        ws2.Rows(1).Cells(i).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Drawing.Color.LightGray)
        ws2.Rows(1).Cells(i).CellFormat.VerticalAlignment = VerticalCellAlignment.Center
        ws2.Rows(1).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(1).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(1).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
        ws2.Rows(1).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      Next

      Return wb

    End Function


  End Class

  Public Class PlayoutDailyEventSummaryDetailedCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

  End Class

  Public Class PlayoutDailyEventSummaryDetailedControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutDailyEventSummaryDetailedCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Playout MCR Shift Projections"
  Public Class MCRShiftProjections
    Inherits ReportBase(Of MCRShiftProjectionsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SS Playout Operations - MCR Shift Projections"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptMCRShiftHoursComparisonReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MCRShiftProjectionsControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).TableName = "MCR Hour Projections"

    End Sub

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Dim ws2 As Worksheet = Workbook.Worksheets(0)

      ws2.Columns(8).CellFormat.FormatString = "0.00%"

    End Sub


  End Class

  Public Class MCRShiftProjectionsCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts")>
    Public Property SystemID As Integer?

  End Class

  Public Class MCRShiftProjectionsControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutISPHoursSummaryCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class
#End Region

#Region " MCR Projections"

  Public Class PlayoutOperationsMCRShiftWeekly
    Inherits ReportBase(Of PlayoutOperationsMCRShiftWeeklyCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "MCR Projections"
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsMCRShiftReportWeekly]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsMCRShiftWeeklyControl)
      End Get
    End Property

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

  End Class

  Public Class PlayoutOperationsMCRShiftWeeklyCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class PlayoutOperationsMCRShiftWeeklyControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftWeeklyCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region "MCR Shift Report (New Policy)"

  Public Class PlayoutOperationsMCRShiftNewPolicy
    Inherits ReportBase(Of PlayoutOperationsMCRShiftNewPolicyCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "MCR Shift Report (New Policy)"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptPlayoutOperationsMCRShiftNewPolicy)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsMCRShiftNewPolicy]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsMCRShiftNewPolicyControl)
      End Get
    End Property
  End Class

  Public Class PlayoutOperationsMCRShiftNewPolicyCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class PlayoutOperationsMCRShiftNewPolicyControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftNewPolicyCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "BookingsReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

#Region "SCCR Shift Report (New Policy)"

  Public Class PlayoutOperationsSCCRShiftNewPolicy
    Inherits ReportBase(Of PlayoutOperationsSCCRShiftNewPolicyCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SCCR Shift Report (New Policy)"
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptPlayoutOperationsSCCRShiftNewPolicy)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptPlayoutOperationsSCCRShiftNewPolicy]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PlayoutOperationsSCCRShiftNewPolicyControl)
      End Get
    End Property
  End Class

  Public Class PlayoutOperationsSCCRShiftNewPolicyCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property ROHumanResourceSelectedList As List(Of ROHumanResourceTravelReq) = New List(Of ROHumanResourceTravelReq)

    <Display(Name:="Human Resources")>
    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room", FilterMethodName:="BookingsReport.AllowedRooms")>
    Public Property RoomID As Integer?

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class PlayoutOperationsSCCRShiftNewPolicyControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of PlayoutOperationsMCRShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  'With .Helpers.Bootstrap.Row
                  '  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  '    With .Helpers.Bootstrap.FormControlFor(Function(c) c.RoomID, BootstrapEnums.InputSize.Small, , "Room")
                  '    End With
                  '  End With
                  'End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 8)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                      With .Helpers.FieldSet("Human Resource Filters")
                        .Attributes("id") = "HumanResourceFilters"
                        .AddClass("FadeHide")
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                            End With
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ContractTypeID, BootstrapEnums.InputSize.Small, , "Contract Type")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.FieldSet("Human Resources")
                      .Attributes("id") = "HumanResources"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 2)
                  With .Helpers.FieldSet("Selected Human Resources")
                    .Attributes("id") = "SelectedHumanResources"
                    .AddClass("FadeHide")
                    With .Helpers.ForEachTemplate(Of ROHumanResourceFind)(Function(d) d.ROHumanResourceSelectedList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", , , , , )
                          .Button.AddBinding(KnockoutBindingString.click, "PlayoutOperationsSCCRShiftReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With

    End Sub
  End Class

#End Region

End Class

