﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly

Public Class EasyReports

#Region "Bookings by Discipline"

  Public Class BookingsByDiscipline
    Inherits ReportBase(Of BookingsByDisciplineCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Bookings by Discipline"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      cmd.CommandText = "[RptProcs].[rptEasyBookingsByDiscipline]"

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(BookingsByDisciplineCriteriaControl)
      End Get
    End Property

  End Class

  Public Class BookingsByDisciplineCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Read Only System List"), ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

  End Class

  Public Class BookingsByDisciplineCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of BookingsByDisciplineCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 3, 2)
            With .Helpers.FieldSet("Sub-Depts and Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "BookingsByDiscipline.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource Details"

  Public Class HumanResourceDetails
    Inherits ReportBase(Of HumanResourceDetailsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.[rptEasyHumanResourceDetails]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HumanResourceDetailsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceDetailsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Drivers Licence")>
    Public Property DriversLicenceInd As Boolean?

    <Display(Name:="Discipline", Description:="", Order:=4), _
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Select Discipline...")>
    Public Property DisciplineID As Integer?

    <Display(Name:="Human Resource", Order:=3), ClientOnly>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Human Resource", Order:=3), ClientOnly>
    Public Property HumanResource As List(Of String)

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?

    <ClientOnly>
    Public Property ROProductionAreaList As List(Of ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.ToList

    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

    <Display(Name:="Firstname", Order:=3), ClientOnly>
    Public Property FirstName As String = ""

    <Display(Name:="Surname", Order:=3), ClientOnly>
    Public Property Surname As String = ""

    <Display(Name:="Preferred Name", Order:=3), ClientOnly>
    Public Property PreferredName As String = ""

  End Class

  Public Class HumanResourceDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 4)
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "HumanResourceDetails.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "HumanResourceDetails.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                  With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                    With .Helpers.FieldSet("Human Resource Filters")
                      .Attributes("id") = "HumanResourceFilters"
                      .AddClass("FadeHide")
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                        End With
                      End With
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Div
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                            .Editor.Attributes("placeholder") = "Discipline"
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                      With .Helpers.Div
                        With .Helpers.Bootstrap.StateButton(Function(d As HumanResourceDetailsCriteria) d.DriversLicenceInd,
                                             "Has Licence", "Has Licence", "btn-success", , , , )
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 8)
                With .Helpers.FieldSet("Human Resources")
                  .AddClass("FadeHide")
                  .Attributes("id") = "HumanResources"
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                      With .Helpers.If(Function(c As HumanResourceDetailsCriteria) Not c.BusyHRList)
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "HumanResourceDetails.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With

            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

#End Region

#Region "JC Dashboard"

  Public Class JCDashboard
    Inherits ReportBase(Of JCDashboardCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "JC Dashboard"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      cmd.CommandText = "RptProcs.[rptEasyJCDashboard]"

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(JCDashboardCriteriaControl)
      End Get
    End Property

  End Class

  Public Class JCDashboardCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="SystemID")>
    Public Property SystemID As Integer

    <Display(Name:="Read Only System List"), ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

  End Class

  Public Class JCDashboardCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of JCDashboardCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 3, 2)
            With .Helpers.FieldSet("Sub-Depts and Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "JCDashboard.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

End Class
