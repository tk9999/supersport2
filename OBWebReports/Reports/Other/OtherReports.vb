﻿Imports Singular.Reporting
Imports Singular
Imports OBLib
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly

Public Class OtherReports

#Region " Room Bookings Data "

  Public Class RoomBookingsDataReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class RoomBookingsDataReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of RoomBookingsDataReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class RoomBookingsDataReport
    Inherits Singular.Reporting.ReportBase(Of RoomBookingsDataReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Room Bookings Data"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptRoomBookingsData"
      cmd.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(RoomBookingsDataReportCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

  End Class

#End Region

#Region " OB Bookings Data "

  Public Class OBBookingsDataReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class OBBookingsDataReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBBookingsDataReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class OBBookingsDataReport
    Inherits Singular.Reporting.ReportBase(Of OBBookingsDataReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "OB Bookings Data"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptOBBookingsData"
      cmd.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(OBBookingsDataReportCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

  End Class

#End Region

End Class
