﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.ICR.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Infragistics.Documents.Excel
Imports Singular.DataAnnotations
Imports System.Globalization
Imports System.Threading
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly
Imports System.Drawing

Public Class ICRReports

#Region " HR Shift Payment Hours "

  Public Class HRShiftPayment
    Inherits Singular.Reporting.ReportBase(Of HRShiftPaymentCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Shift Payment Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceShiftPayment"
      cmd.CommandTimeout = 120 ' this seems to be timing out on live, so I added this in.  BWebber
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptHRShiftHours)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add(New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRShiftPaymentCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HRShiftPaymentCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Team", Order:=1)>
    Public Property TeamID As Integer?

    '<Display(Name:="Team", Order:=2), ClientOnly>
    'Public Property Team As String

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2)>
    Public Property ProductionAreaIDs As List(Of Integer)

    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class HRShiftPaymentCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    
    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRShiftPaymentCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteriaNew) d.Team, "ICRShiftPaymentReport.FindSystemTeam($element)",
          '                           "Search For Team", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.FieldSet("Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            '  With .Helpers.FieldSet("Human Resources")
            '    .Attributes("id") = "HumanResources"
            '    .AddClass("FadeHide")
            '    With .Helpers.Div
            '      With .Helpers.Div
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
            '              .Editor.Attributes("placeholder") = "Search Human Resource"
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
            '            End With
            '          End With
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ICRShiftPaymentReport.RefreshHR()")
            '              .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
            '            End With
            '          End With
            '        End With
            '      End With

            '      .Helpers.HTML.NewLine()
            '      With .Helpers.Div
            '        .Attributes("id") = "HRDiv"
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) Not c.BusyHRList)
            '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
            '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
            '                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
            '                    .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddHumanResource($data)")
            '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
            '                    .Button.AddClass("btn-block buttontext")
            '                  End With
            '                End With
            '              End With
            '            End With
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) c.BusyHRList)
            '              With .Helpers.Div
            '                With .Helpers.HTMLTag("i")
            '                  .AddClass("fa fa-cog fa-lg fa-spin")
            '                End With
            '                With .Helpers.HTMLTag("span")
            '                  .Helpers.HTML("Updating...")
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class FreelancerHRShiftPayment
    Inherits Singular.Reporting.ReportBase(Of FreelancerHRShiftPaymentCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Freelancer Human Resource Shift Payment"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptFreelanceHRShiftHours)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceFreelanceShiftPayment"
      'Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      'cmd.Parameters.Remove(hr)
      'Dim t As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Team").FirstOrDefault
      'cmd.Parameters.Remove(t)
      'Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      'cmd.Parameters.Remove(rosl)
      'Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      'cmd.Parameters.Remove(ropal)
      'Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      'cmd.Parameters.Remove(rohrl)
      'Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      'cmd.Parameters.Remove(bhl)
      'Dim sid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@SystemID").FirstOrDefault
      'cmd.Parameters.Remove(sid)
      'Dim paid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaID").FirstOrDefault
      'cmd.Parameters.Remove(paid)
      'Dim tid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@TeamID").FirstOrDefault
      'cmd.Parameters.Remove(tid)
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      'cmd.Parameters.AddWithValue("@TeamID", Nothing)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FreelancerHRShiftPaymentCriteriaControl)
      End Get
    End Property

  End Class

  Public Class FreelancerHRShiftPaymentCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Team", Order:=1)>
    Public Property TeamID As Integer?

    '<Display(Name:="Team", Order:=2), ClientOnly>
    'Public Property Team As String

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2)>
    Public Property ProductionAreaIDs As List(Of Integer)

    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class FreelancerHRShiftPaymentCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FreelancerHRShiftPaymentCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteriaNew) d.Team, "ICRShiftPaymentReport.FindSystemTeam($element)",
          '                           "Search For Team", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.FieldSet("Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            '  With .Helpers.FieldSet("Human Resources")
            '    .Attributes("id") = "HumanResources"
            '    .AddClass("FadeHide")
            '    With .Helpers.Div
            '      With .Helpers.Div
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
            '              .Editor.Attributes("placeholder") = "Search Human Resource"
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
            '            End With
            '          End With
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ICRShiftPaymentReport.RefreshHR()")
            '              .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
            '            End With
            '          End With
            '        End With
            '      End With

            '      .Helpers.HTML.NewLine()
            '      With .Helpers.Div
            '        .Attributes("id") = "HRDiv"
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) Not c.BusyHRList)
            '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
            '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
            '                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
            '                    .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddHumanResource($data)")
            '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
            '                    .Button.AddClass("btn-block buttontext")
            '                  End With
            '                End With
            '              End With
            '            End With
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) c.BusyHRList)
            '              With .Helpers.Div
            '                With .Helpers.HTMLTag("i")
            '                  .AddClass("fa fa-cog fa-lg fa-spin")
            '                End With
            '                With .Helpers.HTMLTag("span")
            '                  .Helpers.HTML("Updating...")
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

#Region "Staff Shift Details"

  Public Class StaffShiftDetailsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    '<Display(Name:="System", Order:=1)>
    'Public Property SystemID As Integer = 4

  End Class

  Public Class StaffShiftDetails
    Inherits Singular.Reporting.ReportBase(Of StaffShiftDetailsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Shift Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceStaffShiftDetails"
      'Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      'cmd.Parameters.Remove(hr)
      'Dim t As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Team").FirstOrDefault
      'cmd.Parameters.Remove(t)
      'Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      'cmd.Parameters.Remove(rosl)
      'Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      'cmd.Parameters.Remove(ropal)
      'Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      'cmd.Parameters.Remove(rohrl)
      'Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      'cmd.Parameters.Remove(bhl)
      'Dim sid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@SystemID").FirstOrDefault
      'cmd.Parameters.Remove(sid)
      'Dim paid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaID").FirstOrDefault
      'cmd.Parameters.Remove(paid)
      'cmd.Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
      'cmd.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptStaffShiftDetail)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StaffShiftDetailsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class StaffShiftDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of StaffShiftDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        'With .Helpers.FieldSet("Criteria")
        '  With .Helpers.Bootstrap.Row
        '    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '      With .Helpers.FieldSet("Sub-Depts")
        '        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
        '          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '              .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
        '              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
        '              .Button.AddClass("btn-block buttontext")
        '            End With
        '          End With
        '        End With
        '      End With
        '      With .Helpers.Div
        '        With .Helpers.FieldSet("Areas")
        '          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
        '            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
        '              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '                  .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
        '                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
        '                  .Button.AddClass("btn-block buttontext")
        '                End With
        '              End With
        '            End With
        '          End With
        '        End With
        '      End With
        '    End With
        '    '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '    '    With .Helpers.Div
        '    '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteria) d.Team, "ICRShiftPaymentReport.FindSystemTeam($element)",
        '    '                           "Search For Team", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
        '    '      End With
        '    '    End With
        '    '  End With
        '    'End With
        '    'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '    '  With .Helpers.Bootstrap.Row
        '    '    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '    '      With .Helpers.FieldSet("Sub-Depts")
        '    '        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
        '    '          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '    '            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '    '              .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
        '    '              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
        '    '              .Button.AddClass("btn-block buttontext")
        '    '            End With
        '    '          End With
        '    '        End With
        '    '      End With
        '    '      With .Helpers.Div
        '    '        With .Helpers.FieldSet("Areas")
        '    '          With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
        '    '            With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
        '    '              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '    '                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '    '                  .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
        '    '                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
        '    '                  .Button.AddClass("btn-block buttontext")
        '    '                End With
        '    '              End With
        '    '            End With
        '    '          End With
        '    '        End With
        '    '      End With
        '    '    End With
        '    '    'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '    '    '  With .Helpers.FieldSet("Human Resources")
        '    '    '    .Attributes("id") = "HumanResources"
        '    '    '    .AddClass("FadeHide")
        '    '    '    With .Helpers.Div
        '    '    '      With .Helpers.Div
        '    '    '        With .Helpers.Bootstrap.Row
        '    '    '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
        '    '    '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
        '    '    '              .Editor.Attributes("placeholder") = "Search Human Resource"
        '    '    '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
        '    '    '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
        '    '    '            End With
        '    '    '          End With
        '    '    '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
        '    '    '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ICRShiftPaymentReport.RefreshHR()")
        '    '    '              .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
        '    '    '            End With
        '    '    '          End With
        '    '    '        End With
        '    '    '      End With

        '    '    '      .Helpers.HTML.NewLine()
        '    '    '      With .Helpers.Div
        '    '    '        .Attributes("id") = "HRDiv"
        '    '    '        With .Helpers.Bootstrap.Row
        '    '    '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '    '    '            With .Helpers.If(Function(c As HRShiftPaymentCriteria) Not c.BusyHRList)
        '    '    '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
        '    '    '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '    '    '                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
        '    '    '                    .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddHumanResource($data)")
        '    '    '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
        '    '    '                    .Button.AddClass("btn-block buttontext")
        '    '    '                  End With
        '    '    '                End With
        '    '    '              End With
        '    '    '            End With
        '    '    '            With .Helpers.If(Function(c As HRShiftPaymentCriteria) c.BusyHRList)
        '    '    '              With .Helpers.Div
        '    '    '                With .Helpers.HTMLTag("i")
        '    '    '                  .AddClass("fa fa-cog fa-lg fa-spin")
        '    '    '                End With
        '    '    '                With .Helpers.HTMLTag("span")
        '    '    '                  .Helpers.HTML("Updating...")
        '    '    '                End With
        '    '    '              End With
        '    '    '            End With
        '    '    '          End With
        '    '    '        End With
        '    '    '      End With
        '    '    '    End With
        '    '    '  End With
        '    '    'End With
        '    '  End With
        '  End With
        'End With
      End With
    End Sub
  End Class

#End Region

#Region " Shift Roaster "

  Public Class ShiftRoaster
    Inherits Singular.Reporting.ReportBase(Of ShiftRoasterCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Shift Roaster"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

    End Sub
  End Class

  Public Class ShiftRoasterCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Required(ErrorMessage:="Start Date Required")>
    Public Property StartDate As Date?
    <Required(ErrorMessage:="End Date Required")>
    Public Property EndDate As Date?

    Public Property TeamIDs As String = ""

  End Class

#End Region

#Region " ICR No Meal Credit "

  Public Class ICRNoMealsCredit
    Inherits Singular.Reporting.ReportBase(Of ICRNoMealsCreditCriteria)

    Private Const WorksheetName As String = "ICR No Meal Credit Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ICR No Meal Credit"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptICRNoMealsCredit"
      While cmd.Parameters.Count > 0
        cmd.Parameters.Remove(cmd.Parameters.Last)
      End While
      cmd.Parameters.AddWithValue("@MRMonthID", Me.ReportCriteria.MRMonth)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ICRNoMealsCreditCriteriaControl)
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Start Date", "End Date", "Meal Date", "Comments", "Authorised"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      For Col As Integer = 0 To HeaderName.GetCount - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          ' Each Human resource Row
          If Col = 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > 0 Then
            SetText(ws, Row, Col, Data.Tables.Item(0).Rows.Item(Row - 1)(1), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim thisData = Nothing
      Dim DateVar As DateTime? = Nothing
      Dim res As String = ""
      For Row As Integer = 1 To (Data.Tables.Item(0).Rows.Count) ' Human Resources
        For Col As Integer = 1 To HeaderName.GetCount
          thisData = Data.Tables.Item(0).Rows.Item(Row - 1)(Col)
          If IsDate(thisData) Then
            DateVar = thisData
            If Col >= 1 AndAlso Col <= 3 Then
              res = (Convert.ToDateTime(DateVar).ToString("dd-MMM-yy HH:mm:ss"))
              thisData = res
            ElseIf Col = 4 Then
              res = Convert.ToDateTime(DateVar).ToString("dd-MMM-yy")
              thisData = res
            End If
          End If
          SetText(ws, Row, Col - 1, thisData, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Next
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

  Public Class ICRNoMealsCreditCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="MR Month", Order:=1), Required,
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), Source:=DropDownWeb.SourceType.CommonData, UnselectedText:="Month")>
    Public Property MRMonth As Integer?

  End Class

  Public Class ICRNoMealsCreditCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ICRNoMealsCreditCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ICRNoMealsCreditCriteria) d.MRMonth, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


#End Region

#Region " Extra Shift Pattern Conflict "

  Public Class ExtraShiftPatternConflict
    Inherits Singular.Reporting.ReportBase(Of ExtraShiftPatternConflictCriteria)

    Private Const WorksheetName As String = "Extra Shift Conflict Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Extra Shift Pattern Conflict"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptExtraShiftToPatternConflict"
      'While cmd.Parameters.Count > 0
      '    cmd.Parameters.Remove(cmd.Parameters.Last)
      'End While
      'cmd.Parameters.AddWithValue("@MRMonthID", Me.ReportCriteria.MRMonth)
      'cmd.Parameters.AddWithValue("@StartDate", Me.ReportCriteria.StartDate)
      'cmd.Parameters.AddWithValue("@EndDate", Me.ReportCriteria.EndDate)
      'cmd.Parameters.AddWithValue("@ShowPatternStartDiff", Me.ReportCriteria.ShowPatternStartDiff)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ExtraShiftPatternConflictCriteriaControl)
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Schedule Date", "Start Date", "End Date", "Pattern Start Date", "Pattern End Date"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      For Col As Integer = 0 To HeaderName.GetCount - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          ' Each Human resource Row
          If Col = 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > 0 Then
            SetText(ws, Row, Col, Data.Tables.Item(0).Rows.Item(Row - 1)(1), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim thisData = Nothing
      Dim DateVar As DateTime? = Nothing
      Dim res As String = ""
      For Row As Integer = 1 To (Data.Tables.Item(0).Rows.Count) ' Human Resources
        For Col As Integer = 1 To HeaderName.GetCount
          thisData = Data.Tables.Item(0).Rows.Item(Row - 1)(Col)
          If IsDate(thisData) Then
            DateVar = thisData
            If Col <> 2 Then
              res = (Convert.ToDateTime(DateVar).ToString("dd-MMM-yy HH:mm:ss"))
              thisData = res
            Else
              res = Convert.ToDateTime(DateVar).ToString("dd-MMM-yy")
              thisData = res
            End If
          End If
          SetText(ws, Row, Col - 1, thisData, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Next
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

  Public Class ExtraShiftPatternConflictCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    '<Required(ErrorMessage:="Start Date Required")>
    'Public Property StartDate As Date?

    '<Required(ErrorMessage:="End Date Required")>
    'Public Property EndDate As Date?

    '<Display(Name:="MR Month", Order:=1), Required,
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), UnselectedText:="Month")>
    'Public Property MRMonth As Integer?

    Public Property ShowPatternStartDiff As Boolean = True

  End Class

  Public Class ExtraShiftPatternConflictCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ExtraShiftPatternConflictCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
          '        With .Helpers.Bootstrap.FormControlFor(Function(d As ExtraShiftPatternConflictCriteria) d.MRMonth, Singular.Web.BootstrapEnums.InputSize.Small)
          '          .Editor.Attributes("placeholder") = "Month"
          '        End With
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  With .Helpers.Bootstrap.StateButton(Function(d As ExtraShiftPatternConflictCriteria) d.ShowPatternStartDiff,
                                                   "Show Shift Start Different to Pattern Start", "Show Shift Start Different to Pattern Start", , , , "fa-hand-o-up", )
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


#End Region

#Region " Shift Pattern Details "

  Public Class ShiftPatternDetails
    Inherits Singular.Reporting.ReportBase(Of ShiftPatternDetailsCriteria)

    Private Const WorksheetName As String = "Shift Pattern Details Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Shift Pattern Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptShiftPatternDetails"
      While cmd.Parameters.Count > 0
        cmd.Parameters.Remove(cmd.Parameters.Last)
      End While
      cmd.Parameters.AddWithValue("@StartDate", Me.ReportCriteria.StartDate)
      cmd.Parameters.AddWithValue("@EndDate", Me.ReportCriteria.EndDate)
      cmd.Parameters.AddWithValue("@PatternID", Me.ReportCriteria.SystemAreaShiftPatternID)
      cmd.Parameters.AddWithValue("@ShiftTypeID", Me.ReportCriteria.ShiftTypeID)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ShiftPatternDetailsCriteriaControl)
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Pattern Name", "Schedule Date", "Start Date", "End Date", "Pattern Start Date", "Pattern End Date", "Shift Type"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      For Col As Integer = 0 To HeaderName.GetCount - 1
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True
        ws.Columns(Col).Width = DefaultColumnWidth * 400
        If Col = 1 Then
          ws.Columns(Col).Width = DefaultColumnWidth * 600
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          ' Each Human resource Row
          If Col = 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col = 0 AndAlso Row > 0 Then
            SetText(ws, Row, Col, Data.Tables.Item(0).Rows.Item(Row - 1)(1), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf Col > 0 AndAlso Row = 0 Then
            SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      Dim thisData = Nothing
      Dim DateVar As DateTime? = Nothing
      Dim res As String = ""
      For Row As Integer = 1 To (Data.Tables.Item(0).Rows.Count) ' Human Resources
        For Col As Integer = 1 To HeaderName.GetCount
          thisData = Data.Tables.Item(0).Rows.Item(Row - 1)(Col - 1)
          If IsDate(thisData) Then
            DateVar = thisData
            If Col <> 3 Then
              res = (Convert.ToDateTime(DateVar).ToString("dd-MMM-yy HH:mm:ss"))
              thisData = res
            Else
              res = Convert.ToDateTime(DateVar).ToString("dd-MMM-yy")
              thisData = res
            End If
          End If
          SetText(ws, Row, Col - 1, thisData, ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Next
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

  Public Class ShiftPatternDetailsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'Public Property StartDate As Date?

    'Public Property EndDate As Date?

    <System.ComponentModel.DataAnnotations.Display(Name:="Shift Pattern", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList), UnselectedText:="Select Shift Pattern")>
    Public Property SystemAreaShiftPatternID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Shift Type", Order:=2),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ShiftPatterns.ROShiftTypeList), UnselectedText:="Select Shift Type")>
    Public Property ShiftTypeID As Integer?

  End Class

  Public Class ShiftPatternDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ShiftPatternDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ShiftPatternDetailsCriteria) d.SystemAreaShiftPatternID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Shift Pattern"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ShiftPatternDetailsCriteria) d.ShiftTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Shift Type"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


#End Region

#Region " New - HR Shift Payment Hours "

  Public Class HRShiftPaymentNew
    Inherits Singular.Reporting.ReportBase(Of HRShiftPaymentCriteriaNew)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "New - Human Resource Shift Payment Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceShiftPaymentNew"
      cmd.CommandTimeout = 120 ' this seems to be timing out on live, so I added this in.  BWebber
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      DataSet.Relations.Add(New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRShiftPaymentCriteriaControlNew)
      End Get
    End Property

  End Class

  Public Class HRShiftPaymentCriteriaNew
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Team", Order:=1)>
    Public Property TeamID As Integer?

    '<Display(Name:="Team", Order:=2), ClientOnly>
    'Public Property Team As String

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2)>
    Public Property ProductionAreaIDs As List(Of Integer)

    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class HRShiftPaymentCriteriaControlNew
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRShiftPaymentCriteriaNew)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteriaNew) d.Team, "ICRShiftPaymentReport.FindSystemTeam($element)",
          '                           "Search For Team", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.FieldSet("Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            '  With .Helpers.FieldSet("Human Resources")
            '    .Attributes("id") = "HumanResources"
            '    .AddClass("FadeHide")
            '    With .Helpers.Div
            '      With .Helpers.Div
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
            '              .Editor.Attributes("placeholder") = "Search Human Resource"
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
            '            End With
            '          End With
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ICRShiftPaymentReport.RefreshHR()")
            '              .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
            '            End With
            '          End With
            '        End With
            '      End With

            '      .Helpers.HTML.NewLine()
            '      With .Helpers.Div
            '        .Attributes("id") = "HRDiv"
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) Not c.BusyHRList)
            '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
            '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
            '                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
            '                    .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddHumanResource($data)")
            '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
            '                    .Button.AddClass("btn-block buttontext")
            '                  End With
            '                End With
            '              End With
            '            End With
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) c.BusyHRList)
            '              With .Helpers.Div
            '                With .Helpers.HTMLTag("i")
            '                  .AddClass("fa fa-cog fa-lg fa-spin")
            '                End With
            '                With .Helpers.HTMLTag("span")
            '                  .Helpers.HTML("Updating...")
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class FreelancerHRShiftPaymentNew
    Inherits Singular.Reporting.ReportBase(Of FreelancerHRShiftPaymentCriteriaNew)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "New - Freelancer Human Resource Shift Payment"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As System.Type
    '  Get
    '    Return GetType(rptFreelanceHRShiftHours)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceFreelanceShiftPaymentNew"
      'Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      'cmd.Parameters.Remove(hr)
      'Dim t As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Team").FirstOrDefault
      'cmd.Parameters.Remove(t)
      'Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      'cmd.Parameters.Remove(rosl)
      'Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      'cmd.Parameters.Remove(ropal)
      'Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      'cmd.Parameters.Remove(rohrl)
      'Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      'cmd.Parameters.Remove(bhl)
      'Dim sid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@SystemID").FirstOrDefault
      'cmd.Parameters.Remove(sid)
      'Dim paid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaID").FirstOrDefault
      'cmd.Parameters.Remove(paid)
      'Dim tid As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@TeamID").FirstOrDefault
      'cmd.Parameters.Remove(tid)
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      'cmd.Parameters.AddWithValue("@TeamID", Nothing)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FreelancerHRShiftPaymentCriteriaControlNew)
      End Get
    End Property

  End Class

  Public Class FreelancerHRShiftPaymentCriteriaNew
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Team", Order:=1)>
    Public Property TeamID As Integer?

    '<Display(Name:="Team", Order:=2), ClientOnly>
    'Public Property Team As String

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2)>
    Public Property ProductionAreaIDs As List(Of Integer)

    <ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class FreelancerHRShiftPaymentCriteriaControlNew
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FreelancerHRShiftPaymentCriteriaNew)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteriaNew) d.Team, "ICRShiftPaymentReport.FindSystemTeam($element)",
          '                           "Search For Team", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.FieldSet("Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)(Function(d As OBLib.Security.UserSystem) d.UserSystemAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            '  With .Helpers.FieldSet("Human Resources")
            '    .Attributes("id") = "HumanResources"
            '    .AddClass("FadeHide")
            '    With .Helpers.Div
            '      With .Helpers.Div
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
            '              .Editor.Attributes("placeholder") = "Search Human Resource"
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
            '              .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshList.DelayedRefreshList}")
            '            End With
            '          End With
            '          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
            '            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ICRShiftPaymentReport.RefreshHR()")
            '              .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
            '            End With
            '          End With
            '        End With
            '      End With

            '      .Helpers.HTML.NewLine()
            '      With .Helpers.Div
            '        .Attributes("id") = "HRDiv"
            '        With .Helpers.Bootstrap.Row
            '          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) Not c.BusyHRList)
            '              With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
            '                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
            '                  With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
            '                    .Button.AddBinding(KnockoutBindingString.click, "ICRShiftPaymentReport.AddHumanResource($data)")
            '                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
            '                    .Button.AddClass("btn-block buttontext")
            '                  End With
            '                End With
            '              End With
            '            End With
            '            With .Helpers.If(Function(c As HRShiftPaymentCriteriaNew) c.BusyHRList)
            '              With .Helpers.Div
            '                With .Helpers.HTMLTag("i")
            '                  .AddClass("fa fa-cog fa-lg fa-spin")
            '                End With
            '                With .Helpers.HTMLTag("span")
            '                  .Helpers.HTML("Updating...")
            '                End With
            '              End With
            '            End With
            '          End With
            '        End With
            '      End With
            '    End With
            '  End With
            'End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

#Region " Team Hours per Month "

  Public Class TeamHoursPerMonth
    Inherits Singular.Reporting.ReportBase(Of TeamHoursPerMonthCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Hours per Month"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptSystemTeamNumberHoursPerMonth"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TeamHoursPerMonthCriteriaControl)
      End Get
    End Property

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add("Team Stats")

      ws.Columns(0).SetWidth(125, WorksheetColumnWidthUnit.Pixel)
      ws.Columns(1).SetWidth(125, WorksheetColumnWidthUnit.Pixel)
      ws.Columns(2).SetWidth(125, WorksheetColumnWidthUnit.Pixel)

      Dim teamNamesTable As DataTable = Data.Tables(0)
      Dim teamMonthsTable As DataTable = Data.Tables(1)
      Dim teamSummaryStatsTable As DataTable = Data.Tables(2)
      Dim currentRowIndex As Integer = 0

      'Create the 3 column merged cell for the header
      Dim heading As String = "Hours per month (per employee)"
      Dim headingMergedCell As Infragistics.Documents.Excel.WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(currentRowIndex, 0, currentRowIndex, 2)
      headingMergedCell.Value = heading
      headingMergedCell.CellFormat.Alignment = HorizontalCellAlignment.Center
      headingMergedCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

      For Each teamNameRow As DataRow In teamNamesTable.Rows

        'Add TeamName Heading
        currentRowIndex += 1
        Dim currentTeamSystemTeamNumberID As Integer = teamNameRow("SystemTeamNumberID")
        Dim teamNameMergedCell As Infragistics.Documents.Excel.WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(currentRowIndex, 0, currentRowIndex, 2)
        Dim teamNameValue As String = teamNameRow("SystemTeamNumberName")
        teamNameMergedCell.Value = teamNameValue
        teamNameMergedCell.CellFormat.Alignment = HorizontalCellAlignment.Center
        teamNameMergedCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True
        teamNameMergedCell.CellFormat.Fill = CellFill.CreateSolidFill(Color.FromArgb(141, 180, 226))

        'Add TeamName Month Sub Headings
        currentRowIndex += 1
        Dim teamMonthHeadingCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
        teamMonthHeadingCell.Value = "Month"
        teamMonthHeadingCell.CellFormat.Alignment = HorizontalCellAlignment.Center
        teamMonthHeadingCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

        Dim teamMonthTargetHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
        teamMonthTargetHoursCell.Value = "Target Hours"
        teamMonthTargetHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
        teamMonthTargetHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

        Dim teamMonthScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
        teamMonthScheduledHoursCell.Value = "Scheduled Hours"
        teamMonthScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
        teamMonthScheduledHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

        'Loop through the stats to create the months
        For Each teamMonthRow As DataRow In teamMonthsTable.Rows
          Dim currentStatSystemTeamNumberID As Integer = teamMonthRow("SystemTeamNumberID")
          'Per Month Stats---------------------------------------------------------------------
          If CompareSafe(currentTeamSystemTeamNumberID, currentStatSystemTeamNumberID) Then
            currentRowIndex += 1
            Dim teamMonthStatNameCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamMonthStatNameCell.Value = teamMonthRow("MonthName")
            teamMonthStatNameCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamMonthStatRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamMonthStatRequiredHoursCell.Value = teamMonthRow("RequiredHours")
            teamMonthStatRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamMonthStatScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamMonthStatScheduledHoursCell.Value = teamMonthRow("ScheduledHours")
            teamMonthStatScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
          End If
          'currentRowIndex += 1
        Next

        'Loop through the stats summaries
        For Each teamSummaryStatRow As DataRow In teamSummaryStatsTable.Rows
          Dim currentStatSystemTeamNumberID As Integer = teamSummaryStatRow("SystemTeamNumberID")
          If CompareSafe(currentTeamSystemTeamNumberID, currentStatSystemTeamNumberID) Then
            'First Summary Row = Total Per Annum
            currentRowIndex += 1
            Dim teamYearTotalCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamYearTotalCell.Value = "Total Per Annum"
            teamYearTotalCell.CellFormat.Alignment = HorizontalCellAlignment.Left
            teamYearTotalCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearTotalRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamYearTotalRequiredHoursCell.Value = teamSummaryStatRow("TotalRequiredHours")
            teamYearTotalRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearTotalRequiredHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearTotalScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamYearTotalScheduledHoursCell.Value = teamSummaryStatRow("TotalScheduledHours")
            teamYearTotalScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearTotalScheduledHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            'Second Summary Row = Averages
            currentRowIndex += 1
            Dim teamYearAverageCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamYearAverageCell.Value = "Average / Month"
            teamYearAverageCell.CellFormat.Alignment = HorizontalCellAlignment.Left
            teamYearAverageCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearAverageRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamYearAverageRequiredHoursCell.Value = teamSummaryStatRow("AverageRequiredHours")
            teamYearAverageRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearAverageRequiredHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearAverageScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamYearAverageScheduledHoursCell.Value = teamSummaryStatRow("AverageScheduledHours")
            teamYearAverageScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearAverageScheduledHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True
          End If
        Next

        'End Main Loop
      Next

      'ws.Rows(0).Cells(0).Value = If(Data.Tables(1).Rows.Count > 0, "", "No Data")

      'Dim Col As Integer = 1
      'Dim HRCol As Integer = 0
      'Dim ColIndex As Integer = 1

      ''ws.DefaultColumnWidth = 5000

      ''Sets the top row to just a day of the week and the day of the month
      'For Each row In Data.Tables(0).Rows
      '  Dim ColDate As Date = row("CallTimeDate")
      '  ws.Rows(0).Cells(ColIndex).Value = ColDate.ToString("ddd dd")
      '  ColIndex += 1
      'Next
      'Dim RowIndex As Integer = 1

      'ColIndex = 1

      'If Data.Tables(1).Rows.Count > 0 Then

      '  'Table 0 = Shows distinct dates used from top row
      '  'Table 1 = Shows which events are on each day
      '  'Table 2 = Shows the HR details
      '  'Table 3 = Shows the count of the HR
      '  'Table 4 = Shows the number of events per day
      '  'Table 5 = Shows the maximum number of events per the date selection

      '  Dim CurrentDate As Date = Data.Tables(1).Rows(0)("CallTimeDate")
      '  Dim CurrentContractTypeID As Integer = Data.Tables(2).Rows(0)("ContractTypeID")
      '  Dim CurrentEmployeeID As Integer = Data.Tables(2).Rows(0)("HumanResourceID")
      '  Dim FullName As Integer = Data.Tables(3).Rows(0)("FullNameCount")
      '  Dim EventsPerDay As String = Data.Tables(5).Rows(0)("NumberOfEventsPerDay")

      '  'Sets the borders for the entire worksheet
      '  For i As Integer = 0 To Data.Tables(0).Rows.Count
      '    For j As Integer = 0 To EventsPerDay + 15 + FullName
      '      ws.Rows(j).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
      '      ws.Rows(j).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
      '      ws.Rows(j).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
      '      ws.Rows(j).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
      '    Next
      '  Next

      '  'Sets the column width for the HR so that names can be read
      '  For HRCol = 0 To 0
      '    ws.Columns(HRCol).Width = 8000
      '  Next

      '  'Sets the dates in the top row to be bolded and centred
      '  'sets the column width of the rows which have data in them
      '  'Sets the font size to be 12
      '  With ws.Rows(RowIndex = 0)
      '    For i As Integer = 1 To Data.Tables(0).Rows.Count
      '      ws.DefaultColumnWidth = 5000
      '      .Cells(i).CellFormat.Alignment = HorizontalCellAlignment.Center
      '      .Cells(i).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
      '      .Cells(i).CellFormat.Font.Height = 240
      '    Next
      '  End With

      '  'Links each event to its date given in row 1(Excel)
      '  'Iterates through the list of event until the date is no longer equal to columns date
      '  'When no longer equal to column date, moves to the next column until all columns are 
      '  'filled with the correct data
      '  For Each row In Data.Tables(1).Rows
      '    If row("CallTimeDate") <> CurrentDate Then
      '      RowIndex = 1
      '      ColIndex += 1
      '      CurrentDate = row("CallTimeDate")
      '    End If

      '    ws.Rows(RowIndex).Cells(ColIndex).Value = row("EventTime")
      '    RowIndex += 1

      '    With ws.Rows(RowIndex - 1)
      '      .Cells(ColIndex).CellFormat.Font.Height = 180
      '    End With
      '  Next

      '  ColIndex = 0

      '  RowIndex = EventsPerDay + 3
      '  'Links to contractType data
      '  For Each Contracttype As ROContractType In CommonData.Lists.ROContractTypeList
      '    'Places the contract types as values in the cells
      '    'Sets font to be century gothic
      '    'Sets font size to be 12
      '    ws.Rows(RowIndex).Cells(ColIndex).Value = Contracttype.ContractType
      '    ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
      '    ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Name = "Century Gothic"
      '    ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Height = 240
      '    RowIndex += 1

      '    'for each type of contact, puts the fullnames linked to that contractTypeID
      '    'sets font size to be 9
      '    'sets font to be century gothic
      '    For Each DataRow As DataRow In Data.Tables(2).Select("ContractTypeID = " & Contracttype.ContractTypeID)
      '      ws.Rows(RowIndex).Cells(ColIndex).Value = DataRow("FullName")
      '      ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Name = "Century Gothic"
      '      ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Height = 180

      '      RowIndex += 1

      '    Next
      '    'gives a 2 cell gap between each contract type
      '    RowIndex += 2
      '  Next

      '  'Freezes Top Row
      '  ws.DisplayOptions.PanesAreFrozen = True
      '  ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      'End If
      Return wb

    End Function

  End Class

  Public Class TeamHoursPerMonthCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Required(ErrorMessage:="Year is required"),
    DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemYearList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HoursPerMonthReportBO.setSystemYearCriteriaBeforeRefresh",
                PreFindJSFunction:="HoursPerMonthReportBO.triggerSystemYearAutoPopulate",
                AfterFetchJS:="HoursPerMonthReportBO.afterSystemYearRefreshAjax",
                LookupMember:="SystemYear", ValueMember:="SystemYearID", DropDownColumns:={"SubDept", "YearName", "YearStartDate", "YearEndDate"},
                OnItemSelectJSFunction:="HoursPerMonthReportBO.onYearSelected"),
    Display(Name:="Year")>
    Public Property SystemYearID As Integer?

    <ClientOnly>
    Public Property SystemYear As String = ""

  End Class

  Public Class TeamHoursPerMonthCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TeamHoursPerMonthCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.SystemYearID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemYearID, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

#Region " Team Hours per Quarter "

  Public Class TeamHoursPerQuarter
    Inherits Singular.Reporting.ReportBase(Of TeamHoursPerQuarterCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Hours per Quarter"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptSystemTeamNumberHoursPerQuarter"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TeamHoursPerQuarterCriteriaControl)
      End Get
    End Property

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add("Team Stats")

      ws.Columns(0).SetWidth(125, WorksheetColumnWidthUnit.Pixel)
      ws.Columns(1).SetWidth(125, WorksheetColumnWidthUnit.Pixel)
      ws.Columns(2).SetWidth(125, WorksheetColumnWidthUnit.Pixel)

      Dim teamNamesTable As DataTable = Data.Tables(0)
      Dim teamQuartersTable As DataTable = Data.Tables(1)
      Dim teamSummaryStatsTable As DataTable = Data.Tables(2)
      Dim currentRowIndex As Integer = 0

      'Create the 3 column merged cell for the header
      Dim heading As String = "Hours per quarter (per employee)"
      Dim headingMergedCell As Infragistics.Documents.Excel.WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(currentRowIndex, 0, currentRowIndex, 2)
      headingMergedCell.Value = heading
      headingMergedCell.CellFormat.Alignment = HorizontalCellAlignment.Center
      headingMergedCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

      For Each teamNameRow As DataRow In teamNamesTable.Rows

        'Add TeamName Heading
        currentRowIndex += 1
        Dim currentTeamSystemTeamNumberID As Integer = teamNameRow("SystemTeamNumberID")
        Dim teamNameMergedCell As Infragistics.Documents.Excel.WorksheetMergedCellsRegion = ws.MergedCellsRegions.Add(currentRowIndex, 0, currentRowIndex, 2)
        Dim teamNameValue As String = teamNameRow("SystemTeamNumberName")
        teamNameMergedCell.Value = teamNameValue
        teamNameMergedCell.CellFormat.Alignment = HorizontalCellAlignment.Center
        teamNameMergedCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True
        teamNameMergedCell.CellFormat.Fill = CellFill.CreateSolidFill(Color.FromArgb(141, 180, 226))

        'Loop through the stats to create the MonthGroups
        For Each teamQuarterRow As DataRow In teamQuartersTable.Rows
          Dim currentStatSystemTeamNumberID As Integer = teamQuarterRow("SystemTeamNumberID")
          'Per Month Stats---------------------------------------------------------------------
          If CompareSafe(currentTeamSystemTeamNumberID, currentStatSystemTeamNumberID) Then

            'Heading Row
            currentRowIndex += 1
            Dim teamMonthStatNameCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamMonthStatNameCell.Value = teamQuarterRow("GroupName")
            teamMonthStatNameCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamMonthStatNameCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamMonthHeadingCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamMonthHeadingCell.Value = "Target Hours"
            teamMonthHeadingCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamMonthHeadingCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamMonthTargetHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamMonthTargetHoursCell.Value = "Scheduled Hours"
            teamMonthTargetHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamMonthTargetHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True


            'Totals Row
            currentRowIndex += 1
            Dim teamQuarterTotalLabelCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamQuarterTotalLabelCell.Value = "Total"
            teamQuarterTotalLabelCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamQuarterTargetHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamQuarterTargetHoursCell.Value = teamQuarterRow("TargetHours")
            teamQuarterTargetHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamMonthStatRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamMonthStatRequiredHoursCell.Value = teamQuarterRow("AVGTimesheetHours")
            teamMonthStatRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center


            'Variance Row
            currentRowIndex += 1
            Dim teamQuarterVarianceLableCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamQuarterVarianceLableCell.Value = "Variance"
            teamQuarterVarianceLableCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamQuarterVarianceValueCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamQuarterVarianceValueCell.Value = teamQuarterRow("Variance")
            teamQuarterVarianceValueCell.CellFormat.Alignment = HorizontalCellAlignment.Center

          End If
        Next

        'Loop through the stats summaries
        For Each teamSummaryStatRow As DataRow In teamSummaryStatsTable.Rows
          Dim currentStatSystemTeamNumberID As Integer = teamSummaryStatRow("SystemTeamNumberID")
          If CompareSafe(currentTeamSystemTeamNumberID, currentStatSystemTeamNumberID) Then
            'First Summary Row = Headings
            currentRowIndex += 1
            Dim teamYearTotalCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamYearTotalCell.Value = "Year Total"
            teamYearTotalCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearTotalCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearTotalRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamYearTotalRequiredHoursCell.Value = "Target Hours"
            teamYearTotalRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearTotalRequiredHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamYearTotalScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamYearTotalScheduledHoursCell.Value = "Scheduled Hours"
            teamYearTotalScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamYearTotalScheduledHoursCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            'Second Summary Row = Totals
            currentRowIndex += 1
            Dim teamYearAverageCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamYearAverageCell.Value = ""
            teamYearAverageCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamYearAverageRequiredHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(1)
            teamYearAverageRequiredHoursCell.Value = teamSummaryStatRow("TotalTargetHours")
            teamYearAverageRequiredHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            Dim teamYearAverageScheduledHoursCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamYearAverageScheduledHoursCell.Value = teamSummaryStatRow("TotalAverageScheduledHours")
            teamYearAverageScheduledHoursCell.CellFormat.Alignment = HorizontalCellAlignment.Center

            'Third Summary Row = Variance
            currentRowIndex += 1
            Dim teamVarianceLabelCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(0)
            teamVarianceLabelCell.Value = "Variance"
            teamVarianceLabelCell.CellFormat.Alignment = HorizontalCellAlignment.Center
            teamVarianceLabelCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True

            Dim teamVarianceValueCell As Infragistics.Documents.Excel.WorksheetCell = ws.Rows(currentRowIndex).Cells(2)
            teamVarianceValueCell.Value = teamSummaryStatRow("TotalVariance")
            teamVarianceValueCell.CellFormat.Alignment = HorizontalCellAlignment.Center

          End If
        Next

        'End Main Loop
      Next

      Return wb

    End Function

  End Class

  Public Class TeamHoursPerQuarterCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Required(ErrorMessage:="Year is required"),
    DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemYearList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="TeamHoursPerQuarterReportBO.setSystemYearCriteriaBeforeRefresh",
                PreFindJSFunction:="TeamHoursPerQuarterReportBO.triggerSystemYearAutoPopulate",
                AfterFetchJS:="TeamHoursPerQuarterReportBO.afterSystemYearRefreshAjax",
                LookupMember:="SystemYear", ValueMember:="SystemYearID", DropDownColumns:={"SubDept", "YearName", "YearStartDate", "YearEndDate"},
                OnItemSelectJSFunction:="TeamHoursPerQuarterReportBO.onYearSelected"),
    Display(Name:="Year")>
    Public Property SystemYearID As Integer?
    <ClientOnly>
    Public Property SystemYear As String = ""

  End Class

  Public Class TeamHoursPerQuarterCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TeamHoursPerQuarterCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.SystemYearID).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemYearID, BootstrapEnums.InputSize.Small)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

End Class
