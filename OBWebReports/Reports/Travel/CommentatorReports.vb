﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports Singular.DataAnnotations
Imports OBLib.Productions.ReadOnly

Public Class CommentatorReports

#Region "Travel Advances"

  Public Class TravelAdvances
    Inherits ReportBase(Of TravelAdvancesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Travel Advances"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptTravelAdvances)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionTravelAdvances"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TravelAdvancesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class TravelAdvancesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TravelAdvancesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Productions")
                  .Attributes("id") = "Productions"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                          .Attributes("id") = "HRDiv"
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "TravelAdvancesPerPersonReport.AddProduction($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                              .Button.AddClass("btn-block buttontext")
                              .Button.AddClass("add-text-align-left")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

  Public Class TravelAdvancesCriteria
    Inherits DefaultCriteria

    <Display(Name:="Keyword"), ClientOnly>
    Property Keyword As String = ""

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property Production As String

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProductionFind) = New List(Of ROProductionFind)

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="ProductionTypeID"), ClientOnly>
    Public Property ProductionTypeID As Integer?

  End Class

#End Region

#Region "Travel Advances By Person"

  Public Class TravelAdvancesByPerson
    Inherits ReportBase(Of TravelAdvancesByPersonCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Travel Advances By Person"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptTravelAdvanceByPerson)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptTravelAdvanceByPerson"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TravelAdvancesByPersonCriteriaControl)
      End Get
    End Property

  End Class

  Public Class TravelAdvancesByPersonCriteria
    Inherits DefaultCriteria

    <Display(Name:="Keyword"), ClientOnly>
    Property Keyword As String = ""

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property Production As String

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProductionFind) = New List(Of ROProductionFind)

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="ProductionTypeID"), ClientOnly>
    Public Property ProductionTypeID As Integer?

  End Class

  Public Class TravelAdvancesByPersonCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TravelAdvancesByPersonCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Productions")
                  .Attributes("id") = "Productions"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        .Attributes("id") = "HRDiv"
                        With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "TravelAdvancesReport.AddProduction($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                              .Button.AddClass("btn-block buttontext")
                              .Button.AddClass("add-text-align-left")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region

End Class
