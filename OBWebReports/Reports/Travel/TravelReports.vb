﻿Imports Singular.Reporting
Imports Singular
Imports OBLib
Imports OBWebReports.Travel.New
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports Singular.DataAnnotations
Imports System.Text.RegularExpressions

Public Class TravelReports

#Region " Travel Req - Production Services "

  Public Class TravelRecProductionServices
    Inherits ReportBase(Of TravelRecProductionServicesCriteria)

    Public Property TravelRequisitionID As Integer? = Nothing

    Public Property ProductionID As Integer? = Nothing

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Services - Travel Req Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptTravelRecNew)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptTravelRec"
      cmd.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUser.UserID)
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.ZeroNothingDBNull(Me.TravelRequisitionID))
      Dim pt As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(pt)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)

      Dim data = ROTravelRecNew.GetROTravelRecNew(Me.TravelRequisitionID, Me.ReportCriteria.ProductionID)

      Dim list = Maintenance.TravelRecRptList.NewTravelRecRptList(0)
      data.ROTravelHRIDNewList.ToList.ForEach(Sub(s)
                                                Dim obj = Maintenance.TravelRecRpt.NewTravelRecRpt(s.HumanResourceID, 0, s.HumanResource)
                                                list.Add(obj)
                                                With obj
                                                  .CrewTypeID = s.CrewTypeID
                                                  .CrewType = s.CrewType
                                                  .IsEventManager = s.IsEventManager
                                                End With
                                              End Sub)

      data.ROTravelPreNewList.ToList.ForEach(Sub(s)
                                               Dim obj = list.GetBlankPreTravel(s.HumanResourceID)
                                               If obj Is Nothing Then
                                                 obj = Maintenance.TravelRecRpt.NewTravelRecRpt(s.HumanResourceID, 0, s.HumanResource)
                                                 list.Add(obj)
                                               End If

                                               With obj
                                                 .HasPreTravel = True
                                                 '.HumanResourceIDPre = s.HumanResourceID
                                                 .PreTravelDepartureTime = s.PreTravelDepartureTime
                                                 .PreTravelArrivalTime = s.PreTravelArrivalTime
                                                 .PreTravelStartDateTime = s.TravelStartDateTime
                                                 .PreTravelEndDateTime = s.TravelEndDateTime
                                                 .ProductionCrewTravelIDPre = s.ProductionCrewTravelID
                                                 .VersionNoPre = s.VersionNo
                                                 '.TicketPricePre = s.TicketPrice
                                                 '.ServiceFeePre = s.ServiceFee
                                                 .RefNoPre = s.RefNo
                                                 .CancelledDateTimePre = s.CancelledDateTime
                                                 .LatestPreIndPre = s.LatestPreInd
                                                 .PreTravelAirport = s.PreTravelAirport
                                                 .ServiceFee += s.ServiceFee
                                                 .InvoiceAmount += s.InvoiceAmount
                                                 .SortOrder = s.SortOrder
                                               End With
                                             End Sub)

      data.ROTravelPostNewList.ToList.ForEach(Sub(s)
                                                Dim obj = list.GetBlankPostTravel(s.HumanResourceID, s.CancelledDateTime.IsEmpty, s.VersionNo)
                                                If obj Is Nothing Then
                                                  obj = Maintenance.TravelRecRpt.NewTravelRecRpt(s.HumanResourceID, 0, s.HumanResource)
                                                  list.Add(obj)
                                                End If

                                                With obj
                                                  .HasPostTravel = True
                                                  '.HumanResourceIDPost = s.HumanResourceID
                                                  .PostTravelDepartureTime = s.PostTravelDepartureTime
                                                  .PostTravelArrivalTime = s.ArrivalTime
                                                  .PostTravelStartDateTime = s.TravelStartDateTime
                                                  .PostTravelEndDateTime = s.TravelEndDateTime
                                                  .ProductionCrewTravelIDPost = s.ProductionCrewTravelID
                                                  .VersionNoPost = s.VersionNo
                                                  '.TicketPricePost = s.TicketPrice
                                                  '.ServiceFeePost = s.ServiceFee
                                                  .RefNoPost = s.RefNo
                                                  .CancelledDateTimePost = s.CancelledDateTime
                                                  .LatestPostIndPost = s.LatestPostInd
                                                  .PostTravelAirport = s.PostTravelAirport
                                                  .ServiceFee += s.ServiceFee
                                                  .InvoiceAmount += s.InvoiceAmount
                                                  .SortOrder = s.SortOrder
                                                End With
                                              End Sub)

      data.ROTravelAccNewList.ToList.ForEach(Sub(s)
                                               Dim obj = list.GetBlankAccommodation(s.HumanResourceID) 'No Accommodation booked
                                               If obj Is Nothing Then
                                                 obj = Maintenance.TravelRecRpt.NewTravelRecRpt(s.HumanResourceID, s.SupplierHumanResourceID, s.HumanResource) 'Accommodation booked
                                                 list.Add(obj)
                                               End If

                                               With obj
                                                 .HasAccommodation = True
                                                 .ProductionID = s.ProductionID
                                                 '.HumanResource = s.HumanResource
                                                 .AccommodationStartDate = s.AccommodationStartDate
                                                 .AccommodationEndDate = s.AccommodationEndDate
                                                 .AccommodationProvider = s.AccommodationProvider
                                                 .SingleDouble = s.SingleDouble
                                                 .BandB = s.BandB
                                                 .BOnly = s.BOnly
                                                 .InvAmount = s.InvAmount
                                                 .InvNumber = s.InvNumber
                                                 .DatePaid = s.DatePaid
                                                 '.HumanResourceIDAcc = s.HumanResourceID
                                                 .SupplierHumanResourceID = s.SupplierHumanResourceID
                                                 .AccommodationCancelledDateTime = s.AccommodationCancelledDateTime
                                               End With

                                             End Sub)

      'data.ROTravelCommentatorSnTList.ToList.ForEach(Sub(s)
      '                                                 Dim obj = Maintenance.TravelRecRpt.NewTravelRecRpt(s.HumanResourceID, 0, "")
      '                                                 list.Add(obj)


      '                                               End Sub)

      'Dim ds As New DataSet(DataSet.DataSetName)

      'If DataSet.Tables("Information") IsNot Nothing Then ds.Tables.Add(DataSet.Tables("Information").Copy)

      For i As Integer = DataSet.Tables.Count - 1 To 0 Step -1
        Dim dt As DataTable = DataSet.Tables(i)
        'Dim newDT As DataTable = dt.Copy
        Select Case dt.TableName
          Case "Table"
            'ds.Tables.Add(newDT)
          Case "Table1"
            'ds.Tables.Add(list.GetDatasetForReport)
            dt.TableName = "Table2"
            'ds.Tables.Add(newDT)
          Case "Table2"
            dt.TableName = "Table3"
            'ds.Tables.Add(newDT)
          Case "Table3"
            dt.TableName = "Table4"
            'ds.Tables.Add(newDT)
          Case "Table4"
            dt.TableName = "Table5"
        End Select
      Next
      DataSet.Tables("Information").Columns.Add("ProductionID_text", GetType(System.String))
      DataSet.Tables.Add(list.GetDatasetForReport)

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TravRecPSCriteriaControl)
      End Get
    End Property

  End Class

  Public Class TravelRecProductionServicesCriteria
    Inherits DefaultCriteria

    '<Display(AutoGenerateField:=False), Browsable(True)>
    'Public Property TravelRequisitionID As Integer? = Nothing

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=1)>
    Public Property Production As String = ""

    '<System.ComponentModel.DisplayName("User"), Singular.DataAnnotations.DropDownWeb(GetType(CSLAlib.Users.Settings.CurrentUser.UserID), ValueMember:="UserID", DisplayMember:="UserName", UnselectedText:="All Users")>
    'Public Property UserID As Object = Singular.Misc.NothingDBNull(Nothing)

    ''<System.ComponentModel.DisplayName("System"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Security.OBIdentity), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
    'Public Property SystemID As Object = 1

  End Class

  Public Class TravRecPSCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TravelRecProductionServicesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TravelRecProductionServicesCriteria) d.Production, "TravelRecProductionServicesReport.FindProduction($element)",
                                                         "Search for Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Travel Req - Production Content "

  Public Class TravelRecProductionContent
    Inherits ReportBase(Of TravelRecProductionContentCriteria)

    Public Property CustomExportName As String
    Public Property TravelRequisitionID As Integer? = Nothing
    Public Property ProductionID As Integer? = Nothing

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Content Travel Req Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCommentatorTravelRec)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptCommentatorTravelRec"
      cmd.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUser.UserID)
      cmd.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.ZeroNothingDBNull(Me.TravelRequisitionID))
      Dim sd As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@StartDate").FirstOrDefault
      cmd.Parameters.Remove(sd)
      Dim ed As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EndDate").FirstOrDefault
      cmd.Parameters.Remove(ed)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides ReadOnly Property ExportFileName As String
      Get
        If CustomExportName <> "" Then
          Return CustomExportName
        End If
        Return Me.ReportName '"Travel Req " & Me.ReportCriteria.ProductionID
      End Get
    End Property

    Public Overrides Function GetDocumentFile(DocumentType As ReportDocumentType) As ReportFileInfo
      If Singular.Misc.ZeroNothing(Me.ReportCriteria.ProductionID) IsNot Nothing OrElse Singular.Misc.ZeroNothing(Me.TravelRequisitionID) IsNot Nothing Then
        Return MyBase.GetDocumentFile(DocumentType)
      Else
        Dim Files As New List(Of ReportFileInfo)
        Dim Systems As String() = Me.ReportCriteria.SystemIDs.Select(Function(pd) pd.ToString).Distinct.ToArray
        Dim ProductionAreas As String() = Me.ReportCriteria.ProductionAreaIDs.Select(Function(pd) pd.ToString).Distinct.ToArray
        Dim SystemsXML As String = Singular.Data.XML.StringArrayToXML(Systems)
        Dim ProductionAreasXML As String = Singular.Data.XML.StringArrayToXML(ProductionAreas)
        Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdAllProductionsFromPeriod", _
                                  New String() {"StartDate",
                                                "EndDate",
                                                "SystemIDs",
                                                "ProductionAreaIDs"
                                                }, _
                                  New Object() {Me.ReportCriteria.StartDate,
                                                Me.ReportCriteria.EndDate,
                                                SystemsXML,
                                                ProductionAreasXML}
                                                )
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmdProc = cmdProc.Execute
        Dim ProductionIDsWithCrew As New List(Of Tuple(Of Integer, String, Boolean))
        For Each row As DataRow In cmdProc.Dataset.Tables(0).Rows
          ProductionIDsWithCrew.Add(New Tuple(Of Integer, String, Boolean)(row("ProductionID"), row("ProductionRefNo"), row("HasCrew")))
        Next
        For n = 0 To ProductionIDsWithCrew.Count - 1
          If ProductionIDsWithCrew(n).Item3 = True Then
            Dim rpt As New TravelRecProductionContent()
            rpt.CustomExportName = "Travel Req for " & ProductionIDsWithCrew(n).Item2
            rpt.ReportCriteria.ProductionID = ProductionIDsWithCrew(n).Item1
            Dim GetDoc As ReportFileInfo = rpt.GetDocumentFile(ReportDocumentType.PDF)
            GetDoc.FileName = Regex.Replace(GetDoc.FileName, "[(:/\;<>|?*)]", String.Empty)
            Files.Add(GetDoc)
          End If
        Next
        Dim RetInfo As New ReportFileInfo
        Dim FileBytes As Byte()() = Files.Select(Function(d) d.FileBytes).ToArray()
        Dim FileNames As String() = Files.Select(Function(d) d.FileName).ToArray()
        Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes, FileNames)
        Dim ms As New IO.MemoryStream(CompressedFiles)
        RetInfo.FileStream = ms
        RetInfo.FileName = "Travel Req's for " & Me.ReportCriteria.StartDate & " to " & Me.ReportCriteria.EndDate & ".zip"
        Return RetInfo
        'End If
      End If
      Return MyBase.GetDocumentFile(DocumentType)
    End Function

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TravelRecProductionCriteriaControl)
      End Get
    End Property

  End Class

  Public Class TravelRecProductionContentCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production")>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of OBLib.Productions.ReadOnly.ROProductionFind) = New List(Of OBLib.Productions.ReadOnly.ROProductionFind)

    <Display(Name:="Systems"), ClientOnly>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), ClientOnly>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="System"), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

    <Display(Name:="Production Area"), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

  End Class

  Public Class TravelRecProductionCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TravelRecProductionContentCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
              With .Helpers.Bootstrap.Row()
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ProductionContentTravelReqReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionContentTravelReqReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              With .Helpers.FieldSet("Productions")
                .Attributes("id") = "Productions"
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                        With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) d.ROProductionList)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionContentTravelReqReport.AddProduction($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                            .Button.AddClass("btn-block buttontext")
                            .Button.AddClass("add-text-align-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class

#End Region

#Region " Ad Hoc "

  Public Class AdHocTravelRec
    Inherits ReportBase(Of AdHocTravelRecCriteria)

    Public Property CustomExportName As String
    Public Property TravelRequisitionID As Integer? = Nothing
    Public Property SingleReport As Boolean = False

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Ad Hoc Travel Req"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptAdHocTravelReq)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptAdHocTravelRec"
      cmd.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUser.UserID)
      Dim sd As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@StartDate").FirstOrDefault
      cmd.Parameters.Remove(sd)
      Dim ed As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EndDate").FirstOrDefault
      cmd.Parameters.Remove(ed)
      Dim ps As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@SystemIDs").FirstOrDefault
      cmd.Parameters.Remove(ps)
      Dim pc As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionAreaIDs").FirstOrDefault
      cmd.Parameters.Remove(pc)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(AdHocTravelCriteriaControl)
      End Get
    End Property

    Protected Overrides ReadOnly Property ExportFileName As String
      Get
        If CustomExportName <> "" Then
          Return CustomExportName
        End If
        Return Me.ReportName
      End Get
    End Property

    Public Overrides Function GetDocumentFile(DocumentType As ReportDocumentType) As ReportFileInfo
      If Me.SingleReport OrElse Singular.Misc.ZeroNothing(Me.ReportCriteria.AdHocBookingID) IsNot Nothing OrElse Singular.Misc.ZeroNothing(Me.ReportCriteria.TravelRequisitionID) IsNot Nothing Then
        Return MyBase.GetDocumentFile(DocumentType)
      Else
        Dim Files As New List(Of ReportFileInfo)
        Dim Systems As String = OBLib.OBMisc.IntegerListToXML(Me.ReportCriteria.SystemIDs)
        Dim ProductionAreas As String = OBLib.OBMisc.IntegerListToXML(Me.ReportCriteria.ProductionAreaIDs)
        'Dim SystemsXML As String = Singular.Data.XML.StringArrayToXML(Systems)
        'Dim ProductionAreasXML As String = Singular.Data.XML.StringArrayToXML(ProductionAreas)
        Dim cmdProc As New Singular.CommandProc("CmdProcs.cmdAllAdHocBookingsFromPeriod", _
                                  New String() {"StartDate", "EndDate",
                                                "SystemIDs", "ProductionAreaIDs"
                                                }, _
                                  New Object() {Me.ReportCriteria.StartDate,  Me.ReportCriteria.EndDate,
                                                Systems, ProductionAreas})
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmdProc = cmdProc.Execute
        Dim AdHocBookingIDsWithCrew As New List(Of Tuple(Of Integer, String, Boolean, String))
        For Each row As DataRow In cmdProc.Dataset.Tables(0).Rows
          AdHocBookingIDsWithCrew.Add(New Tuple(Of Integer, String, Boolean, String)(row("AdHocBookingID"), row("TravelRequisitionID"), row("HasCrew"), row("Title")))
        Next
        For n = 0 To AdHocBookingIDsWithCrew.Count - 1
          If AdHocBookingIDsWithCrew(n).Item3 = True Then
            Dim rpt As New AdHocTravelRec()
            rpt.CustomExportName = "Ad Hoc Travel Req for " & AdHocBookingIDsWithCrew(n).Item2 & " - " & AdHocBookingIDsWithCrew(n).Item4
            rpt.ReportCriteria.AdHocBookingID = AdHocBookingIDsWithCrew(n).Item1
            Dim GetDoc As ReportFileInfo = rpt.GetDocumentFile(ReportDocumentType.PDF)
            GetDoc.FileName = Regex.Replace(GetDoc.FileName, "[(:/\;<>|?*)]", String.Empty)
            Files.Add(GetDoc)
          End If
        Next
        Dim RetInfo As New ReportFileInfo
        Dim FileBytes As Byte()() = Files.Select(Function(d) d.FileBytes).ToArray()
        Dim FileNames As String() = Files.Select(Function(d) d.FileName).ToArray()
        Dim CompressedFiles As Byte() = Singular.Compression.CompressionUtility.CompressFiles(FileBytes, FileNames)
        Dim ms As New IO.MemoryStream(CompressedFiles)
        RetInfo.FileStream = ms
        RetInfo.FileName = "Ad Hoc Travel Req's for " & Me.ReportCriteria.StartDate & " to " & Me.ReportCriteria.EndDate & ".zip"
        Return RetInfo
        'End If
      End If
      Return MyBase.GetDocumentFile(DocumentType)
    End Function

  End Class

  Public Class AdHocTravelRecCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Ad Hoc Booking", Order:=1)>
    Public Property AdHocBookingID As Integer?

    <Display(Name:="Sub-Dept", Order:=3)>
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

    <Display(Name:="Production Area")>
    Public Property ProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    <Display(Name:="Systems", Order:=4), ClientOnly>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas", Order:=5), ClientOnly>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Ad Hoc Booking", Order:=2), ClientOnly>
    Public Property AdHocBooking As String

    <Display(Name:="Travel Requisition ID")>
    Public Property TravelRequisitionID As Integer?

  End Class

  Public Class AdHocTravelCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of AdHocTravelRecCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "AdHocTravel.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "AdHocTravel.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As AdHocTravelRecCriteria) d.AdHocBooking, "AdHocTravel.FindAdHocBooking($element, $data.StartDate(), $data.EndDate())",
                           "Search Ad Hoc Booking", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "AdHocTravel.ClearBooking()")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

  End Class


#End Region

#Region " HR Travel Details "

  Public Class HRTravelDetails
    Inherits ReportBase(Of HRTravelDetailCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "HR Travel Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHRTravelDetails"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hr)

    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).ChildRelations.Add("FlightHRID", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
                                                         New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

      DataSet.Tables(0).ChildRelations.Add("RCHRID", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
                                                     New DataColumn() {DataSet.Tables(2).Columns("HumanResourceID")})

      DataSet.Tables(0).ChildRelations.Add("AccHRID", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
                                                      New DataColumn() {DataSet.Tables(3).Columns("HumanResourceID")})


      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "RefNo"
                col.ExtendedProperties(Singular.Data.DataTables.ExtendedProperties.ExcludeFromTotal.ToString()) = True
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRTravelDetailCriteriaControl)
      End Get
    End Property

  End Class


  Public Class HRTravelDetailCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resource", Order:=3),
    Required(ErrorMessage:="Human Resource Required")>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=4)>
    Public Property HumanResources As String = ""

  End Class

  Public Class HRTravelDetailCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRTravelDetailCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRTravelDetailCriteria) d.HumanResources, "HRTravelDetailReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "HRTravelDetailReport.ClearHRs()")
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

  End Class

#End Region

#Region " Travel Req S&T Recon "

  Public Class TravelReqSnTRecon
    Inherits ReportBase(Of StartAndEndDateReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Travel Req S&T Recon"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptMontlyTravelSnTRecon"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).ChildRelations.Add("HRID", New DataColumn() {DataSet.Tables(0).Columns("ProductionID")}, _
                                                   New DataColumn() {DataSet.Tables(1).Columns("ProductionID")})

      DataSet.Tables(0).ChildRelations.Add("PRODID", New DataColumn() {DataSet.Tables(0).Columns("ProductionID")}, _
                                                     New DataColumn() {DataSet.Tables(2).Columns("ProductionID")})

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "ProductionID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

  End Class

#End Region

End Class
