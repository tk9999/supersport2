﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Imports Singular.Web
Imports Singular.DataAnnotations
Imports Infragistics.Documents.Excel
Imports OBLib.Maintenance.Resources.ReadOnly

Public Class FinanceReports

#Region " ISP Timesheet "

  <Serializable()>
  Public Class ISPTimesheet
    Inherits Singular.Reporting.ReportBase(Of ISPTimesheetCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ISP TimeSheet Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptISPTimesheet)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptISPTimesheet"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hr)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)
      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ISPTimesheetCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ISPTimesheetCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResources As String = ""

  End Class

  Public Class ISPTimesheetCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ISPTimesheetCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ISPTimesheetCriteria) d.HumanResources, "ISPTimesheetReport.FindHR($element)",
                                                       "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ISPTimesheetReport.ClearHRs()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

  '#Region "ISPTimeSheetsReport"

  '  Public Class ISPTimeSheetsReport
  '    Inherits ReportBase(Of ISPTimesheetCriteria)

  '    Public Overrides ReadOnly Property ReportName As String
  '      Get
  '        Return "ISP TimeSheets Report"
  '      End Get
  '    End Property

  '    Public Overrides ReadOnly Property CrystalReportType As System.Type
  '      Get
  '        Return GetType(rptISPTimesheet)
  '      End Get
  '    End Property

  '    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

  '      cmd.CommandText = "[RptProcs].[rptISPTimesheet]"
  '      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
  '      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
  '      For Each param In cmd.Parameters.Where(Function(c) c.Value.GetType() Is GetType(List(Of Integer)))
  '        param.Value = OBLib.Helpers.MiscHelper.IntegerArrayToXML(param.Value.ToArray())
  '      Next

  '    End Sub

  '  End Class

  '  Public Class ISPTimeSheetsReportCriteria
  '    Inherits StartAndEndDateReportCriteria
  '    Public Property HumanResourceIDs As List(Of Integer) = New List(Of Integer) From {2432}
  '  End Class

  '#End Region

  'Public Class MonthlyCreditorInvoicesCriteria
  '  Inherits DefaultCriteria

  '  '<System.ComponentModel.DisplayName("Production"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
  '  Public Property MonthDate As DateTime?

  'End Class

#Region " Production Cost Report "

  <Serializable()>
  Public Class ProductionCostReport
    Inherits ReportBase(Of ProductionCostReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Costs"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionCosts)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptProductionCosts"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    End Sub

  End Class

  Public Class ProductionCostReportCriteria
    Inherits DefaultCriteria

    Public Property ProductionID As Object

  End Class

#End Region

#Region " Production S&T Report"

  <Serializable()>
  Public Class ProductionSnTReport
    Inherits ReportBase(Of ProductionSnTReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production S&T Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionSnTReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptProductionSnTReport"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
    End Sub

  End Class

  Public Class ProductionSnTReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Description:="", Order:=1), Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer? = Nothing

    <Display(Name:="Production", Description:="", Order:=1), Required(ErrorMessage:="Production Required")>
    Public Property Production As String = ""

  End Class

  Public Class ProductionSnTReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionSnTReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                .Helpers.LabelFor(Function(d As ProductionSnTReportCriteria) d.ProductionID)
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionSnTReportCriteria) d.Production, "ProductionPAFormReport.FindProduction($element)",
                                                         "Search Productions", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

  End Class

#End Region

#Region "ISP Shift Report"

  Public Class ISPShiftReport
    Inherits ReportBase(Of ISPShiftReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ISP Shift Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptISPShiftReport"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ISPShiftCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ISPShiftReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

  End Class

  Public Class ISPShiftCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ISPShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Outsource Service Suppliers"

  Public Class OutsourceServiceSuppliers
    Inherits ReportBase(Of OutsourceServiceSuppliesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Outsource Service Suppliers"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptOutsourceServiceSuppliers"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(OutsourceServiceSuppliersCriteriaControl)
      End Get
    End Property

  End Class

  Public Class OutsourceServiceSuppliesCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Supplier", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSupplierList), UnselectedText:="Supplier")>
    Public Property SupplierID As Integer?

    <Display(Name:="Outsource Service Type", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROOutsourceServiceTypeList), UnselectedText:="Outsource Service Type")>
    Public Property OutsourceServiceTypeID As Integer?

  End Class

  Public Class OutsourceServiceSuppliersCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OutsourceServiceSuppliesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OutsourceServiceSuppliesCriteria) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Supplier"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OutsourceServiceSuppliesCriteria) d.OutsourceServiceTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Outsource Service Type"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Outsource Service Per Supplier Per Production"

  Public Class OutsourceServicePerSupplierPerProduction
    Inherits ReportBase(Of OutsourceServicePerSupplierPerProductionCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Outsource Service Per Supplier Per Production"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptOutsourceServicePerSupplierPerProduction)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptOutsourceServicePerSupplierPerProduction"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(Outsource_Service_Per_Supplier_Per_Production_Criteria_Control)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)
    End Sub

  End Class

  Public Class OutsourceServicePerSupplierPerProductionCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Supplier", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSupplierList), UnselectedText:="Supplier")>
    Public Property SupplierID As Integer?

    <Display(Name:="Outsource Service Type", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROOutsourceServiceTypeList), UnselectedText:="Outsource Service Type")>
    Public Property OutsourceServiceTypeID As Integer?

  End Class

  Public Class Outsource_Service_Per_Supplier_Per_Production_Criteria_Control
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OutsourceServicePerSupplierPerProductionCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OutsourceServicePerSupplierPerProductionCriteria) d.SupplierID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Supplier"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As OutsourceServicePerSupplierPerProductionCriteria) d.OutsourceServiceTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Outsource Service Type"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Petty Cash Report"

  Public Class PettyCashReport
    Inherits ReportBase(Of PettyCashReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Petty Cash Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptPettyCashReport"
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PettyCashCriteriaControl)
      End Get
    End Property

  End Class

  Public Class PettyCashReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=1)>
    Public Property Production As String = ""

  End Class

  Public Class PettyCashCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of PettyCashReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As PettyCashReportCriteria) d.Production, "PettyCashReport.FindProduction($element)",
                                                       "Search for Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Costs"

  Public Class ProductionCosts
    Inherits ReportBase(Of ProductionCostsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Costs"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptProductionCosts)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionCosts"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionCostsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionCostsCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=1)>
    Public Property Production As String = ""

  End Class

  Public Class ProductionCostsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionCostsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionCostsCriteria) d.Production, "ProductionCostsReport.FindProduction($element)",
                                                       "Search for Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Planning Hours"

  Public Class ProductionPlanningHours
    Inherits ReportBase(Of ProductionPlanningHoursCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Planning Hours"

      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionPlanningHours"
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionPlanningHoursCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionPlanningHoursCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Production", Order:=3)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=3)>
    Public Property Production As String = ""

  End Class

  Public Class ProductionPlanningHoursCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionPlanningHoursCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionPlanningHoursCriteria) d.Production, "ProductionPlanningHours.FindProduction($element)",
                                                       "Search for Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


#End Region

#Region "Public Holiday Report"

  Public Class PublicHolidayReport
    Inherits ReportBase(Of PublicHolidayReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Public Holiday Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptPublicHolidays"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PublicHolidayReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class PublicHolidayReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Contract Type", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?

  End Class

  Public Class PublicHolidayReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of PublicHolidayReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As PublicHolidayReportCriteria) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Contract Type"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Monthly Subsistence and Travel Allowance Report"

  Public Class MonthlySubsistenceandTravelAllowanceReport
    Inherits ReportBase(Of Monthly_Subsistence_And_Travel_Allowance_Report_Criteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Monthly Subsistence and Travel Allowance Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptMonthlySnTReport"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(h)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add(New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(Monthly_Subsistence_and_Travel_Allowance_Criteria_Control)
      End Get
    End Property

  End Class

  Public Class Monthly_Subsistence_And_Travel_Allowance_Report_Criteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResource As String = ""

    <Display(Name:="Contract Type", Order:=4),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.HR.ReadOnly.ROContractTypeList), UnselectedText:="Contract Type")>
    Public Property ContractTypeID As Integer?

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class Monthly_Subsistence_and_Travel_Allowance_Criteria_Control
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of Monthly_Subsistence_And_Travel_Allowance_Report_Criteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.HumanResource, "MonthlySubsistenceandTravelAllowanceReport.FindHR($element)",
                                                       "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.ContractTypeID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Contract Type"
                  End With
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As Monthly_Subsistence_And_Travel_Allowance_Report_Criteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Event Manager Planning Hours"

  Public Class EventManagerPlanningHours
    Inherits ReportBase(Of EventManagerPlanningHoursCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Event Manager Planning Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptEventManagerPlanningHours"
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(EventManagerPlanningHoursCriteriaControl)
      End Get
    End Property

  End Class

  Public Class EventManagerPlanningHoursCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Event Manager", Order:=3)>
    Public Property HumanResourceID As Integer?

    Public Property HumanResource As String



  End Class

  Public Class EventManagerPlanningHoursCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EventManagerPlanningHoursCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As EventManagerPlanningHoursCriteria) d.HumanResource, "EventManagerPlanningHoursReport.FindEventManager($element)",
                                                       "Search For Event Manager", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Crew Schedule Per Production Report"

  Public Class CrewSchedulePerProductionReport
    Inherits ReportBase(Of CrewSchedulePerProductionReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Crew Schedule Per Production Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptCrewProductionReport"
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CrewSchedulePerProductionReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class CrewSchedulePerProductionReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Production", Order:=3)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=3)>
    Public Property Production As String = ""

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class CrewSchedulePerProductionReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CrewSchedulePerProductionReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As CrewSchedulePerProductionReportCriteria) d.Production, "CrewSchedulePerProductionReport.FindProduction($element)",
                                                       "Search For Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As CrewSchedulePerProductionReportCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As CrewSchedulePerProductionReportCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As CrewSchedulePerProductionReportCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As CrewSchedulePerProductionReportCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Head Count"

  Public Class ProductionHeadCount
    Inherits ReportBase(Of ProductionHeadCountCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Head Count"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      cmd.CommandText = "RptProcs.rptProductionHeadCount"

    End Sub

  End Class

  Public Class ProductionHeadCountCriteria
    Inherits DefaultCriteria
  End Class

#End Region

#Region "Staff Utilisation"

  Public Class OvertimeReportByContractType
    Inherits ReportBase(Of StaffUtilisationCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Utilisation"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptStaffUtilisation"
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(OvertimeReportByContractTypeCriteriaControl)
      End Get
    End Property

  End Class

  Public Class StaffUtilisationCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Discipline", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList), UnselectedText:="Discipline")>
    Public Property DisciplineID As Integer?

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class OvertimeReportByContractTypeCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of StaffUtilisationCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As StaffUtilisationCriteria) d.DisciplineID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Discipline"
                  End With
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As StaffUtilisationCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As StaffUtilisationCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As StaffUtilisationCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As StaffUtilisationCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Temp Production Cost Report"

  Public Class TempProductionCostReport
    Inherits ReportBase(Of TempProductionCostReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Temp Production Cost Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionCostsTemp"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EventType").FirstOrDefault
      cmd.Parameters.Remove(e)
      cmd.CommandTimeout = 0
    End Sub
    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "ProductionID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TempProductionCostCriteriaControl)
      End Get
    End Property

  End Class

  Public Class TempProductionCostReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Production", Order:=3)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=3)>
    Public Property Production As String = ""

    <Display(Name:="Event Type", Order:=4)>
    Public Property EventTypeID As Integer?

    <Display(Name:="Event Type", Order:=4)>
    Public Property EventType As String = ""

    '<Display(Name:="Production Type", Order:=5)>
    'Public Property ProductionTypeID As Integer?

  End Class

  Public Class TempProductionCostCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TempProductionCostReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TempProductionCostReportCriteria) d.Production, "TempProductionCostReport.FindProduction($element)",
                                                       "Search For Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TempProductionCostReportCriteria) d.EventType, "TempProductionCostReport.FindEventType($element)",
                                                       "Search For Production Type (Event Type)", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TempProductionCostReportCriteria) d.EventTypeID, "TempProductionCostReport.FindProductionType($element)",
          '                                             "Search For Production Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "ISP Timesheet Data"

  Public Class ISPTimesheetData
    Inherits ReportBase(Of ISPTimesheetDataCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ISP Timesheet Data"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptISPTimesheetData"
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)

      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID", "CrewTimesheetID", "AdhocCrewTimesheetID", "TimesheetCategoryID", "OverridenByUserID",
                   "CreatedBy", "CreatedDateTime", "ModifiedBy", "ModifiedDateTime", "TimesheetMonthID", "CreditorInvoiceDetailID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ISPTimesheetDataCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ISPTimesheetDataCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="System", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="System")>
    Public Property SystemID As Integer?

  End Class

  Public Class ISPTimesheetDataCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ISPTimesheetDataCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ISPTimesheetDataCriteria) d.SystemID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "System"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Payment Run"

  Public Class PaymentRun
    Inherits ReportBase(Of PaymentRunCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Payment Run"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "GetProcs.getOBPaymentRunList"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(PaymentRunCriteriaControl)
      End Get
    End Property

  End Class

  Public Class PaymentRunCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

  End Class

  Public Class PaymentRunCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of PaymentRunCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Cost Report-Leon"

  Public Class ProductionCostReportLeon
    Inherits ReportBase(Of ProductionCostReportLeonCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Cost Report-Leon"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionCostsLeon"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EventType").FirstOrDefault
      cmd.Parameters.Remove(e)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "ProductionID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionCostReportLeonCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionCostReportLeonCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Production", Order:=3)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=3)>
    Public Property Production As String = ""

    <Display(Name:="Event Type", Order:=4)>
    Public Property EventTypeID As Integer?

    <Display(Name:="Event Type", Order:=4)>
    Public Property EventType As String = ""

  End Class

  Public Class ProductionCostReportLeonCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionCostReportLeonCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionCostReportLeonCriteria) d.Production, "ProductionCostReportLeon.FindProduction($element)",
                                                       "Search For Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionCostReportLeonCriteria) d.EventType, "ProductionCostReportLeon.FindEventType($element)",
                                                       "Search For Production Type (Event Type)", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          'With .Helpers.Bootstrap.Row
          '  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
          '    With .Helpers.Div
          '      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TempProductionCostReportCriteria) d.EventTypeID, "TempProductionCostReport.FindProductionType($element)",
          '                                             "Search For Production Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
          '      End With
          '    End With
          '  End With
          'End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "ISP TimesheetWith Rates"

  Public Class ISPTimesheetWithRates
    Inherits ReportBase(Of ISPTimesheetWithRatesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ISP Timesheet With Rates"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptFreelancerWithRates)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptFreelancerWithRates"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim e As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(e)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ISPTimesheetWithRatesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ISPTimesheetWithRatesCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resources", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resources", Order:=3)>
    Public Property HumanResources As String = ""

  End Class

  Public Class ISPTimesheetWithRatesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ISPTimesheetWithRatesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ISPTimesheetWithRatesCriteria) d.HumanResources, "ISPTimesheetWithRatesReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ISPTimesheetWithRatesReport.ClearHRs()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production HR Cost"

  Public Class ProductionHRCost
    Inherits ReportBase(Of ProductionHRCostCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production HR Cost"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionHRCost"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionHRCostCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionHRCostCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1),
    Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=1)>
    Public Property Production As String = ""

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class ProductionHRCostCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionHRCostCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionHRCostCriteria) d.Production, "ProductionHRCostReport.FindProduction($element)",
                                                       "Search For Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production HR Cost Details"

  Public Class ProductionHRCostDetails
    Inherits ReportBase(Of ProductionHRCostDetailsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production HR Cost Details"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionHRCostDetails"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionHRCostDetailsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionHRCostDetailsCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1),
    Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Order:=1),
   Required(ErrorMessage:="Production Required")>
    Public Property Production As String = ""

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class ProductionHRCostDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionHRCostDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionHRCostDetailsCriteria) d.Production, "ProductionHRCostDetailsReport.FindProduction($element)",
                                                       "Search For Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostDetailsCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostDetailsCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostDetailsCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As ProductionHRCostDetailsCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Human Resource S&T"

  Public Class ProductionContentHumanResourceSnT
    Inherits ReportBase(Of HumanResourceSnTCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource S&T"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptCommentatorSnTMonthly"
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      Dim adhoc As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@AdHocBooking").FirstOrDefault
      cmd.Parameters.Remove(adhoc)
      Dim studios As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@StudioInd").FirstOrDefault
      cmd.Parameters.Remove(studios)
      Dim ob As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@OutsideBroadcastInd").FirstOrDefault
      cmd.Parameters.Remove(ob)
      Dim cont As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionContentInd").FirstOrDefault
      cmd.Parameters.Remove(cont)
      Dim serv As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionServicesInd").FirstOrDefault
      cmd.Parameters.Remove(serv)
      Dim rfp As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForProductionsInd").FirstOrDefault
      cmd.Parameters.Remove(rfp)
      Dim rfah As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForAdHocInd").FirstOrDefault
      cmd.Parameters.Remove(rfah)

    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Or col.ToString = "CurrencyID" Or col.ToString = "Column1" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRSNTCriteriaControl)
      End Get
    End Property

  End Class

  Public Class HumanResourceSnTCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Group Policy", Order:=3),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Travel.SnTs.ReadOnly.ROGroupSnTList), DropDownColumns:={"DestinationCountry", "InternationalDescription"})>
    Public Property GroupSnTID As Integer? = Nothing

    <Display(Name:="Pay Half Month", Order:=4)>
    Public Property PayHalfMonthInd As Boolean

    <Display(Name:="Ad Hoc Booking", Order:=5)>
    Public Property AdHocBookingIDs As List(Of Integer)

    <Display(Name:="Production", Order:=6)>
    Public Property ProductionIDs As List(Of Integer)

    <Display(Name:="Ad Hoc Booking", Order:=7)>
    Public Property AdHocBooking As String

    <Display(Name:="Production", Order:=8)>
    Public Property Production As String

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

    Public Property ProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

    <ClientOnly>
    Public Property ReportForProductionsInd As Boolean = False

    <ClientOnly>
    Public Property ReportForAdHocInd As Boolean = False

  End Class

  Public Class HRSNTCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HumanResourceSnTCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.LabelFor(Function(c As HumanResourceSnTCriteria) c.GroupSnTID)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d As HumanResourceSnTCriteria) d.GroupSnTID, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Group S&T"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 3, 3, 3)
              With .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSnTCriteria) d.PayHalfMonthInd, "Pay Half Month", "Pay Half Month", "btn-success", "btn-danger", , , "btn-sm")
                .Button.Style.Width = "150px"
                .Button.AddClass("btn-block")
              End With
            End With
          End With
          With .Helpers.FieldSet("Report for Productions or Ad Hoc?")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                With .Helpers.Bootstrap.StateButton(Function(c As HumanResourceSnTCriteria) c.ReportForProductionsInd, "Productions", "Productions", "btn-success", , , , "btn-sm")
                  .Button.AddBinding(KnockoutBindingString.enable, Function(c As HumanResourceSnTCriteria) Not c.ReportForAdHocInd)
                  .Button.AddBinding(KnockoutBindingString.click, "HumanResourceSnTReport.ShowProductions($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                With .Helpers.Bootstrap.StateButton(Function(c As HumanResourceSnTCriteria) c.ReportForAdHocInd, "Ad Hoc", "Ad Hoc", "btn-success", , , , "btn-sm")
                  .Button.AddBinding(KnockoutBindingString.enable, Function(c As HumanResourceSnTCriteria) Not c.ReportForProductionsInd)
                  .Button.AddBinding(KnockoutBindingString.click, "HumanResourceSnTReport.ShowAdHoc($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Productions")
            .Attributes("id") = "ProductionsFieldSet"
            With .Helpers.If(Function(c As HumanResourceSnTCriteria) c.ReportForProductionsInd)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HumanResourceSnTCriteria) d.Production, "HumanResourceSnTReport.FindProduction($element, $data.StartDate(), $data.EndDate())",
                                             "Search Productions", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Ad Hoc Bookings")
            .Attributes("id") = "AdHocBookingsFieldSet"
            With .Helpers.If(Function(c As HumanResourceSnTCriteria) c.ReportForAdHocInd)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
                  With .Helpers.Div
                    With .Helpers.Bootstrap.ButtonGroup
                      If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                        .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSnTCriteria) d.ProductionServicesInd,
                                                       "Production Services", "Production Services", , , , "fa-hand-o-up", )
                      End If
                      If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                        .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSnTCriteria) d.ProductionContentInd,
                                                       "Production Content", "Production Content", , , , "fa-hand-o-up", )
                      End If
                      .AddBinding(KnockoutBindingString.click, "HumanResourceSnTReport.SetStatProdCriteria($data)")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  .Helpers.Bootstrap.LabelDisplay("Areas")
                  With .Helpers.Div
                    With .Helpers.Bootstrap.ButtonGroup
                      If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                        .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSnTCriteria) d.OutsideBroadcastInd,
                                                       "OB", "OB", , , , "fa-hand-o-up", )
                      End If
                      If Singular.Security.HasAccess("Production Area", "Studios") Then
                        .Helpers.Bootstrap.StateButton(Function(d As HumanResourceSnTCriteria) d.StudioInd,
                                                       "Studio", "Studio", , , , "fa-hand-o-up", )
                      End If
                      .AddBinding(KnockoutBindingString.click, "HumanResourceSnTReport.SetStatProdCriteria($data)")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HumanResourceSnTCriteria) d.AdHocBooking, "HumanResourceSnTReport.FindHRBooking($element, $data.StartDate(), $data.EndDate())",
                                             "Search Ad Hoc Booking", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "S&T Monthly Per Person Per Production"

  Public Class SnTMonthlyPerPersonPerProduction
    Inherits ReportBase(Of SnTMonthlyPerPersonPerProductionCriteria)

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "S&T Monthly Per Person Per Production"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptSnTMonthlyPerPersonPerProduction)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptSnTMonthlyPerPersonPerProduction"
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(h)
      Dim adhoc As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@AdHocBooking").FirstOrDefault
      cmd.Parameters.Remove(adhoc)
      Dim studios As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@StudioInd").FirstOrDefault
      cmd.Parameters.Remove(studios)
      Dim ob As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@OutsideBroadcastInd").FirstOrDefault
      cmd.Parameters.Remove(ob)
      Dim cont As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionContentInd").FirstOrDefault
      cmd.Parameters.Remove(cont)
      Dim serv As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionServicesInd").FirstOrDefault
      cmd.Parameters.Remove(serv)
      Dim rfp As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForProductionsInd").FirstOrDefault
      cmd.Parameters.Remove(rfp)
      Dim rfah As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForAdHocInd").FirstOrDefault
      cmd.Parameters.Remove(rfah)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).ChildRelations.Add("HumanResourceIDRelation", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
                                                                      New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SnTMonthlyPerPersonPerProductionCriteriaControl)
      End Get
    End Property

  End Class

  Public Class SnTMonthlyPerPersonPerProductionCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResources As String = ""

    <Display(Name:="Pay Half Month", Order:=4)>
    Public Property PayHalfMonthInd As Boolean

    <Display(Name:="Ad Hoc Booking", Order:=5)>
    Public Property AdHocBookingIDs As List(Of Integer)

    <Display(Name:="Production", Order:=6)>
    Public Property ProductionIDs As List(Of Integer)

    <Display(Name:="Production", Order:=7)>
    Public Property Production As String = ""

    <Display(Name:="AdHocBooking", Order:=8)>
    Public Property AdHocBooking As String = ""

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

    Public Property ProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

    <ClientOnly>
    Public Property ReportForProductionsInd As Boolean = False

    <ClientOnly>
    Public Property ReportForAdHocInd As Boolean = False

  End Class

  Public Class SnTMonthlyPerPersonPerProductionCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SnTMonthlyPerPersonPerProductionCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.HumanResources, "SnTMonthlyPerPersonPerProductionReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "SnTMonthlyPerPersonPerProductionReport.ClearHRs()")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 3, 3, 3)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.StateButton(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.PayHalfMonthInd, "Pay Half Month", "Pay Half Month", "btn-success", "btn-danger", , , "btn-sm")
                  .Button.Style.Width = "150px"
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Report for Productions or Ad Hoc?")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                With .Helpers.Bootstrap.StateButton(Function(c As SnTMonthlyPerPersonPerProductionCriteria) c.ReportForProductionsInd, "Productions", "Productions", "btn-success", , , , "btn-sm")
                  .Button.AddBinding(KnockoutBindingString.enable, Function(c As SnTMonthlyPerPersonPerProductionCriteria) Not c.ReportForAdHocInd)
                  .Button.AddBinding(KnockoutBindingString.click, "SnTMonthlyPerPersonPerProductionReport.ShowProductions($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 2, 2)
                With .Helpers.Bootstrap.StateButton(Function(c As SnTMonthlyPerPersonPerProductionCriteria) c.ReportForAdHocInd, "Ad Hoc", "Ad Hoc", "btn-success", , , , "btn-sm")
                  .Button.AddBinding(KnockoutBindingString.enable, Function(c As SnTMonthlyPerPersonPerProductionCriteria) Not c.ReportForProductionsInd)
                  .Button.AddBinding(KnockoutBindingString.click, "SnTMonthlyPerPersonPerProductionReport.ShowAdHoc($data)")
                  .Button.AddClass("btn-block")
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Productions")
            .Attributes("id") = "SNTProductionsFieldSet"
            With .Helpers.If(Function(c As SnTMonthlyPerPersonPerProductionCriteria) c.ReportForProductionsInd)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.Production, "SnTMonthlyPerPersonPerProductionReport.FindProduction($element, $data.StartDate(), $data.EndDate())",
                                             "Search Productions", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Ad Hoc Bookings")
            .Attributes("id") = "SNTAdHocBookingsFieldSet"
            With .Helpers.If(Function(c As SnTMonthlyPerPersonPerProductionCriteria) c.ReportForAdHocInd)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
                  With .Helpers.Div
                    With .Helpers.Bootstrap.ButtonGroup
                      If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                        .Helpers.Bootstrap.StateButton(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.ProductionServicesInd,
                                                       "Production Services", "Production Services", , , , "fa-hand-o-up", )
                      End If
                      If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                        .Helpers.Bootstrap.StateButton(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.ProductionContentInd,
                                                       "Production Content", "Production Content", , , , "fa-hand-o-up", )
                      End If
                      .AddBinding(KnockoutBindingString.click, "SnTMonthlyPerPersonPerProductionReport.SetStatProdCriteria($data)")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  .Helpers.Bootstrap.LabelDisplay("Areas")
                  With .Helpers.Div
                    With .Helpers.Bootstrap.ButtonGroup
                      If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                        .Helpers.Bootstrap.StateButton(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.OutsideBroadcastInd,
                                                       "OB", "OB", , , , "fa-hand-o-up", )
                      End If
                      If Singular.Security.HasAccess("Production Area", "Studios") Then
                        .Helpers.Bootstrap.StateButton(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.StudioInd,
                                                       "Studio", "Studio", , , , "fa-hand-o-up", )
                      End If
                      .AddBinding(KnockoutBindingString.click, "SnTMonthlyPerPersonPerProductionReport.SetStatProdCriteria($data)")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.AdHocBooking, "SnTMonthlyPerPersonPerProductionReport.FindHRBooking($element, $data.StartDate(), $data.EndDate())",
                                             "Search Ad Hoc Booking", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  Public Class SnTMonthlyPerPersonPerProductionExportable
    Inherits ReportBase(Of SnTMonthlyPerPersonPerProductionCriteria)

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Private Const WorksheetName As String = "S&T Monthly Per Production"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "S&T Monthly Per Person Per Production - Exportable"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As Type
    '  Get
    '    Return GetType(rptSnTMonthlyPerPersonPerProduction)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptSnTMonthlyPerPersonPerProductionExportable"
      Dim p As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(p)
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(h)
      Dim adhoc As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@AdHocBooking").FirstOrDefault
      cmd.Parameters.Remove(adhoc)
      Dim studios As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@StudioInd").FirstOrDefault
      cmd.Parameters.Remove(studios)
      Dim ob As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@OutsideBroadcastInd").FirstOrDefault
      cmd.Parameters.Remove(ob)
      Dim cont As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionContentInd").FirstOrDefault
      cmd.Parameters.Remove(cont)
      Dim serv As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionServicesInd").FirstOrDefault
      cmd.Parameters.Remove(serv)
      Dim rfp As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForProductionsInd").FirstOrDefault
      cmd.Parameters.Remove(rfp)
      Dim rfah As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ReportForAdHocInd").FirstOrDefault
      cmd.Parameters.Remove(rfah)
    End Sub

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Human Resource", "Employee Code", "S&T Day", "Details", "S&T Amount", "Meal", "TxDates", "FistTxDateTime", "City", "Ref No"})
      Private mCurrentHeader As Integer = -1

      Public Sub AddHeader(HeaderName As String)
        mHeaderName.Add(HeaderName)
      End Sub

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns and rows
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 0, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      For n As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
        ws.Columns(n).CellFormat.WrapText = ExcelDefaultableBoolean.True
        If (n = 0) Then
          ws.Columns(n).Width = DefaultColumnWidth * 400
        ElseIf (n > 0 AndAlso n < Data.Tables.Item(0).Rows.Count) Then
          If (n = 2) Then
            ws.Columns(n).Width = DefaultColumnWidth * 400
          ElseIf (n = 3) Then
            ws.Columns(n).Width = DefaultColumnWidth * 600
          ElseIf (n = 5) Then
            ws.Columns(n).Width = DefaultColumnWidth * 400
          ElseIf (n = 6) Then
            ws.Columns(n).Width = DefaultColumnWidth * 400
          ElseIf (n = 7) Then
            ws.Columns(n).Width = DefaultColumnWidth * 400
          ElseIf (n = 8) Then
            ws.Columns(n).Width = DefaultColumnWidth * 400
          Else
            ws.Columns(n).Width = DefaultColumnWidth * 200
          End If
        Else
          ws.Columns(n).Width = DefaultColumnWidth * 225
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count)
          ' Each Human resource Row
          If n = 0 AndAlso Row = 0 Then
            SetText(ws, Row, n, HeaderName.GetNextHeaderName, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf n = 0 AndAlso Row > 0 Then 'First Column
            SetText(ws, Row, n, Data.Tables.Item(0).Rows.Item(Row - 1)(0), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          ElseIf n > 0 AndAlso Row = 0 AndAlso n < HeaderName.GetCount Then 'Header Row
            SetText(ws, 0, n, HeaderName.GetNextHeaderName, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          Else 'Column > 0 And Row > 0
          End If
        Next
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 0, System.Drawing.Color.LightGray, FillPatternStyle.Solid)
      FormatCells(ws, 0, 0, 0, Data.Tables.Item(0).Rows.Count, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      ' Cell Population
      Dim p = 0
      Dim col = 1
      For Row As Integer = 1 To (Data.Tables.Item(0).Rows.Count)
        Dim EmployeeCode = Data.Tables.Item(0).Rows.Item(p)(col)
        SetText(ws, Row, col, EmployeeCode, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        col += 1
        Dim SnTDay = Data.Tables.Item(0).Rows.Item(p)(col)
        SetText(ws, Row, col, SnTDay, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        col += 1
        Dim Details = Data.Tables.Item(0).Rows.Item(p)(col)
        SetText(ws, Row, col, Details, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        col += 1
        Dim SnTAmount = Math.Round(Data.Tables.Item(0).Rows.Item(p)(col), 2)
        SetText(ws, Row, col, SnTAmount, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        col += 1
        Dim Meal = Data.Tables.Item(0).Rows.Item(p)(col)
        SetText(ws, Row, col, Meal, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        col += 1
        Dim TxDates = Data.Tables.Item(0).Rows.Item(p)(col)
        If TxDates.ToString.Trim <> "" Then
          SetText(ws, Row, col, TxDates, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        col += 1
        Dim FistTxDateTime = Data.Tables.Item(0).Rows.Item(p)(col)
        If Not IsDBNull(FistTxDateTime) Then
          SetText(ws, Row, col, FistTxDateTime, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        col += 1
        Dim City = Data.Tables.Item(0).Rows.Item(p)(col)
        If Not IsDBNull(City) Then
          SetText(ws, Row, col, City, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        col += 1
        Dim AHBID = Data.Tables.Item(0).Rows.Item(p)(col)
        If IsDBNull(AHBID) Then
          col += 1
        Else
          SetText(ws, Row, col, AHBID, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          col += 1
        End If
        Dim PID = Data.Tables.Item(0).Rows.Item(p)(col)
        If IsDBNull(PID) Then
          col += 1
        Else
          SetText(ws, Row, col - 1, PID, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          col += 1
        End If
        p += 1
        col = 1
      Next

      'Return MyBase.CreateExcelWorkbook(Data)
      Return wb
    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

    'Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
    '  MyBase.ModifyDataSet(DataSet)

    '  DataSet.Tables(0).ChildRelations.Add("HumanResourceIDRelation", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
    '                                                                  New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

    'End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SnTMonthlyPerPersonPerProductionCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region "Additional Hours Report (Production Services)"

  Public Class AdditionalHoursReportProductionServices
    Inherits ReportBase(Of AdditionalHoursReportProductionServicesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Additional Hours Report (Production Services)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[rptProcs].[rptAdditionalHoursReportProductionServices]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(AdditionalHoursReportProductionServicesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class AdditionalHoursReportProductionServicesCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

  End Class

  Public Class AdditionalHoursReportProductionServicesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of AdditionalHoursReportProductionServicesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3, 2)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

  '#Region "Additional Hours Report (Production Services)"

  '  Public Class AdditionalHoursReportProductionServices
  '    Inherits ReportBase(Of AdditionalHoursReportProductionServicesCriteria)

  '    Public Overrides ReadOnly Property ReportName As String
  '      Get
  '        Return "Additional Hours Report (Production Services)"
  '      End Get
  '    End Property

  '    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
  '      cmd.CommandText = "[rptProcs].[rptAdditionalHoursReportProductionServices]"
  '    End Sub

  '    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
  '      Get
  '        Return GetType(AdditionalHoursReportProductionServicesCriteriaControl)
  '      End Get
  '    End Property

  '  End Class

  '  Public Class AdditionalHoursReportProductionServicesCriteria
  '    Inherits DefaultCriteria

  '    <Display(Name:="Start Date", Order:=1),
  '    Required, DateField(MaxDateProperty:="EndDate")>
  '    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

  '    <Display(Name:="End Date", Order:=2),
  '    Required, DateField(MinDateProperty:="StartDate")>
  '    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

  '  End Class

  '  Public Class AdditionalHoursReportProductionServicesCriteriaControl
  '    Inherits Singular.Web.Reporting.CriteriaControlBase

  '    Protected Overrides Sub Setup()
  '      MyBase.Setup()

  '      With Helpers.With(Of AdditionalHoursReportProductionServicesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
  '        With .Helpers.FieldSet("Date Selection")
  '          With .Helpers.Bootstrap.Row
  '            With .Helpers.Bootstrap.Column(12, 12, 3, 3, 2)
  '              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
  '                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
  '                End With
  '              End With
  '            End With
  '            With .Helpers.Bootstrap.Row
  '              With .Helpers.Bootstrap.Column(12, 12, 3, 3, 2)
  '                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
  '                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
  '                  End With
  '                End With
  '              End With
  '            End With
  '          End With
  '        End With
  '      End With
  '    End Sub
  '  End Class

  '#End Region

  'Public Class ProductionSnTReport
  '  Inherits OBReports

  '  Private mProductionID As Integer = 0
  '  Private mIgnoreCriteria As Boolean = False
  '  Private ucbProduction As Infragistics.Win.UltraWinGrid.UltraCombo
  '  Private uneProduction As Infragistics.Win.UltraWinEditors.UltraNumericEditor
  '  Private ucbEventManager As Infragistics.Win.UltraWinGrid.UltraCombo
  '  Private txStart As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
  '  Private txEnd As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor

  '  Public Function AddCustomDateTime(ByVal Name As String, ByVal LabelText As String, Optional ByVal AllowNothing As Boolean = True, Optional ByVal DefaultToBlank As Boolean = False)
  '    Dim udt As New UltraDateTimeEditor
  '    udt.DisplayStyle = EmbeddableElementDisplayStyle.Office2007
  '    udt.FormatString = "dd MMM yyyy"
  '    If DefaultToBlank AndAlso AllowNothing Then
  '      udt.Value = DBNull.Value
  '    End If
  '    Me.ReportCriteria.AddCustomControl(Name, LabelText, udt, "Value", AllowNothing)
  '    Return udt
  '  End Function

  '  Public Sub New()

  '  End Sub

  '  Public Sub New(ProductionID As Integer)

  '    mProductionID = ProductionID
  '    mIgnoreCriteria = True

  '  End Sub

  '  Public Overrides ReadOnly Property Description() As String
  '    Get
  '      Return "Production Subsistence and Travel Allowance Report"
  '    End Get
  '  End Property

  '  Public Overrides ReadOnly Property Display() As String
  '    Get
  '      Return "Production Subsistence and Travel Allowance Report"
  '    End Get
  '  End Property

  '  Public Overrides Function SetupReport() As Boolean

  '    If mIgnoreCriteria Then
  '      Dim cmd As New SqlClient.SqlCommand
  '      cmd.CommandText = "RptProcs.rptProductionSnTReport"
  '      cmd.Parameters.AddWithValue("@ProductionID", mProductionID)
  '      cmd.CommandType = CommandType.StoredProcedure
  '      Me.ReportCriteria.ReportOutputChoice = Singular.Reports.ReportCriteria.OutputChoice.JustGrid
  '      Me.ReportCriteria.SetReportOutput(Singular.Reports.ReportCriteria.OutputOption.Grid)
  '      DataSource = cmd
  '      Return True
  '    Else
  '      If MustSetupReportCriteria Then

  '        With Me.ReportCriteria
  '          .DateSelectionRange = Singular.Reports.ReportCriteria.DateSelection.DateSelectionRange.NoDates
  '          ucbProduction = .AddCustomCombo("ProductionID", "Production", CommonData.Lists.ROProductionList, False).Control
  '          uneProduction = .AddCustomNumericEditor("ProductionNo").Control
  '          ucbEventManager = .AddCustomCombo("EventManagerID", "Event Manager", CommonData.Lists.EventManagerList, True).Control
  '          txStart = Me.AddCustomDateTime("TxStartDate", "Tx Start Date", True, True)
  '          txEnd = Me.AddCustomDateTime("TxEndDate", "Tx End Date", True, True)
  '          AddHandler ucbProduction.ValueChanged, AddressOf ValueChanged
  '          AddHandler uneProduction.ValueChanged, AddressOf ValueChanged
  '          AddHandler ucbEventManager.ValueChanged, AddressOf ValueChanged
  '          AddHandler txStart.ValueChanged, AddressOf ValueChanged
  '          AddHandler txEnd.ValueChanged, AddressOf ValueChanged
  '          .ReportOutputChoice = Singular.Reports.ReportCriteria.OutputChoice.JustGrid
  '          .SetReportOutput(Singular.Reports.ReportCriteria.OutputOption.Grid)
  '        End With

  '      End If

  '      If ReportCriteria.ShowDialog() = Windows.Forms.DialogResult.OK Then
  '        Dim cmd As New SqlClient.SqlCommand
  '        cmd.CommandText = "RptProcs.rptProductionSnTReport"
  '        cmd.Parameters.AddWithValue("@ProductionID", ReportCriteria.Data.CustomValue("ProductionID"))
  '        cmd.CommandType = CommandType.StoredProcedure
  '        'Report = New rptSnTReport
  '        DataSource = cmd

  '        Return True
  '      End If
  '    End If

  '  End Function

  '  Public Sub ValueChanged(sender As Object, e As System.EventArgs)

  '    RemoveHandler uneProduction.ValueChanged, AddressOf ValueChanged
  '    RemoveHandler ucbProduction.ValueChanged, AddressOf ValueChanged
  '    RemoveHandler ucbEventManager.ValueChanged, AddressOf ValueChanged
  '    RemoveHandler txStart.ValueChanged, AddressOf ValueChanged
  '    RemoveHandler txEnd.ValueChanged, AddressOf ValueChanged
  '    Dim list As OBLib.Productions.ReadOnly.ROProductionList = ucbProduction.DataSource

  '    Try
  '      If TypeOf sender Is Infragistics.Win.UltraWinGrid.UltraCombo And sender IsNot ucbEventManager Then
  '        If sender Is ucbProduction Then
  '          If ucbProduction.SelectedRow Is Nothing Then
  '            uneProduction.Value = DBNull.Value
  '          Else
  '            Dim roProd = DirectCast(ucbProduction.SelectedRow.ListObject, Productions.ReadOnly.ROProduction)
  '            uneProduction.Value = roProd.ProductionRefNo
  '          End If
  '        End If
  '      ElseIf TypeOf sender Is Infragistics.Win.UltraWinEditors.UltraNumericEditor Then
  '        Dim value As Integer = Singular.Misc.IsNull(uneProduction.Value, -1)
  '        If value = -1 Then
  '          ucbProduction.Value = DBNull.Value
  '        Else
  '          Dim roProd = list.GetItemByRefNo(value)
  '          If roProd IsNot Nothing Then
  '            ucbProduction.Value = roProd.ProductionID
  '            ucbProduction.Value = roProd.ProductionID
  '          ElseIf list.GetItem(value) IsNot Nothing Then
  '            ucbProduction.Value = uneProduction.Value
  '          Else
  '            ucbProduction.Value = DBNull.Value
  '          End If
  '        End If
  '      Else
  '        Dim id As Integer = 0
  '        If ucbEventManager.SelectedRow IsNot Nothing Then
  '          id = CType(ucbEventManager.SelectedRow.ListObject, HR.ReadOnly.ROHumanResource).HumanResourceID
  '        End If
  '        ucbProduction.DataSource = Productions.ReadOnly.ROProductionList.GetROProductionList(0, 0, 0, id, txStart.Value, txEnd.Value, 0, Nothing, Nothing, False)
  '      End If
  '    Finally
  '      AddHandler uneProduction.ValueChanged, AddressOf ValueChanged
  '      AddHandler ucbProduction.ValueChanged, AddressOf ValueChanged
  '      AddHandler ucbEventManager.ValueChanged, AddressOf ValueChanged
  '      AddHandler txStart.ValueChanged, AddressOf ValueChanged
  '      AddHandler txEnd.ValueChanged, AddressOf ValueChanged
  '    End Try

  '  End Sub

  'End Class

#Region " OB Production Production Head Count "

  Public Class OBProductionProductionHeadCount
    Inherits ReportBase(Of OBProductionProductionHeadCountCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Content - Production Head Count"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptOBProductionProductionHeadCount"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
    End Sub
  End Class

  Public Class OBProductionProductionHeadCountCriteria
    Inherits DefaultCriteria

  End Class

#End Region

#Region " Overtime Report "

  Public Class OvertimeReport
    Inherits ReportBase(Of OvertimeCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Additional Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptOvertime"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add("ParentIDRelation", DataSet.Tables(0).Columns("ParentID"), DataSet.Tables(1).Columns("ParentID"))
      DataSet.Relations.Add("DisciplineIDRelation", DataSet.Tables(1).Columns("DisciplineID"), DataSet.Tables(2).Columns("DisciplineID"))
      DataSet.Relations.Add("HumanResourceIDRelation", DataSet.Tables(2).Columns("HumanResourceID"), DataSet.Tables(3).Columns("HumanResourceID"))

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "ParentID", "DisciplineID", "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(OvertimeCriteriaControl)
      End Get
    End Property

  End Class

  Public Class OvertimeCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class OvertimeCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OvertimeCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As OvertimeCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As OvertimeCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As OvertimeCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As OvertimeCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Freelancer Overtime Report "

  Public Class FreelancerOvertimeReport
    Inherits ReportBase(Of FreelancerOvertimeReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Freelancer Additional Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptFreelancerOvertime"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add("ParentIDRelation", DataSet.Tables(0).Columns("ParentID"), DataSet.Tables(1).Columns("ParentID"))
      DataSet.Relations.Add("DisciplineIDRelation", DataSet.Tables(1).Columns("DisciplineID"), DataSet.Tables(2).Columns("DisciplineID"))

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FreelancerOvertimeCriteriaControl)
      End Get
    End Property

  End Class

  Public Class FreelancerOvertimeReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    Public Property ProductionServicesInd As Boolean = False
    Public Property ProductionContentInd As Boolean = False

    Public Property OutsideBroadcastInd As Boolean = False
    Public Property StudioInd As Boolean = False

  End Class

  Public Class FreelancerOvertimeCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FreelancerOvertimeReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          '************** THIS IS ONLY A TEMPORARY WAY OF SELECTION Sub-Depts and Areas ******************
          '***********************************************************************************************
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Sub-Depts")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Sub-Dept", "Production Services") Then
                    .Helpers.Bootstrap.StateButton(Function(d As FreelancerOvertimeReportCriteria) d.ProductionServicesInd,
                                                   "Production Services", "Production Services", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Sub-Dept", "Production Content") Then
                    .Helpers.Bootstrap.StateButton(Function(d As FreelancerOvertimeReportCriteria) d.ProductionContentInd,
                                                   "Production Content", "Production Content", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              .Helpers.Bootstrap.LabelDisplay("Areas")
              With .Helpers.Div
                With .Helpers.Bootstrap.ButtonGroup
                  If Singular.Security.HasAccess("Production Area", "Outside Broadcast") Then
                    .Helpers.Bootstrap.StateButton(Function(d As FreelancerOvertimeReportCriteria) d.OutsideBroadcastInd,
                                                   "OB", "OB", , , , "fa-hand-o-up", )
                  End If
                  If Singular.Security.HasAccess("Production Area", "Studios") Then
                    .Helpers.Bootstrap.StateButton(Function(d As FreelancerOvertimeReportCriteria) d.StudioInd,
                                                   "Studio", "Studio", , , , "fa-hand-o-up", )
                  End If
                End With
              End With
            End With
          End With
          '***********************************************************************************************
          '***********************************************************************************************
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Staff Hours Report "

  Public Class StaffHoursReport
    Inherits ReportBase(Of StartAndEndDateReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptStaffHoursReport"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).ChildRelations.Add("ParentHumanResourceID", New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, _
                                                                    New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

      DataSet.Tables(1).ChildRelations.Add("DisciplineIDRelation", New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID"), DataSet.Tables(1).Columns("DisciplineID")}, _
                                                                   New DataColumn() {DataSet.Tables(2).Columns("HumanResourceID"), DataSet.Tables(2).Columns("DisciplineID")})

    End Sub

  End Class

#End Region

#Region "Rate Update Report"
  Class RateUpdateReport
    Inherits ReportBase(Of RateUpdateReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Rate Update Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptRateUpdateReport]"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
      cmd.Parameters.Remove(hr)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(RateUpdateCriteriaControl)
      End Get
    End Property
  End Class

  Class RateUpdateReportCriteria
    Inherits DefaultCriteria

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResourceIDs As List(Of Integer)

    <System.ComponentModel.DisplayName("Human Resource")>
    Public Property HumanResources As String = ""

  End Class

  Public Class RateUpdateCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SnTMonthlyPerPersonPerProductionCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As SnTMonthlyPerPersonPerProductionCriteria) d.HumanResources, "RateUpdateReport.FindHR($element)",
                                                       "Search For Human Resources", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "RateUpdateReport.ClearHRs()")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " OB S&T Report"

  <Serializable()>
  Public Class OBSnTReport
    Inherits ReportBase(Of OBSnTReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "OB S&T Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(OBSnTReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptOBSnTReport"
      cmd.CommandTimeout = 0
    End Sub

  End Class

  Public Class OBSnTReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of OBLib.Productions.ReadOnly.ROProduction) = OBLib.CommonData.Lists.ROProductionList.ToList

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property Production As String = ""

    <Display(Name:="SystemID"), Required(ErrorMessage:="System is Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Read Only System List"), ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="ProductionAreaID")>
    Public Property ProductionAreaID As Integer?

    <Display(Name:="Read Only Production Area List"), ClientOnly>
    Public Property ROProductionAreaList As List(Of OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.ToList

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="Production Type", Order:=3), ClientOnly>
    Public Property ProductionTypeID As Integer?

  End Class

  Public Class OBSnTReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of OBSnTReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "OBSnTReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "OBSnTReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.FieldSet("Productions")
                  .Attributes("id") = "Productions"
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Productions.ReadOnly.ROProduction)(Function(d) d.ROProductionList)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "OBSnTReport.AddProduction($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                            .Button.AddClass("btn-block buttontext add-text-align-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

  End Class

#End Region

#Region " Day Away Report "

  <Serializable()>
  Public Class DayAwayReport
    Inherits ReportBase(Of DayAwayReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Day Away Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptDayAwayReport"
      'cmd.Parameters.AddWithValue("@StartDate", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@EndDate", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    End Sub

  End Class

  Public Class DayAwayReportCriteria
    Inherits StartAndEndDateReportCriteria

  End Class

  Public Class DayAwayReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of DayAwayReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Style.Width = "350px"
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Style.Width = "350px"
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Leon Report pt 2"

  Public Class LeonReportPt2
    Inherits ReportBase(Of LeonReportPt2Criteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Leon Report pt 2"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptLeonPaymentRunCheck"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(LeonReportPt2CriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "ProductionID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

      For Each col In DataSet.Tables(0).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

      For Each col In DataSet.Tables(1).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

      For Each col In DataSet.Tables(2).Columns
        If col.ToString = "HumanResourceID" Then
          col.ExtendedProperties.Add("AutoGenerate", False)
        End If
      Next

    End Sub

    Public Class LeonReportPt2Criteria
      Inherits StartAndEndDateReportCriteria

    End Class

    Public Class LeonReportPt2CriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of LeonReportPt2Criteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub

    End Class
  End Class
#End Region

#Region " Invoice Checks (Production Services) "

  Public Class InvoiceChecksProductionServices
    Inherits ReportBase(Of InvoiceChecksProductionServicesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Invoice Checks (Production Services)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      If cmd.Parameters("@PaymentRunID").Value Is Nothing Then
        cmd.Parameters("@PaymentRunID").Value = DBNull.Value
      End If
      cmd.CommandText = "RptProcs.rptInvoiceChecksProductionServices"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(InvoiceChecksProductionServicesCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).TableName = "All Data"
      DataSet.Tables(1).TableName = "Rate Warnings"
      DataSet.Tables(2).TableName = "Discipline Warnings"
      DataSet.Tables(3).TableName = "Sched with No Tsheet"
      DataSet.Tables(4).TableName = "Tsheets with No Sched"
      DataSet.Tables(5).TableName = "Time warnings Sched v Tsheet"
      DataSet.Tables(6).TableName = "Time warnings Tsheet vs Inv"
      DataSet.Tables(7).TableName = "Primary Skills"
      DataSet.Tables(8).TableName = "Primary Areas"
      DataSet.Tables(9).TableName = "Multiple Primary Disciplines"

    End Sub

    Public Class InvoiceChecksProductionServicesCriteria
      Inherits DefaultCriteria

      <Display(Name:="Payment Run"),
       DropDownWeb(GetType(OBLib.Invoicing.ReadOnly.ROPaymentRunSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                    BeforeFetchJS:="InvoiceChecksProductionServicesReport.setPaymentRunIDCriteriaBeforeRefresh",
                    PreFindJSFunction:="InvoiceChecksProductionServicesReport.triggerPaymentRunIDAutoPopulate",
                    AfterFetchJS:="InvoiceChecksProductionServicesReport.afterPaymentRunIDRefreshAjax",
                    LookupMember:="PaymentRun", ValueMember:="PaymentRunID", DropDownColumns:={"System", "MonthYearString", "Status", "TotalInvoices"},
                    DropDownCssClass:="room-dropdown")>
      Public Property PaymentRunID As Integer?
      <ClientOnly>
      Public Property PaymentRun As String = ""
      Public Property GetCurrentPaymentRun As Boolean = False

    End Class

    Public Class InvoiceChecksProductionServicesCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of InvoiceChecksProductionServicesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.PaymentRunID)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.PaymentRunID, Singular.Web.BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub

    End Class

  End Class

#End Region

#Region " Utilisation "

  Class UtilisationReport
    Inherits Singular.Reporting.ReportBase(Of UtilisationReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Utilisation"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(UtilisationReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptUtilisation]"
    End Sub

  End Class

  Class UtilisationReportCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date", Description:="")>
    Public Property StartDate As DateTime?

    <Display(Name:="End Date", Description:="")>
    Public Property EndDate As DateTime?

    <Display(Name:="Sub-Dept")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Area")>
    Public Property ProductionAreaIDs As List(Of Integer)

    <Display(Name:="Sub-Dept"), ClientOnly, Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID As Integer?

    <Display(Name:="Area"), ClientOnly, Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID As Integer?

  End Class

  Public Class UtilisationReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of UtilisationReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.FieldSet("Criteria")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 12, 3, 2)
                  'Systems and Areas
                  With .Helpers.FieldSet("Sub-Depts")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "UtilisationReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        .Style.MarginLeft("10px")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystemArea) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "UtilisationReport.AddArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                          .Button.AddBinding(KnockoutBindingString.enable, "$parent.IsSelected()")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 12, 9, 10)
                  With .Helpers.FieldSet("Other Criteria")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As UtilisationReportCriteria) d.StartDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As UtilisationReportCriteria) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small, , "Start Date")
                            .Editor.AddBinding(KnockoutBindingString.enable, "UtilisationReport.canEdit('StartDate', $data)")
                          End With
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
                        With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                          .Helpers.Bootstrap.LabelFor(Function(d As UtilisationReportCriteria) d.EndDate).Style.Width = "100%"
                          With .Helpers.Bootstrap.FormControlFor(Function(d As UtilisationReportCriteria) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small, , "End Date")
                            .Editor.AddBinding(KnockoutBindingString.enable, "UtilisationReport.canEdit('EndDate', $data)")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Generic Work Order Report"

  Class GenericWorkOrderReport
    Inherits Singular.Reporting.ReportBase(Of GenericWorkOrderReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Generic Work Order Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(GenericWorkOrderCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptGenericWorkOrder)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptGenericWorkOrder]"
    End Sub

  End Class

  Class GenericWorkOrderReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'XML Equipment list
    <Display(Name:="ROResourcesList"), ClientOnly>
    Public Property ROResourceList As List(Of ROResource) = New List(Of ROResource)

    <Display(Name:="ResourcesIDs")>
    Public Property ResourceIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="ResourceTypeIDs"), ClientOnly>
    Public Property ResourceTypeIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="ROResourceTypeList"), ClientOnly>
    Public Property ROResourceTypeList As OBLib.Maintenance.Resources.ReadOnly.ROResourceTypeList = OBLib.CommonData.Lists.ROResourceTypeList

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required"), ClientOnly>
    Public Property SystemID As Integer?

    <Display(Name:="Keyword"), SetExpression("GenericWorkOrderReport.KeywordSet(self)", , 250), TextField>
    Public Property Keyword As String

  End Class

  Public Class GenericWorkOrderCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of GenericWorkOrderReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.SystemID)
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "GenericWorkOrderReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelDisplay("Resource Select")
                With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Resources.ReadOnly.ROResourceType)("$data.ROResourceTypeList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Maintenance.Resources.ReadOnly.ROResourceType) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "GenericWorkOrderReport.AddResourceType($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Maintenance.Resources.ReadOnly.ROResourceType) c.ResourceType)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Resources")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
              With .Helpers.Bootstrap.Column(12, 12, 4, 4, 3)

                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, BootstrapEnums.InputSize.Small, , "Keyword")
                End With

              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 4, 3)
                With .Helpers.Bootstrap.Button(, "Refresh", Singular.Web.BootstrapEnums.Style.Primary, ,
                                                   Singular.Web.BootstrapEnums.ButtonSize.Medium, , "fa-refresh", ,
                                                   Singular.Web.PostBackType.None, "GenericWorkOrderReport.KeywordSet($data)")
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.ForEachTemplate(Of ROResource)(Function(d) d.ROResourceList)
              With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                  .Button.AddBinding(KnockoutBindingString.click, "GenericWorkOrderReport.AddResource($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ResourceName)
                  .Button.AddClass("btn-block buttontext")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

#Region " Travel Report "

  Public Class TravelReport
    Inherits Singular.Reporting.ReportBase(Of TravelReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Travel Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptTravelReport"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(TravelReportCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

    Public Class TravelReportCriteria
      Inherits Singular.Reporting.StartAndEndDateReportCriteria

    End Class

    Public Class TravelReportCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()

        With Helpers.With(Of TravelReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
              .Editor.Attributes("placeholder") = "Start Date"
            End With
            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
              .Editor.Attributes("placeholder") = "End Date"
            End With
          End With
        End With
      End Sub

    End Class

  End Class

#End Region

End Class
