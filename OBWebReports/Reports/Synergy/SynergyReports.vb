﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Singular.DataAnnotations

Public Class SynergyReports

#Region "Synergy Changes (Simple)"

  Class SynergyChangesSimple
    Inherits Singular.Reporting.ReportBase(Of SynergyChangesSimpleCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Synergy Changes (Simple)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SynergyChangesSimpleCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSynergyChangesSimple]"
    End Sub

  End Class

  Class SynergyChangesSimpleCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required")>
    Public Property StartDateTime As DateTime?

    <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required")>
    Public Property EndDateTime As DateTime?

    Public Property SynergyImportID As Integer?

  End Class

  Public Class SynergyChangesSimpleCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SynergyChangesSimpleCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Synergy Changes (With Booking Info)"

  Class SynergyChangesWithBookingInfo
    Inherits Singular.Reporting.ReportBase(Of SynergyChangesWithBookingInfoCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Synergy Changes (With Booking Info)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SynergyChangesWithBookingInfoCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSynergyChangesWithBookingInfo]"
      If cmd.Parameters(2).Value Is Nothing Then
        cmd.Parameters(2).Value = DBNull.Value
      End If
    End Sub

  End Class

  Class SynergyChangesWithBookingInfoCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required")>
    Public Property StartDateTime As DateTime?

    <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required")>
    Public Property EndDateTime As DateTime?

    <Display(Name:="Synergy Import")>
    Public Property SynergyImportID As Integer?

  End Class

  Public Class SynergyChangesWithBookingInfoCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SynergyChangesWithBookingInfoCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Synergy Production Schedules"

  Class SynergyProductionSchedules
    Inherits Singular.Reporting.ReportBase(Of SynergyProductionSchedulesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Synergy Production Schedules"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(SynergyProductionSchedulesCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSynergyProductionSchedules]"
      If cmd.Parameters(2).Value Is Nothing Then
        cmd.Parameters(2).Value = DBNull.Value
      End If
    End Sub

  End Class

  Class SynergyProductionSchedulesCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As DateTime? = Now

    <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As DateTime? = Now

    <Display(Name:="Genre")>
    Public Property Genre As String

    <Display(Name:="Series")>
    Public Property Series As String

    <Display(Name:="Title")>
    Public Property Title As String

    <Display(Name:="Keyword")>
    Public Property Keyword As String

  End Class

  Public Class SynergyProductionSchedulesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of SynergyProductionSchedulesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Other Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.Genre)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Genre, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Genre"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.Series)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Series, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Series"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.Title)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Title, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Title"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.Keyword)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Keyword"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

End Class
