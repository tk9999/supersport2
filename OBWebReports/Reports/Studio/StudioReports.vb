﻿Imports Singular.Reporting
Imports Singular
Imports OBLib
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly

Public Class StudioReports

#Region " Production Monthly Shifts Report "

  Public Class ProductionMonthlyShiftsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria



    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResource As List(Of String)

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = _
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) = _
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False

  End Class

  Public Class ProductionMonthlyShiftsReport
    Inherits Singular.Reporting.ReportBase(Of ProductionMonthlyShiftsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Monthly Shifts"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionMonthlyShifts)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionMonthlyShiftsReport"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(h)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohrl)
      Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhl)
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionMonthlyShiftsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionMonthlyShiftsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionMonthlyShiftsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "ProductionMonthlyShiftsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "ProductionMonthlyShiftsReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Human Resources")
                  .Attributes("id") = "HumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.EditorFor(Function(d) d.HumanResource)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Search Human Resource"

                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ProductionMonthlyShiftsReport.RefreshHR()")
                            .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
                          End With
                        End With
                      End With
                    End With

                    .Helpers.HTML.NewLine()
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.If(Function(c As ProductionMonthlyShiftsCriteria) Not c.BusyHRList)
                            With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                  .Button.AddBinding(KnockoutBindingString.click, "ProductionMonthlyShiftsReport.AddHumanResource($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.If(Function(c As ProductionMonthlyShiftsCriteria) c.BusyHRList)
                            With .Helpers.Div
                              With .Helpers.HTMLTag("i")
                                .AddClass("fa fa-cog fa-lg fa-spin")
                              End With
                              With .Helpers.HTMLTag("span")
                                .Helpers.HTML("Updating...")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  'Public Class ProductionMonthlyShiftsReportCriteria
  '  Inherits StartAndEndDateReportCriteria

  '  'Public Property StartDate As Date

  '  'Public Property EndDate As Date

  'End Class

#End Region

#Region " Production Services SMS Report "

  Public Class ProductionServicesStudioSMSReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    '<Display(Name:="Human Resource", Order:=3)>
    'Public Property HumanResourceIDs As List(Of Integer)

    '<Display(Name:="Human Resource", Order:=3)>
    'Public Property HumanResource As List(Of String)

    <Display(Name:="Sub-Dept", Order:=1), Required(ErrorMessage:="Sub-Dept Required"),
     DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID As Integer?
    'Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = _
    '  OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Area", Order:=2), Required(ErrorMessage:="Area Required"),
     DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID As Integer?
    'Public Property ROProductionAreaList As List(Of ROProductionArea) = _
    '  OBLib.CommonData.Lists.ROProductionAreaList.ToList

    'Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    '<ClientOnly>
    'Public Property BusyHRList As Boolean = False

    <Display(Name:="Room", Order:=3)>
    Public Property RoomID As Integer?

    <Display(Name:="Summarise By", Order:=4)>
    Public Property ByShift As Boolean = False

    '<Display(Name:="Room", Order:=2)>
    'Public Property Room As String = ""

    '<Display(Name:="Room", Order:=4)>
    'Public Property RoomID As Integer?

  End Class

  Public Class ProductionServicesStudioSMSCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionServicesStudioSMSReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Style.Width = "350px"
            .Style.TextAlign = TextAlign.left
            .Style.FloatLeft()
            .Style.MarginRight("10px")
            .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
              .Editor.Attributes("placeholder") = "Start Date"
            End With
          End With
          With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
            .Style.Width = "350px"
            .Style.TextAlign = TextAlign.left
            .Style.FloatLeft()
            .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
            With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
              .Editor.Attributes("placeholder") = "End Date"
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d) d.SystemID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d) d.SystemID, BootstrapEnums.InputSize.Small, , "Sub-Dept")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
              .Helpers.Bootstrap.LabelFor(Function(d) d.ProductionAreaID).Style.Width = "100%"
              With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionAreaID, BootstrapEnums.InputSize.Small, , "Area")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.ByShift).Style.Width = "100%"
                With .Helpers.Bootstrap.StateButton(Function(d) d.ByShift, "By Shift", "By Booking", , , , , )
                End With
              End With
            End With
          End With
        End With
        'With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
        '  'With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '  '  'With .Helpers.Bootstrap.Row
        '  '  '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '  '  '    With .Helpers.FieldSet("Sub-Depts")
        '  '  '      With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
        '  '  '        With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '  '  '          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '  '  '            .Button.AddBinding(KnockoutBindingString.click, "ProductionServicesStudioSMSReport.AddSystem($data)")
        '  '  '            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
        '  '  '            .Button.AddClass("btn-block buttontext")
        '  '  '          End With
        '  '  '        End With
        '  '  '      End With
        '  '  '    End With
        '  '  '    With .Helpers.Div
        '  '  '      With .Helpers.FieldSet("Production Areas")
        '  '  '        With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
        '  '  '          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '  '  '            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
        '  '  '              .Button.AddBinding(KnockoutBindingString.click, "ProductionServicesStudioSMSReport.AddProductionArea($data)")
        '  '  '              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
        '  '  '              .Button.AddClass("btn-block buttontext")
        '  '  '            End With
        '  '  '          End With
        '  '  '        End With
        '  '  '      End With
        '  '  '    End With

        '  '  '    With .Helpers.Bootstrap.Row
        '  '  '      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '  '  '        With .Helpers.Div
        '  '  '          With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionServicesStudioSMSReportCriteria) d.Room, "ProductionServicesStudioSMSReport.FindRoom($element)",
        '  '  '                                                 "Search For Room", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
        '  '  '          End With
        '  '  '        End With
        '  '  '      End With
        '  '  '    End With
        '  '  '  End With
        '  '  '  With .Helpers.Bootstrap.Column(12, 12, 6, 6)
        '  '  '    With .Helpers.FieldSet("Human Resources")
        '  '  '      .Attributes("id") = "HumanResources"
        '  '  '      .AddClass("FadeHide")
        '  '  '      With .Helpers.Div
        '  '  '        With .Helpers.Div
        '  '  '          With .Helpers.Bootstrap.Row
        '  '  '            With .Helpers.Bootstrap.Column(12, 8, 6, 6)
        '  '  '              With .Helpers.EditorFor(Function(d) d.HumanResource)
        '  '  '                .AddClass("form-control input-sm")
        '  '  '                .Attributes("placeholder") = "Search Human Resource"

        '  '  '              End With
        '  '  '            End With
        '  '  '            With .Helpers.Bootstrap.Column(12, 8, 6, 6)
        '  '  '              With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "ProductionServicesStudioSMSReport.RefreshHR()")
        '  '  '                .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
        '  '  '              End With
        '  '  '            End With
        '  '  '          End With
        '  '  '        End With
        '  '  '        .Helpers.HTML.NewLine()
        '  '  '        With .Helpers.Div
        '  '  '          .Attributes("id") = "HRDiv"
        '  '  '          With .Helpers.Bootstrap.Row
        '  '  '            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
        '  '  '              With .Helpers.If(Function(c As ProductionServicesStudioSMSReportCriteria) Not c.BusyHRList)
        '  '  '                With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
        '  '  '                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
        '  '  '                    With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
        '  '  '                      .Button.AddBinding(KnockoutBindingString.click, "ProductionServicesStudioSMSReport.AddHumanResource($data)")
        '  '  '                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
        '  '  '                      .Button.AddClass("btn-block buttontext")
        '  '  '                    End With
        '  '  '                  End With
        '  '  '                End With
        '  '  '              End With
        '  '  '              With .Helpers.If(Function(c As ProductionServicesStudioSMSReportCriteria) c.BusyHRList)
        '  '  '                With .Helpers.Div
        '  '  '                  With .Helpers.HTMLTag("i")
        '  '  '                    .AddClass("fa fa-cog fa-lg fa-spin")
        '  '  '                  End With
        '  '  '                  With .Helpers.HTMLTag("span")
        '  '  '                    .Helpers.HTML("Updating...")
        '  '  '                  End With
        '  '  '                End With
        '  '  '              End With
        '  '  '            End With
        '  '  '          End With
        '  '  '        End With
        '  '  '      End With
        '  '  '    End With
        '  '  '  End With
        '  '  'End With
        '  'End With
        'End With
      End With


    End Sub
  End Class

  Public Class ProductionServicesStudioSMSReport
    Inherits Singular.Reporting.ReportBase(Of ProductionServicesStudioSMSReportCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "SMS Report"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As System.Type
    '  Get
    '    Return GetType(rptMonthlyScheduleByIndividual)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptSMSReportProductionServicesStudio]"
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      'Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      'cmd.Parameters.Remove(h)
      'Dim r As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Room").FirstOrDefault
      'cmd.Parameters.Remove(r)
      'Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      'cmd.Parameters.Remove(rosl)
      'Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      'cmd.Parameters.Remove(ropal)
      'Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      'cmd.Parameters.Remove(rohrl)
      'Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      'cmd.Parameters.Remove(bhl)
      'cmd.Parameters.AddWithValue("@ProductionAreaID", )
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionServicesStudioSMSCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " HR Shift Payment "

  Public Class HRShiftPaymentCriteria
    Inherits Singular.Reporting.DefaultCriteria

    '<System.ComponentModel.DataAnnotations.Display(Name:="Team", Order:=1),
    'Singular.DataAnnotations.DropDownWeb(GetType(ROTeamList), UnselectedText:="Select Team")>
    'Public Property TeamID As Integer?

    '<System.ComponentModel.DataAnnotations.Display(Name:="Human Resource", Order:=2),
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceList), ValueMember:="HumanResourceID", DisplayMember:="PreferredFirstSurname", UnselectedText:="Select Human Resource", Name:="ROStudioHumanResourceList")>
    'Public Property HumanResourceID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResourceID As Integer?


    <System.ComponentModel.DataAnnotations.Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResource As String = ""

    <Required(ErrorMessage:="Start Date Required")>
    Public Property StartDate As Date?

    <Required(ErrorMessage:="End Date Required")>
    Public Property EndDate As Date?

  End Class

  Public Class HRShiftPaymentCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of HRShiftPaymentCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As HRShiftPaymentCriteria) d.HumanResource, "HRShiftPaymentReport.FindHR($element)",
                                                       "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  Public Class HRShiftPayment
    Inherits Singular.Reporting.ReportBase(Of HRShiftPaymentCriteria)

    'Public Property StartDate As Date?
    'Public Property EndDate As Date?
    'Public Property TeamID As Integer?
    'Public Property HumanResourceID As Integer?
    'Public Property SystemID As Integer?
    'Public Property ProductionAreaID As Integer?

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Human Resource Shift Hours"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceShiftPaymentStudio"
      'cmd.Parameters.AddWithValue("@StartDate", NothingDBNull(StartDate))
      'cmd.Parameters.AddWithValue("@EndDate", NothingDBNull(EndDate))
      'cmd.Parameters.AddWithValue("@TeamID", NothingDBNull(TeamID))
      'cmd.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(HumanResourceID))
      cmd.Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
      cmd.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(h)
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptHRShiftHoursStudio)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Relations.Add(New DataColumn() {DataSet.Tables(0).Columns("HumanResourceID")}, New DataColumn() {DataSet.Tables(1).Columns("HumanResourceID")})

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(HRShiftPaymentCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " HR Shift Payment Hours "

  Public Class FreelancerHRShiftPaymentCriteria
    Inherits Singular.Reporting.DefaultCriteria

    '<System.ComponentModel.DataAnnotations.Display(Name:="Team", Order:=1),
    'Singular.DataAnnotations.DropDownWeb(GetType(ROTeamList), UnselectedText:="Select Team")>
    'Public Property TeamID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResourceID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Human Resource", Order:=2)>
    Public Property HumanResource As String = ""

    <Required(ErrorMessage:="Start Date Required")>
    Public Property StartDate As Date?
    <Required(ErrorMessage:="End Date Required")>
    Public Property EndDate As Date?

  End Class


  Public Class FreelancerHRShiftPaymentCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FreelancerHRShiftPaymentCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As FreelancerHRShiftPaymentCriteria) d.HumanResource, "FreelancerHRShiftPaymentReport.FindHR($element)",
                                                       "Search For Human Resource", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  Public Class FreelancerHRShiftPayment
    Inherits Singular.Reporting.ReportBase(Of FreelancerHRShiftPaymentCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Freelancer Human Resource Shift Payment"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptFreelanceHRShiftHoursStudio)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceFreelanceShiftPaymentStudio"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(h)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FreelancerHRShiftPaymentCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region "Staff Shift Details"

  Public Class StaffShiftDetailsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResource As List(Of String)

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = _
      OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?
    Public Property ROProductionAreaList As List(Of ROProductionArea) = _
      OBLib.CommonData.Lists.ROProductionAreaList.ToList

    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <ClientOnly>
    Public Property BusyHRList As Boolean = False



  End Class

  Public Class StaffShiftDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of StaffShiftDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 11, 8)
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                .Style.TextAlign = TextAlign.center
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts")
                  With .Helpers.ForEachTemplate(Of OBLib.Maintenance.ReadOnly.ROSystem)(Function(d) d.ROSystemList)
                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                      With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "StaffShiftDetailsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.System)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.FieldSet("Production Areas")
                    With .Helpers.ForEachTemplate(Of ROProductionArea)(Function(d) d.ROProductionAreaList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "StaffShiftDetailsReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Human Resources")
                  .Attributes("id") = "HumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.EditorFor(Function(d) d.HumanResource)
                            .AddClass("form-control input-sm")
                            .Attributes("placeholder") = "Search Human Resource"

                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "StaffShiftDetailsReport.RefreshHR()")
                            .Button.AddBinding(KnockoutBindingString.visible = True, Function(c) c.ROHumanResourceList.Count >= 0)
                          End With
                        End With
                      End With
                    End With

                    .Helpers.HTML.NewLine()
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                          With .Helpers.If(Function(c As StaffShiftDetailsCriteria) Not c.BusyHRList)
                            With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                  .Button.AddBinding(KnockoutBindingString.click, "StaffShiftDetailsReport.AddHumanResource($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                          With .Helpers.If(Function(c As StaffShiftDetailsCriteria) c.BusyHRList)
                            With .Helpers.Div
                              With .Helpers.HTMLTag("i")
                                .AddClass("fa fa-cog fa-lg fa-spin")
                              End With
                              With .Helpers.HTMLTag("span")
                                .Helpers.HTML("Updating...")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class StaffShiftDetails
    Inherits Singular.Reporting.ReportBase(Of StaffShiftDetailsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Staff Shift Details Studio"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptHumanResourceStaffShiftDetailsStudio"
      cmd.Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
      cmd.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(h)
      Dim rosl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROSystemList").FirstOrDefault
      cmd.Parameters.Remove(rosl)
      Dim ropal As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROProductionAreaList").FirstOrDefault
      cmd.Parameters.Remove(ropal)
      Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
      cmd.Parameters.Remove(rohrl)
      Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
      cmd.Parameters.Remove(bhl)
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptStaffShiftDetailStudio)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StaffShiftDetailsCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Weekly Changes Report"

  Public Class WeeklyChangesReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    '<Display(Name:="Room Types")>
    'Public Property RoomTypeIDs As List(Of Integer)

  End Class

  Public Class WeeklyChangesReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of WeeklyChangesReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class WeeklyChangesReport
    Inherits Singular.Reporting.ReportBase(Of WeeklyChangesReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Weekly Changes"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptWeeklyChanges)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptWeeklyChangesReportProductionContent"
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(WeeklyChangesReportCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region " Production Facilities Bookings Report"

  Public Class ProductionFacilitiesBookingsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Rooms", Order:=7)>
    Public Property RoomIDs As List(Of Integer)

    <Display(Name:="Room Type List", Order:=8), ClientOnly>
    Public Property RORoomTypeReportList As List(Of RORoomTypeReport) = OBLib.CommonData.Lists.RORoomTypeReportList.ToList

  End Class

  Public Class ProductionFacilitiesBookingsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionFacilitiesBookingsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.FieldSet("Date Selection")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.FieldSet("Rooms")
              With .Helpers.Bootstrap.Row
                With .Helpers.ForEachTemplate(Of RORoomTypeReport)(Function(d) d.RORoomTypeReportList)
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "ProductionFacilitiesBookingsReport.RORoomTypeReportSelected($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As RORoomTypeReport) c.RoomType)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of RORoomTypeReportRoom)("$data.RORoomTypeReportRoomList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionFacilitiesBookingsReport.AddRoom($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As RORoomTypeReportRoom) c.Room)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With


    End Sub
  End Class

  Public Class ProductionFacilitiesBookings
    Inherits Singular.Reporting.ReportBase(Of ProductionFacilitiesBookingsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Facilities Bookings"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionFacilitiesBookings)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptProductionFacilitiesBookings"
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionFacilitiesBookingsCriteriaControl)
      End Get
    End Property

  End Class

#End Region

#Region "Studio Bookings - Shift Report"

  Public Class StudioBookingsShiftReport
    Inherits ReportBase(Of StudioBookingsShiftReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Studio Bookings - Shift Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptStudioBookingsShiftReport)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName = "Table" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
                'Case "HumanResourceShiftID"
                '  col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptStudioBookingsShiftReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StudioBookingsShiftReportControl)
      End Get
    End Property
  End Class

  Public Class StudioBookingsShiftReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

    '<Display(Name:="Only Rooms")>
    'Public Property OnlyRoomsInd As Boolean = False

    '<Display(Name:="Only Human Resources")>
    'Public Property OnlyHRInd As Boolean = False

  End Class

  Public Class StudioBookingsShiftReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of StudioBookingsShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "StudioBookingsShiftReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "StudioBookingsShiftReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With

                End With

              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Studio - Biometric Logs Report"

  Public Class BiometricLogsReport
    Inherits ReportBase(Of BiometricLogsReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Studio - Biometric Logs Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptBiometricLogs]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(BiometricLogsReportControl)
      End Get
    End Property
  End Class

  Public Class BiometricLogsReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer)

  End Class

  Public Class BiometricLogsReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of BiometricLogsReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "BiometricLogsReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "BiometricLogsReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With

                End With

              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Services Studio Data Checks Report"

  Public Class ServicesStudioDataChecksReport
    Inherits ReportBase(Of ServicesStudioDataChecksReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Data Checks (Services Studio)"
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName = "Table" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
                'Case "HumanResourceShiftID"
                '  col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Infragistics.Documents.Excel.Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Workbook.Worksheets(0).Name = "Hours Exceeded"
      Workbook.Worksheets(1).Name = "Unassigned TBC"
      Workbook.Worksheets(2).Name = "Criteria"

    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionServicesStudioDataChecksReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ServicesStudioDataChecksReportControl)
      End Get
    End Property
  End Class

  Public Class ServicesStudioDataChecksReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1), Required(ErrorMessage:="Start Date Required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2), Required(ErrorMessage:="End Date Required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required")>
    Public Property SystemIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Production Areas"), Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaIDs As List(Of Integer) = New List(Of Integer)

    '<Display(Name:="Only Rooms")>
    'Public Property OnlyRoomsInd As Boolean = False

    '<Display(Name:="Only Human Resources")>
    'Public Property OnlyHRInd As Boolean = False

  End Class

  Public Class ServicesStudioDataChecksReportControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()
      With Helpers.With(Of StudioBookingsShiftReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Sub-Depts and Areas")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                      With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "ServicesStudioDataChecksReport.AddSystem($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "ServicesStudioDataChecksReport.AddProductionArea($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With

                End With

              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Weekly Changes Report Maximo "

  Public Class WeeklyChangesReportMaximoCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    '<Display(Name:="Room Types")>
    'Public Property RoomTypeIDs As List(Of Integer)

  End Class

  Public Class WeeklyChangesReportMaximoCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of WeeklyChangesReportMaximoCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate).Style.Width = "100%"
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

  Public Class WeeklyChangesReportMaximo
    Inherits Singular.Reporting.ReportBase(Of WeeklyChangesReportMaximoCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Weekly Changes (Maximo)"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptWeeklyChanges)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptWeeklyChangesReportMaximo"
    End Sub

    Public Overrides ReadOnly Property AllowDataExport As Boolean
      Get
        Return False
      End Get
    End Property

    Public Overrides ReadOnly Property ShowWordExport As Boolean
      Get
        Return True
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(WeeklyChangesReportCriteriaControl)
      End Get
    End Property

  End Class

#End Region

End Class
