﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Maintenance
Imports OBLib.Timesheets.OBCity
Imports Singular.DataAnnotations
Imports Singular.Web
Imports Infragistics.Documents.Excel
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

Public Class BiometricReports

    Public Class BiometricFlagsReport
        Inherits ReportBase(Of BiometricFlagsReportCriteria)



        Public Overrides ReadOnly Property ReportName As String
            Get
                Return "Biometric Flags Report"
            End Get
        End Property

        Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
            cmd.CommandText = "[RptProcs].[rptBiometricFlags]"
            Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
            cmd.Parameters.Remove(hr)
            Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
            cmd.Parameters.Remove(hrs)
            Dim hrids As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResourceIDs").FirstOrDefault
            cmd.Parameters.Remove(hrids)
            Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
            cmd.Parameters.Remove(rohrl)
            Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
            cmd.Parameters.Remove(bhl)
        End Sub


        Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
            MyBase.ModifyDataSet(DataSet)
            MyBase.TablesToIgnore.Add("Table")
        End Sub

        Public Overrides ReadOnly Property CrystalReportType As Type
            Get
                Return GetType(rptBiometricFlagsReport)
            End Get
        End Property

        Public Overrides ReadOnly Property CustomCriteriaControlType As Type
            Get
                Return GetType(BiometricFlagsReportCriteriaControl)
            End Get
        End Property

    End Class


    Public Class BiometricFlagsReportCriteria
        Inherits Singular.Reporting.StartAndEndDateReportCriteria


        Public Property IncludeICR As Boolean = True
        Public Property IncludeStudios As Boolean = True
        Public Property OnlyCurrentFlags As Boolean = True
        Public Property IncludePlayoutServices As Boolean = True

        <Display(Name:="Human Resource")>
        Public Property HumanResourceIDs As List(Of Integer)

        <Display(Name:="Human Resource")>
        Public Property HumanResourceID As Integer?

        <Display(Name:="Human Resource")>
        Public Property HumanResources As String = ""

        <Display(Name:="Human Resource", Order:=3)>
        Public Property HumanResource As List(Of String)


        Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

        <ClientOnly>
        Public Property BusyHRList As Boolean = False

    End Class

    Public Class BiometricFlagsReportCriteriaControl
        Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()

            With Helpers.With(Of BiometricFlagsReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                .Style.TextAlign = TextAlign.left
                .Style.MarginAll("20px")
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                .Style.TextAlign = TextAlign.left
                .Style.MarginAll("20px")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With

                With .Helpers.FieldSet("Criteria")
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.Bootstrap.Row()
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                                With .Helpers.FieldSet("Area")
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricFlagsReportCriteria) d.IncludeICR,
                                                                               "ICR", "Exclude ICR", , , , "fa-hand-o-up", )

                                        End With
                                        With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricFlagsReportCriteria) d.IncludeStudios,
                                                           "Studio", "Studio", , , , "fa-hand-o-up", )

                                        End With
                                        With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricFlagsReportCriteria) d.IncludePlayoutServices,
                                                            "Playout Services", "Playout Services", , , , "fa-hand-o-up", )
                                        End With
                                    End With
                                End With
                            End With
                    



                    With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                        With .Helpers.FieldSet("Human Resources")
                            '.AddClass("FadeHide")
                            .Attributes("id") = "HumanResources"
                            With .Helpers.Div
                                With .Helpers.Div
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(12, 12, 8, 6)
                                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                                                        .Editor.Attributes("placeholder") = "Search Human Resource"
                                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : HumanResourceDelayedRefreshListNoSIDNoPA.DelayedRefreshList}")
                                                    End With
                                        End With
                                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                                            With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "BiometricsReport.RefreshHR()")
                                                    End With
                                        End With
                                    End With
                                End With



                                .Helpers.HTML.NewLine()
                                With .Helpers.Div
                                    .Attributes("id") = "HRDiv"
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                            With .Helpers.If(Function(c As BiometricFlagsReportCriteria) Not c.BusyHRList)
                                                With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                                                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                                            .Button.AddBinding(KnockoutBindingString.click, "BiometricsReport.AddHumanResource($data)")
                                                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                                            .Button.AddClass("btn-block buttontext")
                                                        End With
                                                    End With
                                                End With
                                            End With
                                            With .Helpers.If(Function(c As BiometricFlagsReportCriteria) c.BusyHRList)
                                                With .Helpers.Div
                                                    With .Helpers.HTMLTag("i")
                                                        .AddClass("fa fa-cog fa-lg fa-spin")
                                                    End With
                                                    With .Helpers.HTMLTag("span")
                                                        .Helpers.HTML("Updating...")
                                                    End With
                                                End With
                                            End With
                                        End With
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
                    End With
                End With
            End With









        End Sub

    End Class

    Public Class BiometricsReport
        Inherits ReportBase(Of BiometricReportCriteria)



        Public Overrides ReadOnly Property ReportName As String
            Get
                Return "Biometric Report"
            End Get
        End Property

        Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
            cmd.CommandText = "[RptProcs].[rptBiometricsReport]"
            Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
            cmd.Parameters.Remove(hr)
            Dim hrs As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResources").FirstOrDefault
            cmd.Parameters.Remove(hrs)
            Dim hrids As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResourceIDs").FirstOrDefault
            cmd.Parameters.Remove(hrids)
            Dim rohrl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ROHumanResourceList").FirstOrDefault
            cmd.Parameters.Remove(rohrl)
            Dim bhl As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@BusyHRList").FirstOrDefault
            cmd.Parameters.Remove(bhl)
            cmd.CommandTimeout = 120

        End Sub

        Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
            MyBase.ModifyDataSet(DataSet)
            MyBase.TablesToIgnore.Add("Table")
        End Sub

        Public Overrides ReadOnly Property CrystalReportType As Type
            Get
                Return GetType(rptBiometricsReport)
            End Get
        End Property

        Public Overrides ReadOnly Property CustomCriteriaControlType As Type
            Get
                Return GetType(BiometricReportCriteriaControl)
            End Get
        End Property

    End Class


    Public Class BiometricReportCriteria
        Inherits Singular.Reporting.StartAndEndDateReportCriteria


        <Display(Name:="Human Resource", Order:=3)>
        Public Property HumanResource As List(Of String)

        <System.ComponentModel.DisplayName("Human Resource")>
        Public Property HumanResourceIDs As List(Of Integer)

        <System.ComponentModel.DisplayName("Human Resource")>
        Public Property HumanResources As String = ""

        <System.ComponentModel.DisplayName("Human Resource")>
        Public Property HumanResourceID As Integer?

        Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

        <ClientOnly>
        Public Property BusyHRList As Boolean = False

        Public Property IncludeICR As Boolean = True
        Public Property IncludeStudios As Boolean = True
        Public Property OnlySummary As Boolean = False
        Public Property IncludePlayoutServices As Boolean = True
        Public Property IncludeBreakDetail As Boolean = False
        Public Property IncludeOther As Boolean


    End Class

    Public Class BiometricReportCriteriaControl
        Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()

            With Helpers.With(Of BiometricReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                .Style.TextAlign = TextAlign.left
                .Style.MarginAll("20px")
                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 4, 3)
                .Style.TextAlign = TextAlign.left
                .Style.MarginAll("20px")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
        End With
                With .Helpers.FieldSet("Criteria")
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.Bootstrap.Row
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                                With .Helpers.FieldSet("Area")
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.IncludeICR,
                                                                             "ICR", "Exclude ICR", , , , "fa-hand-o-up", )

                                        End With
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.IncludeStudios,
                                                         "Studio", "Studio", , , , "fa-hand-o-up", )

                                        End With
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.IncludeOther,
                                                 "Other", "Other", , , , "fa-hand-o-up", )


                                        End With
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.IncludePlayoutServices,
                                                          "Playout Services", "Playout Services", , , , "fa-hand-o-up", )

                                        End With
                                    End With
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.OnlySummary,
                                                        "Only Summary", "Only Summary", , , , "fa-hand-o-up", ).Button.Style.Width = 120
                                        End With
                                    End With
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(3, 3, 2, 2)
                                            .Helpers.Bootstrap.StateButton(Function(d As BiometricReportCriteria) d.IncludeBreakDetail,
                                                        "Break Detail", "Break Detail", , , , "fa-hand-o-up", ).Button.Style.Width = 120
                                        End With
                                    End With
                                End With
                            End With



                    With .Helpers.Bootstrap.Column(12, 12, 8, 8)
                        With .Helpers.FieldSet("Human Resources")
                            '.AddClass("FadeHide")
                            .Attributes("id") = "HumanResources"
                            With .Helpers.Div
                                With .Helpers.Div
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(12, 12, 8, 6)
                                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.HumanResource, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                                                        .Editor.Attributes("placeholder") = "Search Human Resource"
                                                        .Editor.AddBinding(Singular.Web.KnockoutBindingString.valueUpdate, "'keyup'")
                            .Editor.AddBinding(Singular.Web.KnockoutBindingString.event, "{keyup : BiometricsReport.DelayedRefreshList}")
                                                    End With
                                        End With
                                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.Bootstrap.Button("", "Refresh", BootstrapEnums.Style.Primary, , BootstrapEnums.ButtonSize.Small, , "fa-refresh", , , "BiometricsReport.RefreshHR()")
                          End With
                                        End With
                                    End With
                                End With
                                .Helpers.HTML.NewLine()
                                With .Helpers.Div
                                    .Attributes("id") = "HRDiv"
                                    With .Helpers.Bootstrap.Row
                                        With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                                            With .Helpers.If(Function(c As BiometricReportCriteria) Not c.BusyHRList)
                                                With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                                                    With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                                                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                                                            .Button.AddBinding(KnockoutBindingString.click, "BiometricsReport.AddHumanResource($data)")
                                                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                                                            .Button.AddClass("btn-block buttontext")
                                                        End With
                                                    End With
                                                End With
                                            End With
                                            With .Helpers.If(Function(c As BiometricReportCriteria) c.BusyHRList)
                                                With .Helpers.Div
                                                    With .Helpers.HTMLTag("i")
                                                        .AddClass("fa fa-cog fa-lg fa-spin")
                                                    End With
                                                    With .Helpers.HTMLTag("span")
                                                        .Helpers.HTML("Updating...")
                                                    End With
                                                End With
                                            End With
                                        End With
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
            End With

                End With
            End With


        End Sub

    End Class




End Class
