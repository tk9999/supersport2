﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Singular.DataAnnotations

Public Class OutsideBroadcastReports

#Region "Production Schedules"

  Class ProductionSchedules
    Inherits Singular.Reporting.ReportBase(Of ProductionSchedulesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Schedules"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionSchedulesCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionSchedules]"
    End Sub

    Public Overrides ReadOnly Property GridInfo As Singular.Reporting.GridInfo
      Get
        Return New Singular.Reporting.GridInfo(Me)
      End Get
    End Property

  End Class

  Class ProductionSchedulesCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required"), DateField(MaxDateProperty:="EndDate", AlwaysShow:=True)>
    Public Property StartDate As DateTime? = Now

    <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required"), DateField(MinDateProperty:="StartDate", AlwaysShow:=True)>
    Public Property EndDate As DateTime? = Now

  End Class

  Public Class ProductionSchedulesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionSchedulesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

End Class
