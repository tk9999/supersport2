﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance
Imports OBLib.FeedbackReports.ReadOnly
Imports OBLib.FeedbackReports
Imports Singular.DataAnnotations
Imports Singular.Web
Imports Infragistics.Documents.Excel
Imports OBLib.Productions.ReadOnly
Imports OBLib.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

Public Class FeedbackReports

#Region "Crew Feedback Summary Report"
  Public Class CrewFeedbackSummaryReport
    Inherits ReportBase(Of CrewFeedbackSummaryReportCriteria)



    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Crew Feedback Summary Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptCrewFeedbackSummaryReport]"
    End Sub


    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptCrewFeedbackSummaryReport) 'GetType(rptBiometricFlagsReport)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CrewFeedbackSummaryReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class CrewFeedbackSummaryReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria


    <Display(Name:="Human Resource", Order:=3), ClientOnly>
    Public Property HumanResource As List(Of String)

    <Display(Name:="Human Resource"), ClientOnly>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource"), ClientOnly>
    Public Property HumanResources As String = ""

    <Display(Name:="Human Resource")>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Read Only Human Resource List"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="Firstname", Order:=3), ClientOnly>
    Public Property FirstName As String = ""

    <Display(Name:="Surname", Order:=3), ClientOnly>
    Public Property Surname As String = ""

    <Display(Name:="Preferred Name", Order:=3), ClientOnly>
    Public Property PreferredName As String = ""

    <Display(Name:="System", Order:=1), ClientOnly>
    Public Property SystemID As Integer?

    <Display(Name:="Read Only System List"), ClientOnly>
    Public Property ROSystemList As List(Of OBLib.Maintenance.ReadOnly.ROSystem) = OBLib.CommonData.Lists.ROSystemList.ToList

    <Display(Name:="Production Area", Order:=2), ClientOnly>
    Public Property ProductionAreaID As Integer?

    <Display(Name:="Read Only Production Area List"), ClientOnly>
    Public Property ROProductionAreaList As List(Of ROProductionArea) = OBLib.CommonData.Lists.ROProductionAreaList.ToList

    Public Property FeedbackReportID As Integer?
    Public Property DisciplineID As Integer?
    Public Property ContractTypeID As Integer?
    Public Property FlagID As Integer?
    Public Property OutsideBroadcastInd As Boolean = True
    Public Property StudioInd As Boolean = True
    Public Property productioncontentind As Boolean = True
    Public Property productionservicesind As Boolean = True

  End Class

  Public Class CrewFeedbackSummaryReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CrewFeedbackSummaryReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Column(12, 12, 6, 4)
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "BiometricReportsCrewFeedbackSummaryReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "BiometricReportsCrewFeedbackSummaryReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.FieldSet("Human Resource Filters")
                    .Attributes("id") = "HumanResourceFilters"
                    .AddClass("FadeHide")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 8)
              With .Helpers.FieldSet("Human Resources")
                .AddClass("FadeHide")
                .Attributes("id") = "HumanResources"
                With .Helpers.Div
                  .Attributes("id") = "HRDiv"
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                    With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                      With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                        With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "BiometricReportsCrewFeedbackSummaryReport.AddHumanResource($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Area")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                      .Helpers.Bootstrap.StateButton(Function(d As CrewFeedbackSummaryReportCriteria) d.OutsideBroadcastInd,
                                                         "OB", "Exclude OB", , , , "fa-hand-o-up", )
                    End With
                    With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                      .Helpers.Bootstrap.StateButton(Function(d As CrewFeedbackSummaryReportCriteria) d.StudioInd,
                                     "Studio", "Studio", , , , "fa-hand-o-up", )
                    End With
                    With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                      .Helpers.Bootstrap.StateButton(Function(d As CrewFeedbackSummaryReportCriteria) d.productioncontentind,
                                      "Studio Content", "Studio Content", , , , "fa-hand-o-up", )
                    End With
                    With .Helpers.Bootstrap.Column(3, 3, 3, 3)
                      .Helpers.Bootstrap.StateButton(Function(d As CrewFeedbackSummaryReportCriteria) d.productionservicesind,
                                      "Studio Services", "Studio Services", , , , "fa-hand-o-up", )
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Feedback Production Report"
  Public Class FeedBackProductionReport
    Inherits ReportBase(Of FeedBackProductionReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "FeedBack Production Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptFeedBackProductionReport]"

    End Sub


    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptFeedBackProductionReport) 'GetType(rptCrewFeedbackSummaryReport) ' 
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FeedBackProductionReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class FeedBackProductionReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="FeedbackFilterType ID", Order:=1)> Public Property FeedbackFilterTypeID As Integer?

    <Display(Name:="Keyword")> Public Property Keyword As String = ""

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property Production As String

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProductionFind) = New List(Of ROProductionFind)

    <Display(Name:="Production Reference No")>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="Production Type", Order:=3), ClientOnly>
    Public Property ProductionTypeID As Integer?

    Public Property OutsideBroadcastInd As Boolean = True
    Public Property StudioInd As Boolean = True
    Public Property ProductionContentInd As Boolean = True
    Public Property ProductionServicesInd As Boolean = True
    Public Property RequiresFeedback As Boolean = False
    Public Property RequiresReconciliation As Boolean = False
    Public Property RequiresFeedbackorReconciliation As Boolean = False
    Public Property RequiresAll As Boolean = False
  End Class

  Public Class FeedBackProductionReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FeedBackProductionReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                With .Helpers.FieldSet("Productions")
                  .Attributes("id") = "Productions"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                            End With
                          End With
                          With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                            End With
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.Div
                      .Attributes("id") = "HRDiv"
                      With .Helpers.Bootstrap.Row
                        With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "OBProductionPAForm2.AddProduction($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                              .Button.AddClass("btn-block buttontext")
                              .Button.AddClass("add-text-align-left")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.FieldSet("Filter")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(6, 3, 3, 3)
                  With .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.RequiresFeedback,
                                   "Feedback Only", "Feedback Only", , , , "fa-hand-o-up", )
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "FeedBackProductionReport.FeedbackFilter(1)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(6, 3, 3, 3)
                  With .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.RequiresReconciliation,
                                   "Recon Only", "Recon Only", , , , "fa-hand-o-up")
                    With .Button
                      .AddBinding(Singular.Web.KnockoutBindingString.click, "FeedBackProductionReport.FeedbackFilter(2)")
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(6, 3, 3, 3)
                  With .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.RequiresFeedbackorReconciliation,
                                   "Feedback & Recon", "Feedback & Recon", , , , "fa-hand-o-up", )
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "FeedBackProductionReport.FeedbackFilter(3)")
                  End With
                End With
                With .Helpers.Bootstrap.Column(6, 3, 3, 3)

                  With .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.RequiresAll,
                                   "All", "All", , , , "fa-hand-o-up", )
                    .Button.AddBinding(Singular.Web.KnockoutBindingString.click, "FeedBackProductionReport.FeedbackFilter(4)")
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.FieldSet("Area")
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(6, 3, 2, 1)
                  .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.OutsideBroadcastInd,
                                                     "OB", "Exclude OB", , , , "fa-hand-o-up", )
                End With
                With .Helpers.Bootstrap.Column(6, 3, 2, 1)
                  .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.StudioInd,
                                  "Studio", "Studio", , , , "fa-hand-o-up", )
                End With
                With .Helpers.Bootstrap.Column(6, 3, 2, 1)
                  .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.ProductionContentInd,
                                    "Studio Content", "Studio Content", , , , "fa-hand-o-up", )
                End With
                With .Helpers.Bootstrap.Column(6, 3, 2, 1)
                  .Helpers.Bootstrap.StateButton(Function(d As FeedBackProductionReportCriteria) d.ProductionServicesInd,
                                    "Studio Services", "Studio Services", , , , "fa-hand-o-up", )
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class
#End Region

#Region "Feedback Report"
  Public Class FeedbackReport
    Inherits ReportBase(Of FeedbackReportCriteria)



    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Feedback Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptFeedbackReport]"

    End Sub


    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptFeedbackReport) 'GetType(rptCrewFeedbackSummaryReport) ' 
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FeedBackReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class FeedbackReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production", Order:=1), ClientOnly>
    Public Property Production As String

    <Display(Name:="Production Reference No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="Production Type", Order:=3), ClientOnly>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Production ID")>
    Public Property ProductionID As Integer?

    <Display(Name:="Feedback Report ID")>
    Public Property FeedbackReportID As Integer?

    <Display(Name:="AdHoc Report Ind")>
    Public Property AdHocReportInd As Boolean = False

    <Display(Name:="Production System Area ID"), ClientOnly>
    Public Property ProductionSystemAreaID As Integer

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProductionFind) = New List(Of ROProductionFind)



  End Class

  Public Class FeedBackReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of FeedbackReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(6, 3, 2, 1)
                .Helpers.Bootstrap.StateButton(Function(d As FeedbackReportCriteria) d.AdHocReportInd,
                                  "Ad Hoc Report", "Ad Hoc Report", , , , "fa-hand-o-up", )
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(6, 3, 2, 2)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.FeedbackReportID, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Enter Feedback Report ID")
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 6, 6, 6)
                    With .Helpers.FieldSet("Productions")
                      .Attributes("id") = "Productions"
                      With .Helpers.Div
                        With .Helpers.Div
                          With .Helpers.Bootstrap.Row
                            With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                              With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                                End With
                              End With
                              With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                                With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                                End With
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.Div
                          .Attributes("id") = "HRDiv"
                          With .Helpers.Bootstrap.Row
                            With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                              With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                  .Button.AddBinding(KnockoutBindingString.click, "OBProductionPAForm2.AddProduction($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                                  .Button.AddClass("btn-block buttontext")
                                  .Button.AddClass("add-text-align-left")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class
#End Region

#Region "Studio Feedback Report"

  Public Class StudioFeedbackReport
    Inherits ReportBase(Of StudioFeedbackReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Studio FeedBack Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptStudioFeedBackReport]"

    End Sub


    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)
      'MyBase.TablesToIgnore.Add("Table")
    End Sub

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptStudioFeedbackReport)
      End Get
    End Property

    'Public Overrides ReadOnly Property CustomCriteriaControlType As Type
    '  Get
    '    Return GetType(FeedBackProductionReportCriteriaControl)
    '  End Get
    'End Property

  End Class

  Public Class StudioFeedbackReportCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Singular.DataAnnotations.DateField(AlwaysShow:=True, AutoChange:=AutoChangeType.None)>
    Public Property ReportDate As Date = New Date(2016, 11, 15)

  End Class

#End Region

End Class
