﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports Singular.Web


Public Class DeveloperReports


#Region "HR Schedule Detail"
  Public Class HRScheduleDetail
    Inherits ReportBase(Of HRScheduleDetailCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "HR Schedule Detail"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRScheduleDetail]"
    End Sub
  End Class

  Public Class HRScheduleDetailCriteria
    Inherits StartAndEndDateReportCriteria
    <System.ComponentModel.DisplayName("Human Resource"),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceList), UnselectedText:="All", DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen)>
    Public Property HumanResourceID As Object
  End Class
#End Region

#Region "Incorrect Timesheet Dates"
  Public Class IncorrectTimesheetDates
    Inherits ReportBase(Of IncorrectTimesheetDatesCriteria)


    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Incorrect Timesheet Dates"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptIncorrectTimesheetDates]"
    End Sub
  End Class
  Public Class IncorrectTimesheetDatesCriteria
    Inherits DefaultCriteria

  End Class
#End Region

#Region "HR Duplicates"
  Public Class HRDuplicates
    Inherits ReportBase(Of DefaultCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "HR Duplicates"
      End Get
    End Property
    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptHRDuplicates]"
    End Sub
  End Class
#End Region

#Region "Production Facility Booking"
  Public Class ProductionFacilityBooking
    Inherits ReportBase(Of ProductionFacilityBookingCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Facility Booking"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionFacilityBookings]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionFacilityBookingCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionFacilityBookingCriteria
        Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class ProductionFacilityBookingCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

        Protected Overrides Sub Setup()
            MyBase.Setup()

            With Helpers.With(Of ProductionFacilityBookingCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
                With .Helpers.FieldSet("Date Selection")
                    With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 12, 11, 8)
                            With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                                .Style.TextAlign = TextAlign.center
                                With .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                                End With
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                        .Editor.Attributes("placeholder") = "Start Date"
                                    End With
                                End With
                            End With
                            With .Helpers.Bootstrap.Column(8, 12, 4, 3)
                                .Style.TextAlign = TextAlign.center
                                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                                        .Editor.Attributes("placeholder") = "End Date"
                                    End With
                                End With
                            End With
                        End With
                    End With
                End With
            End With
        End Sub
  End Class

#End Region
End Class

