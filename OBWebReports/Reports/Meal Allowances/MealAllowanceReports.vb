﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports Singular.DataAnnotations

Public Class MealAllowanceReports

#Region " Master "

  <Serializable()>
  Public Class MealAllowanceCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="MR Month", Order:=1), Required,
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), UnselectedText:="Select Month")>
    Public Property MRMonthID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Include ICR?", Order:=2)>
    Public Property IncludeICR As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include NSW?", Order:=3)>
    Public Property IncludeNSW As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Special?", Order:=4)>
    Public Property IncludeSpecial As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Studio?", Order:=5)>
    Public Property IncludeProductionServicesStudio As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Playout?", Order:=6)>
    Public Property IncludePlayout As Boolean = True

  End Class


  Public Class MealReimbursementsSummary
    Inherits Singular.Reporting.ReportBase(Of MealAllowanceCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Meal Reimbursements Summary"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptMealReimbursementsSummary)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMealReimbursementSummary]"
      cmd.CommandTimeout = 0

    End Sub

    Private Function GetTextFile() As Singular.Documents.TemporaryDocument

      FetchData()

      Dim ds As New DataSet("DataSet")
      Dim dt As DataTable = ds.Tables.Add("Table")
      dt.Columns.Add("HumanResource", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("EmployeeID", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("IDNo", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("CostCentre", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("MonthStartDate", GetType(Date)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("MonthEndDate", GetType(Date)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("TotalReimbursement", GetType(System.Decimal)).ColumnMapping = MappingType.Attribute

      For Each obj As DataRow In DataSet.Tables("Table1").Rows
        Dim row As DataRow = dt.NewRow
        row("HumanResource") = obj("FirstName") & " " & obj("Surname")
        row("EmployeeID") = obj("EmployeeCode")
        row("IDNo") = obj("IDNo")
        row("CostCentre") = obj("CostCentre")
        row("MonthStartDate") = obj("MonthStartDate")
        row("MonthEndDate") = obj("MonthEndDate")
        row("TotalReimbursement") = obj("ReimbursementAmount")
        dt.Rows.Add(row)
      Next

      Return New Singular.Documents.TemporaryDocument(Me.ReportName & ".csv", Singular.Data.CSV.FromDataTable(ds.Tables("Table"), False))

    End Function

    Public Sub New()
      AddCustomButton("Download CSV", "~/Singular/Images/IconExcelData.png", AddressOf GetTextFile)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MealReimbursementsSummaryCriteriaControl)
      End Get
    End Property

  End Class

  Public Class MealReimbursementsSummaryCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MealAllowanceCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MealAllowanceCriteria) d.MRMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeICR, "Include ICR", "Include ICR", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeNSW, "Include NSW", "Include NSW", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeSpecial, "Include Special", "Include Special", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeProductionServicesStudio, "Include Studio", "Include Studio", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludePlayout, "Include Playout", "Include Playout", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


  Public Class DetailMealReimbursementsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MealAllowanceCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MealAllowanceCriteria) d.MRMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Select Month"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeICR, "Include ICR", "Include ICR", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeNSW, "Include NSW", "Include NSW", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeSpecial, "Include Special", "Include Special", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludeProductionServicesStudio, "Include Studio", "Include Studio", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealAllowanceCriteria) d.IncludePlayout, "Include Playout", "Include Playout", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

  Public Class DetailMealReimbursements
    Inherits Singular.Reporting.ReportBase(Of MealAllowanceCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Meal Reimbursements Detailed"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptDetailMealReimbursements)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMealReimbursementDetails]"
      cmd.CommandTimeout = 0

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(DetailMealReimbursementsCriteriaControl)
      End Get
    End Property

  End Class


  Public Class MealReimbursementsPendingShiftAuthorisations
    Inherits Singular.Reporting.ReportBase(Of MealReimbursementsPendingShiftAuthorisationsCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Meal Reimbursements - Pending Shift Authorisations"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptMealReimbursementsPendingShiftAuthorisations)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMealReimbursementPendingShiftAuthorisations]"

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(Meal_Reimbursements_Pending_Shift_Authorisations_CriteriaControl)
      End Get
    End Property

  End Class

  Public Class MealReimbursementsPendingShiftAuthorisationsCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="MR Month", Order:=1), Required,
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), UnselectedText:="Month")>
    Public Property MRMonthID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Include ICR?", Order:=2)>
    Public Property IncludeICR As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include NSW?", Order:=3)>
    Public Property IncludeNSW As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Special?", Order:=4)>
    Public Property IncludeSpecial As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Studio?", Order:=5)>
    Public Property IncludeProductionServicesStudio As Boolean = True

    <System.ComponentModel.DataAnnotations.Display(Name:="Include Playout?", Order:=6)>
    Public Property IncludePlayout As Boolean = True

  End Class

  Public Class Meal_Reimbursements_Pending_Shift_Authorisations_CriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MealReimbursementsPendingShiftAuthorisationsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MealReimbursementsPendingShiftAuthorisationsCriteria) d.MRMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealReimbursementsPendingShiftAuthorisationsCriteria) d.IncludeICR, "Include ICR", "Include ICR", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealReimbursementsPendingShiftAuthorisationsCriteria) d.IncludeNSW, "Include NSW", "Include NSW", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealReimbursementsPendingShiftAuthorisationsCriteria) d.IncludeSpecial, "Include Special", "Include Special", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.Bootstrap.Column(12, 3, 3, 3)
                With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.StateButton(Function(d As MealReimbursementsPendingShiftAuthorisationsCriteria) d.IncludeProductionServicesStudio, "Include Studio", "Include Special", "btn-success", "btn-danger", , , "btn-sm")
                    .Button.Style.Width = "150px"
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class


  Public Class MealReimbursementsProductionContentSummary
    Inherits Singular.Reporting.ReportBase(Of MealReimbursementsProductionContentSummaryCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Meal Reimbursements Summary (Production Content)"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As System.Type
    '  Get
    '    Return GetType(rptDetailMealReimbursements)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMealVouchersProductionContentSummary]"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MealReimbursementsProductionContentSummaryCriteriaControl)
      End Get
    End Property

  End Class

  <Serializable()>
  Public Class MealReimbursementsProductionContentSummaryCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="MR Month", Order:=1), Required,
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), UnselectedText:="Select Month")>
    Public Property MRMonthID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Studios?", Order:=2)>
    Public Property Studios As Boolean = False

    <System.ComponentModel.DataAnnotations.Display(Name:="Maximo?", Order:=3)>
    Public Property Maximo As Boolean = False

  End Class

  Public Class MealReimbursementsProductionContentSummaryCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MealReimbursementsProductionContentSummaryCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MealReimbursementsProductionContentSummaryCriteria) d.MRMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As MealReimbursementsProductionContentSummaryCriteria) d.Studios, "Include Studios", "Exclude Studios", , , , , "btn-sm")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As MealReimbursementsProductionContentSummaryCriteria) d.Maximo, "Include Maximo", "Exclude Maximo", , , , , "btn-sm")
              End With
            End With
          End With
        End With
      End With

    End Sub

  End Class


  Public Class MealReimbursementsProductionContentDetail
    Inherits Singular.Reporting.ReportBase(Of MealReimbursementsProductionContentDetailCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Meal Reimbursements Detail (Production Content)"
      End Get
    End Property

    'Public Overrides ReadOnly Property CrystalReportType As System.Type
    '  Get
    '    Return GetType(rptDetailMealReimbursements)
    '  End Get
    'End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMealVouchersProductionContentDetail]"
      cmd.CommandTimeout = 0

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MealReimbursementsProductionContentDetailCriteriaControl)
      End Get
    End Property

  End Class

  <Serializable()>
  Public Class MealReimbursementsProductionContentDetailCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="MR Month", Order:=1), Required,
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.NSWTimesheets.ReadOnly.ROMRMonthList), UnselectedText:="Select Month")>
    Public Property MRMonthID As Integer?

    <System.ComponentModel.DataAnnotations.Display(Name:="Studios?", Order:=2)>
    Public Property Studios As Boolean = False

    <System.ComponentModel.DataAnnotations.Display(Name:="Maximo?", Order:=3)>
    Public Property Maximo As Boolean = False

  End Class

  Public Class MealReimbursementsProductionContentDetailCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MealReimbursementsProductionContentDetailCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  With .Helpers.Bootstrap.FormControlFor(Function(d As MealReimbursementsProductionContentDetailCriteria) d.MRMonthID, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Month"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As MealReimbursementsProductionContentDetailCriteria) d.Studios, "Include Studios", "Exclude Studios", , , , , "btn-sm")
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As MealReimbursementsProductionContentDetailCriteria) d.Maximo, "Include Maximo", "Exclude Maximo", , , , , "btn-sm")
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

End Class
