﻿Imports OBLib
Imports Singular.Misc

Public Class OBWebReportHierarchy
  Inherits Singular.Reporting.ReportHierarchy

  Protected Overrides Sub SetupHeirarchy()

    If OBLib.Security.Settings.CurrentUser Is Nothing Then
      Exit Sub
    End If

    'Budgeting Reports-------------------------------------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------------------------------------------------------
    If OBLib.Security.Settings.CurrentUser.UserID = 152 Then 'Chevani
      With MainSection("Budgeting")
        .Report(New ProductionReports.ProductionsBudgetedReport, "Reports.Can View Out Of Budget Events")
      End With
    End If

    'Production Reports-------------------------------------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Productions")
      If OBLib.Security.Settings.CurrentUser.SystemID = CInt(OBLib.CommonData.Enums.System.ProductionServices) Then
        .Report(New ProductionReports.ProductionStatusReport, "Reports.Can View Production Status Report")
        .Report(New ProductionReports.ProductionReport, "Reports.Can View Production Report")
        '.Report(New ProductionReports.TheBoard, "Reports.Can View The Board Report")
        '.Report(New ProductionReports.CrewScheduleReport, "Reports.Can View Crew Schedule Report")
        .Report(New ProductionReports.ProductionPAForm, "Reports.Can View Production PA Form Report")
        .Report(New ProductionReports.StudioPAForm, "Reports.Can View Studio PA Form Report")
        .Report(New ProductionReports.StudioPAFormSignOut, "Reports.Can View Studio PA Form Report")
        .Report(New FinanceReports.ProductionSnTReport, "Reports.Can view Production SnT Report")
        .Report(New ProductionReports.ProductionServicesWeekendProductionReport, "Reports.Can View Weekend Production Report")
        .Report(New ProductionReports.ProductionRegionReport, "Reports.Can View Production Region Report")
        .Report(New ProductionReports.ProductionTxAndRigDays, "Reports.Can View Production Tx and Rig Days Report")
        .Report(New ProductionReports.ProductionVenueReport, "Reports.Can View Production Venue Report")
        .Report(New ProductionReports.MonthlyProductionCrewSchedule, "Reports.Can View Monthly Production Crew Schedule")
      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CInt(OBLib.CommonData.Enums.System.ProductionContent) Then
        .Report(New ProductionReports.CommentatorProductionPAForm, "Reports.Can View Production PA Form Report")
        .Report(New ProductionReports.CommentatorProductionPAFormNew, "Reports.Can View Production PA Form Report")
        .Report(New ProductionReports.ProductionContentWeekendProductionReport, "Reports.Can View Weekend Production Report")
        .Report(New ProductionReports.ProductionVenueReport, "Reports.Can View Production Venue Report")
        .Report(New ProductionReports.ProductionOverview, "Reports.Can View Production Overview")
      End If
      .Report(New ProductionReports.RoomScheduleGrid, "Reports.Can View Room Schedule Booking")
    End With

    'Vehicles & Equipment Reports---------------------------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------------------------------------------------------
    If OBLib.Security.Settings.CurrentUser.SystemID = CInt(OBLib.CommonData.Enums.System.ProductionServices) Then
      With MainSection("Vehicle and Equipment")
        .Report(New VehicleAndEquipmentReports.VehicleEquipmentSummary, "Reports.Can View Vehicle Equipment Summary Report")
        .Report(New VehicleAndEquipmentReports.AdHocTimesheets, "Reports.Can View Ad Hoc Timesheets Report")
      End With
    End If

    'Human Resource Reports---------------------------------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Human Resources")
      If OBLib.Security.Settings.CurrentUser.SystemID = CInt(OBLib.CommonData.Enums.System.ProductionServices) Then
        .Report(New HumanResourceReports.HRTimesheet, "Timesheets.Print Timesheets")
        .Report(New HumanResourceReports.PrintHRTimesheet, "Timesheets.Print Timesheets")
        .Report(New HumanResourceReports.HumanResourceShifts, "Reports.Can View Human Resource Shifts Report")
        .Report(New HumanResourceReports.HumanResourceSnT, "Reports.Can View Human Resource S&T Report")
        '.Report(New HumanResourceReports.HoursBooked, "Reports.Can view Overtime Report")
      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CInt(OBLib.CommonData.Enums.System.ProductionContent) Then
        .Report(New HumanResourceReports.ProductionHRShiftReport, "Reports.Can View Production Human Resource Shift Report")
        .Report(New HumanResourceReports.ProductionHRShiftDetailReport, "Reports.Can View Production Human Resource Shift Detail Report")
      End If
      .Report(New HumanResourceReports.HRContractTypes, "Reports.Can View Human Resource Contract Types")
      .Report(New HumanResourceReports.AnnualLeaveReport, "Reports.Can View Annual Leave Report")
      .Report(New HumanResourceReports.HumanResourceLeave, "Reports.Can View Human Resource Leave Report")
      .Report(New HumanResourceReports.HumanResourceSkills, "Reports.Can View Human Resource Skills Report")
      .Report(New HumanResourceReports.HumanResourceSkillLevels, "Reports.Can View Human Resource Skill Levels Report")
      .Report(New HumanResourceReports.HRSkillsReportNew, "Reports.Can View Human Resource Skills Report")
      .Report(New HumanResourceReports.MonthlyScheduleByIndividualReport, "Reports.Can View Monthly Schedule By Individual Report")
      .Report(New HumanResourceReports.HumanResourceScheduleExcelReport, "Reports.Can View Monthly Schedule By Individual Report")
      .Report(New HumanResourceReports.StageHandSchedule, "Reports.Can View Stage Hand Schedule")
      If Singular.Security.HasAccess("Human Resources", "Remuneration Company") _
           Or Singular.Security.HasAccess("Human Resources", "Can View Freelancer Rates") _
           Or Singular.Security.HasAccess("Human Resources", "Remuneration Manager") _
           Or Singular.Security.HasAccess("Human Resources", "Remuneration Company") Then
        .Report(New HumanResourceReports.HRRates)
      End If
      .Report(New HumanResourceReports.HumanResourceDetailsReport, "Reports.Can View Human Resource Details Report")
      .Report(New HumanResourceReports.HoursBookedNew, "Reports.Can View Hours Booked Report")
      '.Report(New HumanResourceReports.HoursBookedStudio, "Reports.Can View Hours Booked Report Studio")
      .Report(New HumanResourceReports.SkillsDatabase, "Reports.Can View Human Resource Skills Database Report")
      .Report(New HumanResourceReports.SkillsAvailability, "Reports.Can View Human Resource Skills Database Report")
      .Report(New HumanResourceReports.DisciplinesBookedForStudios, "Reports.Can View Discipline Bookings Report")
      .Report(New EasyReports.HumanResourceDetails, "Reports.Can View Human Resource Details")
      .Report(New HumanResourceReports.HumanResourceAccreditationReport, "Reports.Can View Human Resource Accreditation Report")
      .Report(New HumanResourceReports.TimesheetNewPolicy, "Reports.Can View Human Resource Timesheet Report - New Policy")
      .Report(New HumanResourceReports.TimesheetNewPolicyPlayout, "Reports.Can View Human Resource Timesheet Report - New Policy")
      .Report(New HumanResourceReports.ProductionContentStaffUsage, "Reports.Can View Production Content Staff Usage Report")
      .Report(New HumanResourceReports.ProductionServicesStaffUsage, "Reports.Can View Production Services Staff Usage Report")
      .Report(New HumanResourceReports.PersonHoursPerQuarter, "Reports.Can View Quarterly Hours Report")
    End With
    'blah

    'Travel Reports-----------------------------------------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Travel")
      If OBLib.Security.Settings.CurrentUser.IsProductionServicesUser Then
        .Report(New TravelReports.TravelRecProductionServices, "Reports.Can View Travel Req Report")
      ElseIf OBLib.Security.Settings.CurrentUser.IsProductionContentUser Then
        .Report(New TravelReports.TravelRecProductionContent, "Reports.Can View Travel Req Report")
        .Report(New TravelReports.TravelReqSnTRecon, "Reports.Can View Travel Req Report")
      End If
      .Report(New TravelReports.HRTravelDetails, "Reports.Can View HR Travel Details Reports")
      .Report(New TravelReports.AdHocTravelRec, "Reports.Can View HR Travel Details Reports")
    End With

    'Finance Reports-----------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Finance")
      If OBLib.Security.Settings.CurrentUser.IsProductionServicesUser Then
        .Report(New FinanceReports.ISPShiftReport, "Reports.Can View ISP Shift Report")
        .Report(New FinanceReports.PettyCashReport, "Reports.Can View Petty Cash Report")
        .Report(New FinanceReports.ProductionCosts, "Reports.Can View Production Costs Report")
        .Report(New FinanceReports.ProductionPlanningHours, "Reports.Can View Production Planning Hours Report")
        .Report(New FinanceReports.PublicHolidayReport, "Reports.Can View Public Holiday Report")
        .Report(New FinanceReports.MonthlySubsistenceandTravelAllowanceReport, "Reports.Can view Monthly SnT Report")
        .Report(New FinanceReports.EventManagerPlanningHours, "Reports.Can View Event Manager Planning Hours Report")
        .Report(New FinanceReports.SnTMonthlyPerPersonPerProduction, "Reports.Can View Commentator S&T Monthly Report")
        .Report(New FinanceReports.SnTMonthlyPerPersonPerProductionExportable, "Reports.Can View Commentator S&T Monthly Report")
        .Report(New FinanceReports.OBSnTReport, "Reports.Can View OB S&T Report")
        .Report(New FinanceReports.ProductionHeadCount, "Reports.Can View Production Head Count Report")
        .Report(New FinanceReports.OvertimeReport, "Reports.Can view Overtime Report") 'AddReport("Finance Reports", New FinanceReports.OvertimeReport)
        .Report(New FinanceReports.OvertimeReportByContractType, "Reports.Can view Overtime Report")  'AddReport("Finance Reports", New FinanceReports.OvertimeReportByContractType)
        .Report(New FinanceReports.FreelancerOvertimeReport, "Reports.Can view Overtime Report") 'AddReport("Finance Reports", New FinanceReports.FreelancerOvertimeReport)
        .Report(New ProductionReports.ProductionDisciplineCountPerContractType, "Reports.Can View Production Discipline Count per Contract Type")
        .Report(New FinanceReports.ProductionSnTReport, "Reports.Can view Production SnT Report")
        With .SubSection("ISP Timesheets")
          .Report(New FinanceReports.ISPTimesheet, "Reports.Can View ISP Timesheet Report")
          .Report(New FinanceReports.ISPTimesheetData, "Reports.Can View ISP Timesheet Report")
          .Report(New FinanceReports.PaymentRun, "Reports.Can View ISP Timesheet Report")
          .Report(New FinanceReports.ProductionCostReportLeon, "Reports.Can View ISP Timesheet Report")
          .Report(New FinanceReports.ISPTimesheetWithRates, "Reports.Can View Reports with Rates")
        End With
      End If
      If OBLib.Security.Settings.CurrentUser.IsProductionContentUser Then
        .Report(New FinanceReports.OBProductionProductionHeadCount, "Reports.Can View OB Production Head Count Report")
        .Report(New FinanceReports.ProductionContentHumanResourceSnT, "Reports.Can View Commentator S&T Monthly Report")
        .Report(New FinanceReports.SnTMonthlyPerPersonPerProduction, "Reports.Can View Commentator S&T Monthly Report")
        .Report(New FinanceReports.SnTMonthlyPerPersonPerProductionExportable, "Reports.Can View Commentator S&T Monthly Report")
        .Report(New FinanceReports.StaffHoursReport, "Reports.Can View Production Content Staff Hours Report")
      End If
      .Report(New FinanceReports.OutsourceServiceSuppliers, "Reports.Can View Outsource Service Suppliers Report")
      .Report(New FinanceReports.OutsourceServicePerSupplierPerProduction, "Reports.Can View Outsource Service Suppliers Report")
      .Report(New FinanceReports.RateUpdateReport, "Reports.Can View Rate Update Report")
      .Report(New FinanceReports.CrewSchedulePerProductionReport, "Reports.Can View Crew Schedule Per Production Report")
      .Report(New FinanceReports.TempProductionCostReport, "Reports.Can view Overtime Report")
      .Report(New FinanceReports.ProductionHRCost, "Reports.Can view Production HR Cost Report")
      .Report(New FinanceReports.ProductionHRCostDetails, "Reports.Can view Production HR Cost Report")
      .Report(New FinanceReports.AdditionalHoursReportProductionServices, "Reports.Can View Additional Hours Report (Production Services)")
      .Report(New FinanceReports.DayAwayReport, "Reports.Can View Day Away Report")
      .Report(New FinanceReports.LeonReportPt2, "Reports.Can View Leon Report Pt 2")
      .Report(New FinanceReports.InvoiceChecksProductionServices, "Reports.Can View Invoice Checks (Production Services) Report")
      .Report(New FinanceReports.UtilisationReport, "Reports.Can View Utilisation Report")
      .Report(New FinanceReports.GenericWorkOrderReport, "Reports.Can View Generic Work Order Report")
      .Report(New FinanceReports.TravelReport, "Reports.Can View Travel Report")
    End With

    'Finance Reports-----------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Outside Broadcast")
      .Report(New OutsideBroadcastReports.ProductionSchedules, "Reports.Can View Production Schedules Report (OB)")
    End With

    'Invoice Reports-----------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Invoicing")
      .Report(New InvoiceReports.CreditorInvoices, "Invoicing.Can Access Payment Runs")
      .Report(New InvoiceReports.CreditorInvoiceDetails, "Invoicing.Can Access Payment Runs")
    End With

    'Studio Reports------------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Studios")
      'Studios
      .Report(New StudioReports.ProductionMonthlyShiftsReport, "Reports.Can View Studio Monthly Shifts Report")
      .Report(New StudioReports.ProductionServicesStudioSMSReport, "Reports.Can View Production Services Studio SMS Report")
      '.Report(New StudioReports.HRShiftPayment, "Reports.Can View Studio HR Shift Payment")
      '.Report(New StudioReports.FreelancerHRShiftPayment, "Reports.Can View Studio Freelance HR Shift Payment")
      .Report(New StudioReports.StaffShiftDetails, "Reports.Can View Studio Staff Shift Details")
      .Report(New StudioReports.ProductionFacilitiesBookings, "Reports.Can View Studio Facility Bookings Report")
      .Report(New StudioReports.WeeklyChangesReport, "Reports.Can View Weekly Changes Report")
      .Report(New StudioReports.WeeklyChangesReportMaximo, "Reports.Can View Weekly Changes Report Maximo")
      .Report(New StudioReports.StudioBookingsShiftReport, "Reports.Can View Studio Crew Shift Report")
      .Report(New StudioReports.BiometricLogsReport, "Reports.Can View Studio Biometric Logs Report")
      .Report(New StudioReports.ServicesStudioDataChecksReport, "Reports.Can View Production services (Studio) Data Checks Report")
    End With

    'Studio Reports------------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Other Reports")
      'Studios
      .Report(New OtherReports.RoomBookingsDataReport, "Reports.Can View Room Bookings Data Report")
      .Report(New OtherReports.OBBookingsDataReport, "Reports.Can View OB Bookings Data Report")
    End With

    'ICR Reports---------------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("ICR")
      .Report(New ICRReports.HRShiftPayment, "Reports.Can View ICR HR Shift Payment")
      .Report(New ICRReports.FreelancerHRShiftPayment, "Reports.Can View ICR Freelance HR Shift Payment")
      .Report(New ICRReports.StaffShiftDetails, "Reports.Can View ICR Staff Shift Details")
      .Report(New ICRReports.ICRNoMealsCredit, "Reports.Can View ICR Staff Shift Details")
      .Report(New ICRReports.ExtraShiftPatternConflict, "Reports.Can View ICR Staff Shift Details")
      .Report(New ICRReports.ShiftPatternDetails, "Reports.Can View Shift Pattern Detail Report")
      .Report(New ICRReports.HRShiftPaymentNew, "Reports.Can View ICR HR Shift Payment")
      .Report(New ICRReports.FreelancerHRShiftPaymentNew, "Reports.Can View ICR Freelance HR Shift Payment")
      .Report(New ICRReports.TeamHoursPerMonth, "Reports.Can View ICR Freelance HR Shift Payment")
      .Report(New ICRReports.TeamHoursPerQuarter, "Reports.Can View ICR Freelance HR Shift Payment")
    End With

    'Satellite Operations------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Equipment Bookings")
      .Report(New SatOpsReports.EquipmentBookingReportExcel, "Reports.Can View Equipment Bookings Report")
      .Report(New SatOpsReports.FeedReportExcel, "Reports.Can View Feed Report")
      .Report(New SatOpsReports.ICRIncidentReport, "Reports.Can View ICR Incident Report")
      .Report(New SatOpsReports.EquipmentBookingCostsReportExcel, "Reports.Can View Feed Costs Report")
      .Report(New SatOpsReports.EquipmentWorkOrderReport, "Reports.Can View Equipment Work Order Report")
    End With

    'Meal Reports--------------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Meal Allowances")
      'Master/ICR
      .Report(New MealAllowanceReports.DetailMealReimbursements, "Reports.Can View Meal Reimbursement Detailed Report")
      .Report(New MealAllowanceReports.MealReimbursementsSummary, "Reports.Can View Meal Reimbursement Summary Report")
      .Report(New MealAllowanceReports.MealReimbursementsPendingShiftAuthorisations, "Reports.Can View Meal Reimbursements Pending Auth. Report")
      .Report(New MealAllowanceReports.MealReimbursementsProductionContentSummary, "Reports.Can View Meal Reimbursement (Production Content) Summary Report")
      .Report(New MealAllowanceReports.MealReimbursementsProductionContentDetail, "Reports.Can View Meal Reimbursement (Production Content) Detail Report")
    End With

    'Easy Reports--------------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Easy")
      .Report(New EasyReports.BookingsByDiscipline, "Reports.Can View Bookings By Discipline")
      .Report(New EasyReports.JCDashboard, "Reports.Can View JC Dashboard")
    End With

    'Commentator Reports-------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    If OBLib.Security.Settings.CurrentUser.IsProductionContentUser Then
      With MainSection("Commentator Reports")
        .Report(New CommentatorReports.TravelAdvances, "Reports.Can View Travel Advances Report")
        .Report(New CommentatorReports.TravelAdvancesByPerson, "Reports.Can View Travel Advances By Person Report")
      End With
    End If

    If CompareSafe(OBLib.Security.Settings.CurrentUserID, CInt(OBLib.CommonData.Enums.Users.SuperUser)) Then
      'Analysis Reports---------------------------------------------------------------------------------------------------------------------------
      '-------------------------------------------------------------------------------------------------------------------------------------------
      With MainSection("Analysis Reports")
        .Report(New AnalysisReports.ProductionSchedule)
        .Report(New AnalysisReports.ProductionsWithMultiplecatering)
        .Report(New AnalysisReports.ProductionSchedulingBreakdown)
        .Report(New AnalysisReports.ProductionSchedulingSummary)
        .Report(New AnalysisReports.MaxOvertimePerDiscipline)
        .Report(New AnalysisReports.OvertimeHoursNew)
        .Report(New AnalysisReports.ProductionVehiclesHoursWorked)
      End With
      'Developer Reports---------------------------------------------------------------------------------------------------------------------------
      '--------------------------------------------------------------------------------------------------------------------------------------------
      With MainSection("Developer Reports")
        .Report(New DeveloperReports.HRDuplicates)
        '.Report(New DeveloperReports.HRScheduleDetail)
        .Report(New DeveloperReports.IncorrectTimesheetDates)
        .Report(New DeveloperReports.ProductionFacilityBooking)
      End With
    End If

    'Biometric Reports---------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Biometric Reports")
      .Report(New BiometricReports.BiometricFlagsReport)
      .Report(New BiometricReports.BiometricsReport)
    End With

    'Feedback Reports---------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Feedback Reports")
      .Report(New FeedbackReports.CrewFeedbackSummaryReport)
      .Report(New FeedbackReports.FeedBackProductionReport)
      .Report(New FeedbackReports.FeedbackReport)
      .Report(New FeedbackReports.StudioFeedbackReport)
    End With

    'Playout Operations Reports-------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    If OBLib.Security.Settings.CurrentUser.IsPlayoutUser Then
      With MainSection("Playout Operations")
        .Report(New PlayoutOperationsReports.PlayoutOperationsMCRShiftReport)
        .Report(New PlayoutOperationsReports.PlayoutOperationsSCCRShiftReport)
        .Report(New PlayoutOperationsReports.IndependentFreelanceAgreementReport)
        .Report(New PlayoutOperationsReports.PlayoutOperationsActivityByResource)
        .Report(New PlayoutOperationsReports.SCCRResourcesInWorkOrder)
        .Report(New PlayoutOperationsReports.PlayoutOperationsASRAReport)
        .Report(New PlayoutOperationsReports.SCCRCalculator)
        .Report(New PlayoutOperationsReports.SCCRLiveEvents)
        .Report(New PlayoutOperationsReports.PlayoutChecksReport)
        .Report(New PlayoutOperationsReports.PlayoutBookingStats)
        .Report(New PlayoutOperationsReports.PlayoutISPHoursSummary)
        .Report(New PlayoutOperationsReports.PlayoutDailyEventSummaryDetailed)
        .Report(New PlayoutOperationsReports.MCRShiftProjections)
        .Report(New ICRReports.PersonHoursPerQuarter)
        '.Report(New PlayoutOperationsReports.MealAllowanceDetailedReport)
        '.Report(New PlayoutOperationsReports.HumanResourceShiftPaymentReport)
        '.Report(New PlayoutOperationsReports.TransmissionMCRReport)
        .Report(New PlayoutOperationsReports.TimesheetNewPolicy)
        .Report(New PlayoutOperationsReports.PlayoutOperationsMCRShiftNewPolicy)
        .Report(New PlayoutOperationsReports.PlayoutOperationsSCCRShiftNewPolicy)
      End With
    End If

    'Biometric Reports---------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Synergy")
      .Report(New SynergyReports.SynergyChangesSimple)
      .Report(New SynergyReports.SynergyChangesWithBookingInfo)
      .Report(New SynergyReports.SynergyProductionSchedules)
    End With

    'User Reports---------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Users")
      .Report(New UserReports.UserActionSummary, "Reports.Can View User Action Summary Report")
    End With

    'Notification Reports------------------------------------------------------------------------------------------------------------------------
    '--------------------------------------------------------------------------------------------------------------------------------------------
    With MainSection("Notifications")
      .Report(New NotificationReports.SmsDelivery, "Reports.Can View Sms Delivery Report")
      .Report(New NotificationReports.SmsDeliveryOBEvent, "Reports.Can View Sms Delivery Report (OB)")
    End With

  End Sub

End Class
